<?php

class LibraryDb {
    
    /**
     * MySQLi link
     * 
     * @var link
     */
    protected $_link;
    
    public function __construct() {
        // Connect
        $this->_link = mysqli_connect('localhost', 'root', '');
        
        // Select the database
        mysqli_select_db($this->_link, 'm4ast');
    }
    
    /**
     * Perform a query
     * 
     * @param string $sql SQL query
     * @return mixed
     */
    public function query($sql) {
        return mysqli_query($this->_link, $sql);
    }
    
    /**
     * Real escape string
     * 
     * @param string $string
     * @return string
     */
    public function escape($string) {
        return mysqli_real_escape_string($this->_link, $string);
    }
    
}