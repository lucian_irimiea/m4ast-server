<?php

class NightPlanning {

    public function modmp($n, $k) {
        // modulo function (modulo created by me, to not used additional libraries)
        // Inputs: - $n = dividend
        //	  - $k = divisor
        // Outputs (return) - $out = remainder;
        $out = $n - floor($n / $k) * $k;
        return $out;
    }

    public function jdtoutc($JD) {
        // Not used in current version , used with IMCCE querry service
        // In this version Miriade Server is used
        // Convert Julian Day to UTC
        // Inputs: $JD - Julian Day
        // Outputs:	$ye = Year
        //           	$mo = Mounth
        //           	$dz = Day
        //           	$ho = Hour
        //           	$mi = Minut
        //           	$se = Second
        // The fallowing is the algorithm and is taken from Wikipedia
        //http://en.wikipedia.org/wiki/Julian_day
        /* ===== Algorithm ==================
          Let J = JD + 0.5: (note: this shifts the epoch back by one half day, to start it at 00:00UTC, instead of 12:00 UTC);
          let j = J + 32044; (note: this shifts the epoch back to astronomical year -4800 instead of the start of the
          Christian era in year AD 1 of the proleptic Gregorian calendar).
          let g = j div 146097; let dg = j mod 146097;
          let c = (dg div 36524 + 1) × 3 div 4; let dc = dg - c × 36524;
          let b = dc div 1461; let db = dc mod 1461;
          let a = (db div 365 + 1) × 3 div 4; let da = db - a × 365;
          let y = g × 400 + c × 100 + b × 4 + a; (note: this is the integer number of full years elapsed since March 1, 4801 BC at 00:00 UTC);
          let m = (da × 5 + 308) div 153 - 2; (note: this is the integer number of full months elapsed since the last March 1 at 00:00 UTC);
          let d = da - (m + 4) × 153 div 5 + 122; (note: this is the number of days elapsed since day 1 of the month at 00:00 UTC,
          including fractions of one day);
          let Y = y - 4800 + (m + 2) div 12; let M = (m + 2) mod 12 + 1; let D = d + 1;
          return astronomical Gregorian date (Y, M, D).
          ====================================== */
        $J = $JD + 0.5;

        $z = $J - floor($J);
        $ho = floor($z * 24);
        $zz = $z * 24 - $ho;
        $mi = floor($zz * 60);
        $se = round(($zz * 60 - $mi) * 60 * 100) / 100; // with 2 decimal places

        $j = $J + 32044;
        $g = floor($j / 146097);
        $dg = modmp($j, 146097);
        $c = floor(((floor($dg / 36524) + 1) * 3) / 4);
        $dc = $dg - $c * 36524;
        $b = floor($dc / 1461);
        $db = modmp($dc, 1461);
        $a = floor(((floor($db / 365) + 1) * 3) / 4);
        $da = $db - $a * 365;
        $y = $g * 400 + $c * 100 + $b * 4 + $a;
        $m = floor(($da * 5 + 308) / 153) - 2;
        $d = $da - floor(($m + 4) * 153 / 5) + 122;
        $ye = $y - 4800 + floor(($m + 2) / 12);
        $mo = modmp(($m + 2), 12) + 1;
        $dz = floor($d + 1);
        $CD = $ye . '-' . $mo . '-' . $dz;
        return $CD;
    }

    public function epsilonmp($jd) {
        //Converted from pb2c (pascal version)
        /* -----------------------------------------------------------------
          Calculeaza inclinarea ecuat pe ecliptica, epsilon (grade cu zec),
          cunoscind data iuliana jd
          -----------------------------------------------------------------
         */
        $tu = ($jd - 2415020.0) / 36525;
        $epsilon = 23.452294 - 0.013012 * $tu - 0.000001 * pow($tu, 2);
        return $epsilon;
    }

    public function coordsunmp($JD, &$xsun, &$ysun, &$zsun) {
        // Convertd from pb2c (Pascal Version)
        // See Meeus 1991 p.151 
        /*
          ----------------------------------------------------------------------------
          Calculeaza coord geoc rect si ecuat ale Soarelui, cunoscind JD.
          v.9 - mai scoate "diamap" diam aparent (') si DistPam (dist S-P in AU)
          v.9 - updatat cateva formule dupa Meeus 1991 p.151
          ----------------------------------------------------------------------------
         */

        $t = ($JD - 2451545) / 36525;
        $rl = 280.46645 + 36000.76983 * $t + 0.0003032 * pow($t, 2); //mean long
        $rm = 357.52910 + 35999.05030 * $t - 0.0001559 * pow($t, 2) - 0.00000048 * pow($t, 3); //mean anom
        $re = 0.016708617 - 0.000042037 * $t - 0.0000001236 * pow($t, 2); //ecc Earth orbit
        $rmr = $rm * M_PI / 180;
        $c = (1.91946 - 0.004817 * $t - 0.000014 * pow($t, 2)) * sin($rmr) + (0.019993 - 0.000101 * $t) * sin(2 * $rmr) + 0.000290 * sin(3 * $rmr); //eq centr
        $rlsun = $rl + $c; //true long
        $rlsunr = $rlsun * M_PI / 180;
        $v = $rm + $c; //true anom
        $vr = $v * M_PI / 180;
        $ro = 1.000001018 * (1 - pow($re, 2)) / (1 + $re * cos($vr)); //dist Pam-Soare UA
        $rla = $rlsun;
        $rlar = $rla * M_PI / 180;
        $ep = $this->epsilonmp($JD);
        $epr = $ep * M_PI / 180;

        $alfa = atan(cos($epr) * sin($rlar) / cos($rlar));
        $delta = atan(sin($epr) * sin($rlar) / sqrt(1 - pow((sin($epr) * sin($rlar)), 2)));
        $rsa = cos($epr) * sin($rlar) / cos($delta);

        if ($rsa >= 0) {
            $raa = 1;
        } else {
            $raa = -1;
        }
        if ($alfa >= 0) {
            $rbb = 1;
        } else {
            $rbb = -1;
        }
        $alfa += M_PI - ($raa + $rbb) * M_PI / 2;

        $xsun = $ro * cos($alfa) * cos($delta);
        $ysun = $ro * sin($alfa) * cos($delta);
        $zsun = $ro * sin($delta);
        // the fallowing data are not used in our program    
        /*
          $DistPam = $ro;

          $alfa = $alfa*180/M_PI;
          $delta = $delta*180/M_PI;
          $alfasun = $alfa/15.0;
          $deltasun = $delta;

          $RazaS = 696000; //km}
          $DiamAp =2.0* atan($RazaS/($ro*149597870))* 60* 180.0/M_PI; //minutes
         */
        return;
    }

    public function equkeplermp($rm, $eccenvsu) {
        // Convertd from pb2c (Pascal Version)
        /* -------------------------------------------------------------------
          rezolva ec lui Kepler (cunoscind anomalia medie rm -expr in grade-
          la un moment dat si excentricit orbitei eccenvsu, calculeaza anom
          excentrica evar -in radiani - )
          -------------------------------------------------------------------
         */

        $k = 1;
        $flag = 1;
        $aux = $rm * M_PI / 180;
        do {
            $equkepler = $aux;
            $aux = $eccenvsu * sin($equkepler) + $rm * M_PI / 180;
            $edif = abs($aux - $equkepler);
            $k++;
            if ($edif < 1e-7) {
                $flag = 0;
            }
            if ($k > 98) {
                $flag = 0;
            }
        } while ($flag);
        return $equkepler;
    }

    public function efemastcommp($JD, $sem, $Epoch, $M, $eccen, $omega, $lomega, $incl, $H, &$alfa, &$delta, &$mV, &$beta) {
        // Convertd from pb2c (Pascal Version)
        /* -----------------------------------------------------------------------
          Calculeaza cu problema celor doua corpuri efemerida (alfa, delta):
          - unui asteroid, cunoscind elementele orbitei
          (M - anomalia medie,
          sem - semiaxa orbitei,
          eccen - excentr, l
          omega - argum periheliului,
          omega - long nodului asc,
          incl- inclinarea), sau
          - unei comete, cunoscand elementele orbitale (sem - semiaxa orbitei,
          eccen - excentr, lomega - argum periheliului, omega - long nodului asc,
          incl - inclinarea, Epoch - epoca osculatiei), pt data
          JD - data iuliana.
          Pt aster mai calculeaza magnitudinea vizuala (mag1) stiind
          H (M1=m.abs)
          si G (M2=slope parameter)
          http://www.bitnik.com/mp/archive/Formula.html
          Pt comete mai calculeaza magnit totala (mag1) si magnit nucleara (mag2)
          stiind Ma1 (total abs mag) si Ma2 (nuclear abs mag) - formule HORIZONS
          Mai calculeaza diam aparent pt aster (in ") cunoscand raza (raza in Km)
          Ref calcul efemerida: Matei Alexescu - Laboratorul.. pag 191
          ------------------------------------------------------------------------
         */

        //-------------------------------------------------------------------------------
        // Compute Sun Coordinates
        $xsun = 0;
        $ysun = 0;
        $zsun = 0;
        $this->coordsunmp($JD, $xsun, $ysun, $zsun);
        //-------------------------------------------------------------------------------

        $ttt = ($JD - 2451545) / 36525;
        $aP = 1.00000011 - 0.00000005 * $ttt; //semiaxa Pamant
        $per = pow($sem / $aP, 3 / 2);

        //epoca osculatiei}
        $EpochY = substr($Epoch, 0, 4);
        $EpochM = substr($Epoch, 4, 2);
        $EpochD = substr($Epoch, 6, 2);
        $oradumy = 0;
        $mindumy = 0;
        $secdumy = 0;
        $fusdumy = 0;
        $varadumy = 0;
        $Epoca = $this->jdaymp($EpochD, $EpochM, $EpochY, $oradumy, $mindumy, $secdumy, $varadumy, $fusdumy);

        $M1 = $M + (360 / ($per * 365.25)) * ($JD - $Epoca);
        while ($M1 < 0) {
            $M1 = $M1 + 360;
        }
        while ($M1 >= 360) {
            $M1 = $M1 - 360;
        }

        //algoritm Alexescu
        $evar = $this->equkeplermp($M1, $eccen);
        $ogr = $omega * M_PI / 180;
        $epr = $this->epsilonmp($JD) * M_PI / 180;
        $orr = $lomega * M_PI / 180;
        $er = $evar;
        $rir = $incl * M_PI / 180;

        $a1 = cos($ogr);
        $a2 = -cos($rir) * sin($ogr);
        $b1 = sin($ogr) * cos($epr);
        $b2 = cos($rir) * cos($ogr) * cos($epr) - sin($rir) * sin($epr);
        $c1 = sin($ogr) * sin($epr);
        $c2 = cos($rir) * cos($ogr) * sin($epr) + sin($rir) * cos($epr);

        $px = $a1 * cos($orr) + $a2 * sin($orr);
        $py = $b1 * cos($orr) + $b2 * sin($orr);
        $pz = $c1 * cos($orr) + $c2 * sin($orr);
        $qx = $a2 * cos($orr) - $a1 * sin($orr);
        $qy = $b2 * cos($orr) - $b1 * sin($orr);
        $qz = $c2 * cos($orr) - $c1 * sin($orr);

        $p[1] = $px;
        $p[2] = $py;
        $p[3] = $pz;
        $q[1] = $qx;
        $q[2] = $qy;
        $q[3] = $qz;

        for ($i = 1; $i <= 3; $i++) {
            $r[$i] = $sem * $p[$i] * (cos($er) - $eccen) + $sem * $q[$i] * sqrt(1 - pow($eccen, 2)) * sin($er);
        }
        $xgeo = $r[1] + $xsun;
        $ygeo = $r[2] + $ysun;
        $zgeo = $r[3] + $zsun;

        $d = sqrt(pow($xgeo, 2) + pow($ygeo, 2) + pow($zgeo, 2));  //distanta la Pamant
        $lr = sqrt((pow($r[1], 2) + pow($r[2], 2) + pow($r[3], 2))); //distanta la Soare
        $DistPam = $d;

        $gr = sqrt($xsun * $xsun + $ysun * $ysun + $zsun * $zsun);
        $a = ($lr * $lr + $d * $d - $gr * $gr) / (2 * $lr * $d);

        if ($a > 0) {
            $betar = atan(sqrt(1 / pow($a, 2) - 1));
        }
        if ($a < 0) {
            $betar = atan(-sqrt(1 / pow($a, 2) - 1)) + M_PI;
        }
        if ($a = 0) {
            $betar = M_PI / 2;
        }
        $beta = $betar * 180 / M_PI; //ungh.de faza in grade}

        $mV = $this->getmagnitude($H, $DistPam, $lr, $beta);

        $alfa = atan($ygeo / $xgeo) * 180 / M_PI;
        if ($ygeo >= 0) {
            $aaa = 1;
        } else {
            $aaa = -1;
        }
        if ($alfa >= 0) {
            $bbb = 1;
        } else {
            $bbb = -1;
        }
        $alfa = ($alfa + 180 - ($aaa + $bbb) * 90);
        $alfa = $alfa / 15.0;

        $delta = atan($zgeo / sqrt($xgeo * $xgeo + $ygeo * $ygeo)) * 180 / M_PI;
        return;
    }

    public function jdohmp($d, $mo, $an) {
        // Computes Julian day at 0h
        // Converted from pb2c pascal version
        /* -------------------------------------------------------------------------
          Calculeaza data iuliana(jd0h)corespunzatoare unei date calendaristice
          cunoscuta, introducind d-ziua,mo-luna,y-anul la ora 0h TU a zilei
          a fost verificat
          -------------------------------------------------------------------------
         */
        if ($mo > 2) {
            $a = floor($an / 100);
            $jd1 = floor(365.25 * $an) + floor(30.6001 * ($mo + 1)) + $d + 1720994.5;
            if ($jd1 >= 2299161) {
                $jd1 = $jd1 + 2 - $a + floor($a / 4);
            }
            $jd0h = $jd1;
        } else {
            $an = $an - 1;
            $mo = $mo + 12;
            $a = floor($an / 100);
            $jd1 = floor(365.25 * $an) + floor(30.6001 * ($mo + 1)) + $d + 1720994.5;
            if ($jd1 >= 2299161) {
                $jd1 = $jd1 + 2 - $a + floor($a / 4);
            }
        }
        $jd0h = $jd1;
        return $jd0h;
    }

    public function jdaymp($d, $mo, $an, $ora, $minut, $sec, $vara, $fus) {
        // Computes Julian Day for any moment of the day
        // Converted from pb2c pascal version
        /* --------------------------------------------------------------------------
          calculeaza data iuliana cunoscind ora(ora), minutul(minut), secunda(sec),
          fusul orar(fus), ora de vara(vara=1 daca sintem in perioada ofic. a
          orei de vara=0,contrar) apelind functia jd0h(zi-d-,luna-mo-,an-an-)
          a fost verificat impreuna cu jd0h
          --------------------------------------------------------------------------
         */
        $jdrest = ($ora + $minut / 60 + $sec / 3600 - $fus - $vara) / 24;
        $jday = $this->jdohmp($d, $mo, $an) + $jdrest;
        return $jday;
    }

    public function orbitelmmp(&$Obj) {
        //-----------------------------------------------------------------------
        // Extract orbital elements from astorb.dat file
        // Input: - $obj_designation = either name or object number
        //		example for the names : Fortuna, 2001 QT166, 5059_T-3, 5059 T-3
        // Output:- $obj_designation = modified and returned as output in format for IMCCE querry
        //			meaning number, if the asteroid is numberd or name in format 2001_QT166
        //	 - Orbital elements
        //	     	$Epoch_obj =  Epoch of osculation 'YYYYMMDD'
        //	     	$M_obj = Mean anomaly in degres
        //	     	$lomega_obj = Argument of perihelion
        //	     	$omega_obj =  Longitude of ascending node
        //	     	$incl_obj =  Inclination
        //	     	$eccen_obj =  Eccentricity
        //	     	$sem_obj = Semimajor axis
        // For additional informatins regarding the format of the raw see 
        // http://www.naic.edu/~nolan/astorb.html
        // Also use the link above to update astorb.dat file
        //-------------------------------------------------------------------------
        //----------------
        // open file identifier for astrorb.dat
        $fdastrorb = fopen(ROOT . "/mvc/library/Astorb/astorb.dat", "r");
        
        $Obj_data = ''; // Output vector
        
        if(!$fdastrorb) View::$alert = "Astorb.dat missing on the server or corrupted";
        else
        {
            foreach ($Obj as $key => $obj_desig)
            {
                $Obj[$key] = trim($obj_desig);
                if(isset($obj_design[4]) && !empty($obj_design[4]))
                {
                    if(trim($obj_desig[4]) == "_")
                    {
                        // if obj_desig is name with underscore, replace  underscore with space 
                        // astorb.dat  object name format
                        $obj_desig[4] = " ";
                        $Obj[$key] = $obj_desig;
                    } 
                    if ((strlen(trim($obj_desig)) == 0)||(strlen(trim($obj_desig)) >64))  // empty input
                    {
                        $obj_designation = ''; // Already empty string, but to keep in mind bad name format
                        //trigger_error("Please go back and input an object", E_USER_ERROR);
                        unset($Obj[$key]);
                    }
                }    
            }

            $counter_astorb = 0; // to escape from infinit while loop
            while (!feof($fdastrorb) || ($counter_astorb > 1e6)) { // Until designation of the object is found in astorb.dat
                $counter_astorb ++; // escape from the loop

                $nwline = fgets($fdastrorb, 4096); // read next line from input file
                $Number_obj = trim(substr($nwline, 0, 6));  //Object number, blank if unumbered
                $Name_obj = trim(substr($nwline, 7, 19)); // Object name
            // Check either object name or object number from astorb line match object designation

                foreach ($Obj as $key => $obj_desig) {
                    $flag = (!strcmp($obj_desig, $Number_obj)) || (!strcmp($obj_desig, $Name_obj));
                    if ($flag) { //object found in astorb.dat
                        $Epoch_obj = trim(substr($nwline, 106, 9)); // Epoch of osculation
            //$Epoch_obj is string with format 'YYYYMMDD'
            // and should extract from this  $EpochY; $EpochM;$EpochD
                        $M_obj = trim(substr($nwline, 115, 11)); //Mean anomaly in degres
                        $lomega_obj = trim(substr($nwline, 126, 11)); // Argument of perihelion in deg.
                        $omega_obj = trim(substr($nwline, 137, 11)); // Longitude of ascending node in deg
                        $incl_obj = trim(substr($nwline, 148, 10)); // Inclination in deg (J2000)
                        $eccen_obj = trim(substr($nwline, 158, 11)); // Eccentricity
                        $sem_obj = trim(substr($nwline, 169, 12)); // Semimajor axis
                        $H_obj = trim(substr($nwline, 42, 6)); // Absolute magnitude
                        if (strlen($Number_obj) > 0) { // select name or number(if exist) of the object
                            $obj_desig = $Number_obj; // Use forward the number if given
                        } else { // if unumbered use the name
                            $obj_desig = $Name_obj; // Use name of the Object if Unnumber
                        }

                        $Obj_data[$obj_desig]['found'] = 1;
                        $Obj_data[$obj_desig]['Epoch'] = $Epoch_obj;
                        $Obj_data[$obj_desig]['M'] = $M_obj;
                        $Obj_data[$obj_desig]['lomega'] = $lomega_obj;
                        $Obj_data[$obj_desig]['omega'] = $omega_obj;
                        $Obj_data[$obj_desig]['incl'] = $incl_obj;
                        $Obj_data[$obj_desig]['eccen'] = $eccen_obj;
                        $Obj_data[$obj_desig]['sem'] = $sem_obj;
                        $Obj_data[$obj_desig]['H'] = $H_obj;
                        unset($Obj[$key]);
                    }// end if found
                }// end for each object
            } // end while,for search object in astorb.dat
        }    
        fclose($fdastrorb); // close astorb.dat file identifier
        return $Obj_data;
    }// END orbitelmmp function

    public function stringnomp($in_str) {
    //Keep only the numbers from the string
    //Meanig the following charcaters "+,-./0123456789"
        $k = 0;
        $out = "";
        for ($i = 0; $i <= strlen($in_str); $i++) {
            if ((ord($in_str[$i]) > 42) && (ord($in_str[$i]) < 58)) {
                $out[$k] = $in_str[$i];
                $k++;
            }
        }
        if (is_array($out))
            $in_str = implode($out);
        $out = $in_str;
        return $out;
    }

    public function getmagnitude($H, $DistPam, $lr, $beta) {
        $beta = $beta * M_PI / 180;
        $p = 0.5 * ((1 - $beta / M_PI) * cos($beta) + 1 / M_PI * sin($beta));
        $m = $H + 2.5 * log10(pow($DistPam, 2) * pow($lr, 2) / $p);
        return $m;
    }

    public function angularsep($RA_1, $DEC_1, $RA_2, $DEC_2) {
        $ang_sep_cos = sin($DEC_1 * M_PI / 180) * sin($DEC_2 * M_PI / 180) + cos($DEC_1 * M_PI / 180) * cos($DEC_2 * M_PI / 180) * cos(($RA_1 - $RA_2) * M_PI / 12);
        $ang_sep = acos($ang_sep_cos) * 180 / M_PI;
        return $ang_sep;
    }

    public function getinfoplan($JD, $obj_designation, $sem_obj, $Epoch_obj, $M_obj, $eccen_obj, $omega_obj, $lomega_obj, $incl_obj, $H, &$mV, &$beta, &$miu, &$ra, &$dec) {
    //-------------------------------------------------------------------------------
    //Calculate efemerids with pb2c
        $RA_obj_pb2c = 0;
        $DEC_obj_pb2c = 0;
        $RA_obj_pb2c_2 = 0;
        $DEC_obj_pb2c_2 = 0;
        $mV = 0;
        $beta = 0;
        $miu = 0;
        $this->efemastcommp($JD, $sem_obj, $Epoch_obj, $M_obj, $eccen_obj, $omega_obj, $lomega_obj, $incl_obj, $H, $RA_obj_pb2c, $DEC_obj_pb2c, $mV, $beta);
        $this->efemastcommp($JD + 1 / 24, $sem_obj, $Epoch_obj, $M_obj, $eccen_obj, $omega_obj, $lomega_obj, $incl_obj, $H, $RA_obj_pb2c_2, $DEC_obj_pb2c_2, $mV, $beta);
        $miu = $this->angularsep($RA_obj_pb2c, $DEC_obj_pb2c, $RA_obj_pb2c_2, $DEC_obj_pb2c_2) * 60;
        $ra = $RA_obj_pb2c;
        $dec = $DEC_obj_pb2c;

    //-------------------------------------------------------------------------------
    //echo $RA_obj_pb2c.'<br>';
    //echo $DEC_obj_pb2c.'<br>';
    //echo $mV.'<br>';
    //echo $miu.'<br>';

        return;
    }

    public function getgeocoord($uai) {
    //$sites = file('http://www.minorplanetcenter.net/iau/lists/ObsCodes.html');
        
        $sites = file(ROOT . '/mvc/library/astorb/UAI.DAT');

        if(!$sites) View::$alert = "UAI.DAT missing on the server or corrupted";
        else
        {
            foreach ($sites as $key => $temp) {
                if (!strcmp($uai, substr($temp, 0, 3))) {
                    $lon = trim(substr($temp, 4, 9));
                    $lat = asin(trim(substr($temp, 21, 9))) * 180 / M_PI;
                    $coord['lon'] = $lon;
                    $coord['lat'] = $lat;
                }
            }
            return $coord;
        }
        return;
    }

    public function hac($JD, $long, $ut, $ra) {
        $JD2000 = 2451545.00000;
        $d = $JD - $JD2000;
    //echo '<br>'.$d.'<br>';
        $lst_c = 100.46 + 0.985647 * $d + $long + 15 * $ut;
        $ha = $lst_c - $ra;
    //echo $lst_c;
        return $ha;
    }

    public function eqtoaltaz($JD, $ra, $dec, $geocoord) {
        $ut = (($JD - 0.5) - floor($JD - 0.5)) * 24;
        $ra = $ra * 15;
        $dec = $dec * M_PI / 180;

        $lon = $geocoord['lon'] * M_PI / 180;
        $lat = $geocoord['lat'] * M_PI / 180;

        $ha = $this->hac($JD, $geocoord['lon'], $ut, $ra);
        $ha = $ha * M_PI / 180;

        $sin_alt = sin($dec) * sin($lat) + cos($dec) * cos($lat) * cos($ha);
        $alt = asin($sin_alt);

        $cos_a = (sin($dec) - sin($alt) * sin($lat)) / (cos($alt) * cos($lat));

        $a = acos($cos_a);
        $alt = $alt * 180 / M_PI;
        $a = $a * 180 / M_PI;

        if (sin($ha) < 0)
            $az = $a;
        else
            $az = 360 - $a;

        $altaz['alt'] = $alt;
        $altaz['az'] = $az;
        return $altaz;
    }

    public function coordsuneq($JD) {
        // Convertd from pb2c (Pascal Version)
        // See Meeus 1991 p.151 
        /*
          ----------------------------------------------------------------------------
          Calculeaza coord geoc rect si ecuat ale Soarelui, cunoscind JD.
          v.9 - mai scoate "diamap" diam aparent (') si DistPam (dist S-P in AU)
          v.9 - updatat cateva formule dupa Meeus 1991 p.151
          ----------------------------------------------------------------------------
         */

        $t = ($JD - 2451545) / 36525;
        $rl = 280.46645 + 36000.76983 * $t + 0.0003032 * pow($t, 2); //mean long
        $rm = 357.52910 + 35999.05030 * $t - 0.0001559 * pow($t, 2) - 0.00000048 * pow($t, 3); //mean anom
        $re = 0.016708617 - 0.000042037 * $t - 0.0000001236 * pow($t, 2); //ecc Earth orbit
        $rmr = $rm * M_PI / 180;
        $c = (1.91946 - 0.004817 * $t - 0.000014 * pow($t, 2)) * sin($rmr) + (0.019993 - 0.000101 * $t) * sin(2 * $rmr) + 0.000290 * sin(3 * $rmr); //eq centr
        $rlsun = $rl + $c; //true long
        $rlsunr = $rlsun * M_PI / 180;
        $v = $rm + $c; //true anom
        $vr = $v * M_PI / 180;
        $ro = 1.000001018 * (1 - pow($re, 2)) / (1 + $re * cos($vr)); //dist Pam-Soare UA
        $rla = $rlsun;
        $rlar = $rla * M_PI / 180;
        $ep = $this->epsilonmp($JD);
        $epr = $ep * M_PI / 180;

        $alfa = atan(cos($epr) * sin($rlar) / cos($rlar));
        $delta = atan(sin($epr) * sin($rlar) / sqrt(1 - pow((sin($epr) * sin($rlar)), 2)));
        $rsa = cos($epr) * sin($rlar) / cos($delta);

        if ($rsa >= 0) {
            $raa = 1;
        } else {
            $raa = -1;
        }
        if ($alfa >= 0) {
            $rbb = 1;
        } else {
            $rbb = -1;
        }
        $alfa += M_PI - ($raa + $rbb) * M_PI / 2;

        $ad_sun['alfa'] = $alfa * 180 / M_PI / 15; //in hours
        $ad_sun['delta'] = $delta * 180 / M_PI; //in hours
        return $ad_sun;
    }

    public function splitdeg($number) {
        if ($number < 0)
            $mysgn = '-';
        else
            $mysgn = '+';
        $number = abs($number);
        $dec = floor($number);
        $min = floor(($number - $dec) * 60);
        $sec = ($number - $min / 60 - $dec) * 3600;
        $sec = round($sec * 100) / 100;
        return $mysgn . sprintf("%02d", $dec) . ':' . sprintf("%02d", $min) . ':' . sprintf("%.2f", $sec);
    }

    public function getg2v($ra, $dec) {
        $data = file(ROOT . '/mvc/library/astorb/G2V.csv');
        if(!$data) View::$alert = "G2V.csv missing on the server or corrupted";
        else
        {
            foreach ($data as $key => $value) {
                $z0 = explode(',', $value);
                $dist[trim($z0[0])] = $this->angularsep($ra, $dec, trim($z0[1]) / 15, trim($z0[2]));
                $mag[trim($z0[0])] = round(trim($z0[3]) * 10) / 10;
                $RAg2v[trim($z0[0])] = $this->splitdeg(trim($z0[1]) / 15);
                $DEg2v[trim($z0[0])] = $this->splitdeg(trim($z0[2]));
            }
            $dist_min = min($dist);
            foreach ($dist as $key => $value) {
                if ($value == $dist_min)
                    $stardesig = $key;
            }
            $magstar = $mag[$stardesig];
            $out['s'] = $stardesig;
            $out['m'] = $magstar;
            $out['d'] = round($dist_min * 10) / 10;
            $out['ra'] = $RAg2v[$stardesig];
            $out['de'] = $DEg2v[$stardesig];
            return $out;
        }
        return;
    }

    public function runsmplan($Obj_data, $JD, $Maglim, $malt, $uai, &$ret) {
        $geocoord = $this->getgeocoord($uai);
        foreach ($Obj_data as $obj_desig => $arr_data) {
            $ah = array();
            $aRA = array();
            $aDEC = array();
            $aMag = array();
            $aMiu = array();
            $aAlt = array();
            $kidx = 0;
            for ($h = 0; $h < 24; $h++) {
                $mV = 0;
                $beta = 0;
                $miu = 0;
                $ra = 0;
                $dec = 0;
                $this->getinfoplan($JD + $h / 24, $obj_desig, $arr_data['sem'], $arr_data['Epoch'], $arr_data['M'], $arr_data['eccen'], $arr_data['omega'], $arr_data['lomega'], $arr_data['incl'], $arr_data['H'], $mV, $beta, $miu, $ra, $dec);
                $altaz_obj = $this->eqtoaltaz($JD + $h / 24, $ra, $dec, $geocoord);
                $ad_sun = $this->coordsuneq($JD + $h / 24);
                $altaz_sun = $this->eqtoaltaz($JD + $h / 24, $ad_sun['alfa'], $ad_sun['delta'], $geocoord);
                if (($altaz_obj['alt'] >= $malt) && ($altaz_sun['alt'] <= -15) && ($mV <= $Maglim)) {
                    $ah[$kidx] = $h + 12;
                    if ($ah[$kidx] >= 24)
                        $ah[$kidx] = $ah[$kidx] - 24;
                    $aRA[$kidx] = $ra;
                    $aDEC[$kidx] = $dec;
                    $aMag[$kidx] = $mV;
                    $aMiu[$kidx] = $miu;
                    $aAlt[$kidx] = $altaz_obj['alt'];
                    $kidx++;
                }
            }
            if (count($ah)) {
                $star = $this->getg2v($aRA[floor($kidx / 2)], $aDEC[floor($kidx / 2)]);
                $ret .='<tr>';
                $ret .='<td>' . $obj_desig . '</td>';
                $ret .='<td>' . (round($aMag[floor($kidx / 2)] * 10) / 10) . '</td>';
                $ret .='<td>' . (round($aMiu[floor($kidx / 2)] * 10) / 10) . '</td>';
                $ret .='<td>' . (round($aRA[floor($kidx / 2)] * 10) / 10) . '</td>';
                $ret .='<td>' . (round($aDEC[floor($kidx / 2)] * 10) / 10) . '</td>';
                $ret .='<td>' . $ah[0] . '</td>';
                $ret .='<td>' . round($aAlt[0]) . '</td>';
                $ret .='<td>' . $ah[floor($kidx / 2)] . '</td>';
                $ret .='<td>' . round($aAlt[floor($kidx / 2)]) . '</td>';
                $ret .='<td>' . $ah[$kidx - 1] . '</td>';
                $ret .='<td>' . round($aAlt[$kidx - 1]) . '</td>';
                $ret .='<td>' . $star['s'] . '</td>';
                $ret .='<td>' . $star['m'] . '</td>';
                $ret .='<td>' . $star['d'] . '</td>';
                $ret .='<td>' . $star['ra'] . '</td>';
                $ret .='<td>' . $star['de'] . '</td>';
                $ret .='</tr>';
            }
        }

        return;
    }

}