<?php

/*Calculeaza cu problema celor doua corpuri efemerida (alfa, delta):
- unui asteroid, cunoscind elementele orbitei 
(M - anomalia medie,
sem - semiaxa orbitei, 
eccen - excentr, l
omega - argum periheliului,
omega - long nodului asc, 
incl- inclinarea), sau
- unei comete, cunoscand elementele orbitale (sem - semiaxa orbitei,
eccen - excentr, lomega - argum periheliului, omega - long nodului asc,
incl - inclinarea, Epoch - epoca osculatiei), pt data 
JD - data iuliana.
Pt aster mai calculeaza magnitudinea vizuala (mag1) stiind 
H (M1=m.abs)
si G (M2=slope parameter) 
http://www.bitnik.com/mp/archive/Formula.html
Pt comete mai calculeaza magnit totala (mag1) si magnit nucleara (mag2)
stiind Ma1 (total abs mag) si Ma2 (nuclear abs mag) - formule HORIZONS
Mai calculeaza diam aparent pt aster (in ") cunoscand raza (raza in Km)
Ref calcul efemerida: Matei Alexescu - Laboratorul.. pag 191 
------------------------------------------------------------------------
*/

//-------------------------------------------------------------------------------
// Compute Sun Coordinates
$xsun = 0;
$ysun = 0;
$zsun = 0;

require_once ROOT . '/mvc/functions/coordsunmp.php';
//-------------------------------------------------------------------------------

$ttt = ($JD-2451545)/36525;
$aP = 1.00000011 - 0.00000005*$ttt; //semiaxa Pamant
$per = pow($sem/$aP,3/2);

//epoca osculatiei}
$EpochY = substr ($Epoch,0,4); 
$EpochM = substr ($Epoch,4,2); 
$EpochD = substr ($Epoch,6,2);
$oradumy = 0; $mindumy = 0; $secdumy = 0; $fusdumy = 0; $varadumy = 0;

$dd = $EpochD;
$mm = $EpochM;
$yyyy = $EpochY;
$ora = $oradumy;
$minut = $mindumy;
$sec = $secdumy;
$vara = $varadumy;
$fus = $fusdumy;

require_once ROOT . '/mvc/functions/julianday.php';

$M1 =  $M + (360/($per*365.25))*($JD-$Epoch);
while ($M1<0) {$M1 = $M1+360;}
while ($M1>=360) {$M1 = $M1-360;}

$rm = $M1;
$eccenvsu = $eccen;

//algoritm Alexescu
require_once ROOT . '/mvc/functions/equkeplermp.php';
$evar = $equkepler; 
$ogr = $omega*M_PI/180;  

require_once ROOT . '/mvc/functions/epsilonmp.php';
$epr = $epsilon*M_PI/180;
$orr = $lomega*M_PI/180;
$er = $evar;
$rir = $incl*M_PI/180;

$a1 = cos($ogr);
$a2 = -cos($rir)*sin($ogr);
$b1 = sin($ogr)*cos($epr);
$b2 = cos($rir)*cos($ogr)*cos($epr)-sin($rir)*sin($epr);
$c1 = sin($ogr)*sin($epr);
$c2 = cos($rir)*cos($ogr)*sin($epr)+sin($rir)*cos($epr);

$px = $a1*cos($orr)+$a2*sin($orr);
$py = $b1*cos($orr)+$b2*sin($orr);
$pz = $c1*cos($orr)+$c2*sin($orr);
$qx = $a2*cos($orr)-$a1*sin($orr);
$qy = $b2*cos($orr)-$b1*sin($orr);
$qz = $c2*cos($orr)-$c1*sin($orr);

$p[1] = $px; $p[2] = $py; $p[3] = $pz;
$q[1] = $qx; $q[2] = $qy; $q[3] = $qz;

for($i = 1; $i<= 3; $i++)
{
    $r[$i] =  $sem*$p[$i]*(cos($er)-$eccen)+$sem*$q[$i]*sqrt(1 - pow($eccen,2))*sin($er);
}
$xgeo =  $r[1] + $xsun; $ygeo =  $r[2] + $ysun; $zgeo =  $r[3]+$zsun;

$d =  sqrt(pow($xgeo,2) + pow($ygeo,2) + pow($zgeo,2));  //distanta la Pamant
$lr =  sqrt((pow($r[1],2) + pow($r[2],2) + pow($r[3],2))); //distanta la Soare
$DistPam =  $d;

$gr =  sqrt($xsun*$xsun+$ysun*$ysun+$zsun*$zsun);
$a =  ($lr*$lr+$d*$d-$gr*$gr)/(2*$lr*$d);

if($a>0){$betar = atan(sqrt(1/pow($a,2)-1));}
if($a<0){$betar = atan(-sqrt(1/pow($a,2)-1))+M_PI;}
if($a=0){$betar = M_PI/2;}
$beta =  $betar*180/M_PI; //ungh.de faza in grade}

require_once ROOT . '/mvc/functions/getmagnitude.php';
$mv = $m;

$alfa =  atan($ygeo/$xgeo)*180/M_PI;
if($ygeo >= 0){$aaa = 1;} 
else {$aaa = -1;}
if($alfa >= 0){$bbb = 1;} 
else {$bbb = -1;}
$alfa =  ($alfa + 180-($aaa + $bbb)*90); 
$alfa =  $alfa/15.0;

$delta =  atan($zgeo/sqrt($xgeo*$xgeo + $ygeo*$ygeo))*180/M_PI;