<?php

// Generate database
$sql = "CREATE DATABASE m4ast
        COLLATE utf8_general_ci
       ";

// Execute the query
$this->db->query($sql);

// GENERATE M4AST_DATABASE------------------------------------------------------
// Delete m4ast_database table
$sql = "DROP TABEL `m4ast_database`
       ";

// Execute the query
$this->db->query($sql);

// Generate m4ast_database table
$sql = "CREATE TABLE m4ast_database
        (
        FILE_NAME varchar(50) PRIMARY KEY,
        O_DESIG text,
        O_NR text,
        OBSV_DATE text,
        UAI int(11),
        L_MIN float,
        L_MAX float,
        PUBLISHED text,
        NAME_EQUIV text
        );
       ";

// Execute the query
$this->db->query($sql);
// END GENERATE M4AST_DATABASE--------------------------------------------------

// GENERATE SMASSMIT_ASTEROIDS--------------------------------------------------
// Delete smassmit_asteroids table
$sql = "DROP TABEL `smassmit_database`
       ";

// Execute the query
$this->db->query($sql);

// Generate smassmit_asteroids table
$sql = "CREATE TABLE smassmit_asteroids
        (
        ID int(11) PRIMARY KEY AUTO_INCREMENT,
        NUMBER text,
        NAME text,
        PROV_NAME text,
        ENTRIES int(11) NOT NULL
        );
       ";

// Execute the query
$this->db->query($sql);
// END GENERATE SMASSMIT_ASTEROIDS----------------------------------------------

// GENERATE SMASSMIT_SPECTRA----------------------------------------------------
// Delete smassmit_spectra table
$sql = "DROP TABEL `smassmit_spectra`
       ";

// Execute the query
$this->db->query($sql);

// Generate smassmit_spectra table
$sql = "CREATE TABLE smassmit_spectra
        (
        ID int(11) PRIMARY KEY AUTO_INCREMENT,
        ID_AST int(11) NOT NULL,
        FILE_NAME text,
        PUBLISHED text,
        OBSV_DATE text,
        L_MIN float,
        L_MAX float
        );
       ";

// Execute the query
$this->db->query($sql);
// END GENERATE SMASSMIT_SPECTRA------------------------------------------------

// GENERATE USER----------------------------------------------------------------
// Delete user table
$sql = "DROP TABEL `user`
       ";

// Execute the query
$this->db->query($sql);

// Generate user table
$sql = "CREATE TABLE user
        (
        ID int(11) PRIMARY KEY AUTO_INCREMENT,
        USERNAME varchar(25) NOT NULL UNIQUE,
        PASSWORD varchar(50) NOT NULL,
        ADMIN int(11) DEFAULT '0'
        );
       ";

// Execute the query
$this->db->query($sql);

$sql = "INSERT INTO `user`(
        `USERNAME`,
        `PASSWORD`,
        `ADMIN`
        ) 
        VALUES (
        'Lucian Irimiea',
        'd5f7891d336db7d86b2f3ee7303c3169',
        '1'
        )
       ";

// Execute the query
$this->db->query($sql);
// END GENERATE user------------------------------------------------