<?php

// Computes Julian Day for any moment of the day

// Gets hour, minute, second
$jdrest = ($ora+$minut/60+$sec/3600-$fus-$vara)/24;

// Gets Julian day at 0h
if ($mm>2)
{
    $a = floor($yyyy/100);
    $jd1 = floor(365.25*$yyyy) + floor(30.6001*($mm+1))+$dd+1720994.5;
    if ($jd1 >=2299161)
    {
        $jd1 = $jd1 + 2 - $a + floor($a/4);
    }
    $jd0h = $jd1;
}
else
{
    $yyyy = $yyyy - 1;
    $mm = $mm + 12;
    $a = floor($yyyy/100);
    $jd1 = floor(365.25*$yyyy) + floor(30.6001*($mm+1))+ $dd + 1720994.5;
    if ($jd1 >=2299161) 
    {
        $jd1 = $jd1+2-$a+ floor($a/4);
    }
}
$jd0h = $jd1;

// Final result
$JD = $jd0h + $jdrest; 