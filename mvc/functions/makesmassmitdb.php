<?php

    // Insert SmassSpec.log into sql database
    
//change execution time
ini_set('max_execution_time', 3000);

// Prepare the sql
// Sequence used to determine errors in database update---------------------
$sql = "SELECT  * 
        FROM    smassmit_spectra
        ";
$old = 0;// Number of spectra already in table
$old2 = 0;// Number of asteroids already in table
if (false !== $result = $this->db->query($sql)) 
{
    while ($row = mysqli_fetch_assoc($result)) 
    {
        $old++;
    }
}
$sql = "SELECT  * 
        FROM    smassmit_asteroids
        ";

if (false !== $result = $this->db->query($sql)) 
{
    while ($row = mysqli_fetch_assoc($result)) 
    {
        $old2++;
    }
}
//End error determination---------------------------------------------------

$ok1 = '';
$ok2 = '';

//Database generation-------------------------------------------------------
// Open catalog with informations about all database asteroid spectrums;
$fdcatalog = fopen(ROOT . "/mvc/library/smassmit/SmassSpec.log","r");
$new = 0;// Number of new spectra added
$new2 = 0;// Number of new asteroids added
$all = 0;// Number of spectra
$all2 = 0;// Number of asteroids
while(!feof($fdcatalog))
{
    $catalog_line = trim(fgets($fdcatalog)); // get line from the log, clean blank spaces from begining and end
    $fields = explode('|',$catalog_line);
    if(count($fields) > 1)
    {
        $all2++;
        $n = trim($fields[0]);// Get number of spectras for asteroid
        // Prepare the SQL 
        $sql = "INSERT INTO `m4ast`.`smassmit_asteroids` (`ID`, `NUMBER`, `NAME`, `PROV_NAME`, `ENTRIES`) 
            VALUES ('" . $this->db->escape($all2) . "',
                    '" . $this->db->escape(trim($fields[1])) . "',
                    '" . $this->db->escape(trim($fields[2])) . "',
                    '" . $this->db->escape(trim($fields[3])) . "',
                    '" . $this->db->escape(trim($fields[0])) . "'
                    );
            ";
        // Execute the query
        if (false == $result = $this->db->query($sql)) 
        {
            $ok1 = "Asteroid list update encountered errors";
        } 
        else
        {
            $new2++;
        }

        $i = 0;
        while($i < $n)   
        {
            $all++;

            // Prepare the SQL 
            $sql = "INSERT INTO `m4ast`.`smassmit_spectra` (`ID`, `ID_AST`, `FILE_NAME`, `PUBLISHED`, `OBSV_DATE`, `L_MIN`, `L_MAX`) 
                VALUES ('" . $this->db->escape($all) . "',
                        '" . $this->db->escape($all2) . "',
                        '" . $this->db->escape(trim($fields[$i * 5 + 4])) . "',
                        '" . $this->db->escape(trim($fields[$i * 5 + 5])) . "',
                        '" . $this->db->escape(trim($fields[$i * 5 + 6])) . "',
                        '" . $this->db->escape(trim($fields[$i * 5 + 7])) . "',
                        '" . $this->db->escape(trim($fields[$i * 5 + 8])) . "'
                        );
                ";
            // Execute the query
            if (false == $result = $this->db->query($sql)) 
            {
                $ok2 = "Spectra list update encountered errors";
            } 
            else
            {
                $new++;
            }
            $i++;
        }
    }
}
//End db generation---------------------------------------------------------

//Notifications-------------------------------------------------------------
if($old + $new < $all || $old2 + $new2 < $all2)
{
    // Error
    View::$alert = $ok1 . "<br>" . $ok2 . "<br>Database update incomplete";
}
View::$info = 'Number of asteroids already in table: '.$old2.'<br>New asteroids added: '.$new2.'<br>Number of spectra already in table: '.$old.'<br>New spectra added: '.$new;
if($old == $all && $old2 == $all2)
{
    View::$info .= '<br><br>No new asteroids or spectra added<br>Database up to date';
}
else 
{
    View::$info .= '<br>New number of asteroids in database: '.$all2.'<br>New number of spectra in database: '.$all;
}
//End notifications---------------------------------------------------------

// Close catalog with informations about all database asteroid spectrums;
fclose($fdcatalog);

// Revert execution time
ini_set('max_execution_time', 300);