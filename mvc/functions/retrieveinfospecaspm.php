<?php

// Retrive informations about asteroid spectrum from the header of file that contains spectrum
// Input: $filename - name of the file located in .Spectra/ database

$path = $path_file;

if(file_exists($path))
{
    // Open asteroid spectrum file (located in folder ./Spectra )  
    $fdfile = fopen($path_file,"r");

    // Print out the first seven line from header of spectrum
    for($i=0; $i<7; $i++)
    {
        $file_line = trim(fgets($fdfile));
        if(strpos($file_line, 'http'))
        {
            $z0 = explode('article:',$file_line);
            @ $file_line = $z0[0].'article: <a href='.'"'.trim($z0[1]). '" target="_blank">'.trim($z0[1]).'</a>';
        }

        $ast_header[$i]=$file_line;
    }
    fclose($fdfile);
}
else 
{
    View::$alert = "Spectrum does not exist, check the given name";
}