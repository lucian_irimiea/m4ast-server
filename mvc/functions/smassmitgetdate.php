<?php

// Gets date of observation

$nodate = 1;

// Search for the observation date
foreach ($sp as $key => $value)
{
    if($z5[0] == $value)
    {
        fwrite($fdoutput, sprintf("%s | ", $spdate[$key]));
        $nodate = 0;
    }
}

// No observation date found
if($nodate == 1)
{
    fwrite($fdoutput, sprintf("- | "));
}