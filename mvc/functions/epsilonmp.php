<?php

//Converted from pb2c (pascal version)
/*-----------------------------------------------------------------
Calculeaza inclinarea ecuat pe ecliptica, epsilon (grade cu zec),
cunoscind data iuliana jd                                        
-----------------------------------------------------------------
*/
$tu = ($JD-2415020.0)/36525;
$epsilon = 23.452294-0.013012*$tu-0.000001*pow($tu,2);