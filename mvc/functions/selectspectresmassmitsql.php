<?php

// Search in SMASSMIT spectrum database catalog for name of the files containing 
// asteroids spectrums according with  some criterias
// The results will be printed on screen

if ((strlen($crit1s) == 0)&&(strlen($crit2s) == 0)&&(strlen($crit3s) == 0)) 
{
    //Information alert
    View::$info="Please give an input parameter to search in database.";
}
 else 
{
    
    if (strlen($crit1s) == 0) $crit1s = '|'; else $crit1s = trim($crit1s);
    if (strlen($crit2s) == 0) $crit2s = '|'; else $crit2s = trim($crit2s);
    if (strlen($crit3s) == 0) $crit3s = '|'; else $crit3s = trim($crit3s);
    // Prepare the sql
    $sql = " 
            SELECT      A.NUMBER, A.NAME, A.PROV_NAME, B.FILE_NAME, B.PUBLISHED, B.OBSV_DATE, B.L_MIN, B.L_MAX
            FROM        smassmit_asteroids as A, smassmit_spectra as B
            WHERE       (A.NUMBER LIKE '%".$crit1s."%' 
               OR       A.NUMBER LIKE '%".$crit2s."%' 
               OR       A.NUMBER LIKE '%".$crit3s."%' 
               OR       A.NAME LIKE '%".$crit1s."%'
               OR       A.NAME LIKE '%".$crit2s."%'
               OR       A.NAME LIKE '%".$crit3s."%'
               OR       A.PROV_NAME LIKE '%".$crit1s."%'
               OR       A.PROV_NAME LIKE '%".$crit2s."%'
               OR       A.PROV_NAME LIKE '%".$crit3s."%'
               OR       B.FILE_NAME LIKE '%".$crit1s."%'
               OR       B.FILE_NAME LIKE '%".$crit2s."%'
               OR       B.FILE_NAME LIKE '%".$crit3s."%') 
               AND      A.ID = B.ID_AST    
    ";
    
    // Prepare the data
    $fields = array();
    
    // Generate table headers
    $headers = array(
        'FILE_NAME' => 'File Name',
        'NAME' => 'Obj. Design',
        'NUMBER' => 'Obj. No.',
        'PROV_NAME' => 'Obj. Prov. Name',
        'OBSV_DATE' => 'Obsv. Date',
        'L_MIN' => '<font size=5>'.'&#955;'.'</font>'.'<sub>min</sub>',
        'L_MAX' => '<font size=5>'.'&#955;'.'</font>'.'<sub>max</sub>',
        'PUBLISHED' => 'Published'
    );
    // Get the result, max 50
    if (false !== $result = $this->db->query($sql)) 
    {
        $kdisp = 0;
        while ($row = mysqli_fetch_assoc($result)) 
        {
            if($kdisp<50)
            {
                $fields[] = $row;
                $kdisp++;
            }
            else
            {
                break;
            }
        }
        
        if($kdisp === 0)
        {
            // No results found
            View::$alert="No results for the entered criteria";
        }
        else
        {
            // Search finished, results found
            $search_done = 1;
            View::$success = "Search successful<br>The following are the files in the database, acording to your criteria:";
        }
    }
    else
    {
        View::$alert = 'SQL query returned false';
    }
}