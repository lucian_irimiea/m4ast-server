<?php

// This code is used to download a file with it's name given by rlfname variable

if($rlfname != NULL)
{
    //define the path to your download folder plus assign the file name
    $path = ROOT.'/mvc/library/Relab/Alldata/'.$rlfname.'.txt';
    
    // check that file exists and is readable
    if (file_exists($path) && is_readable($path))
    {
        // get the file size and send the http headers
        $size = filesize($path);
        header('Content-Type: application/octet-stream');
        header('Content-Length: '.$size);
        header('Content-Disposition: attachment; filename='.$rlfname.'.txt');
        header('Content-Transfer-Encoding: binary');

        // open the file in binary read-only mode
        $file = fopen($path, 'rb');

        if ($file)
        {
            // stream the file and exit the script when complete
            fpassthru($file);
            exit;
        } 
        else
        {
            //Alert, file cannot be read
            View::$alert = "Download failed"."<br>"."File cannot be read";
        }
    } 
    else
    {
        //Alert, file does not exist
        View::$alert = "Download failed"."<br>"."File with name ".$ftodown." does not exist in ".$path;
    }
}
else
{
    View::$alert = "No file name given";
}