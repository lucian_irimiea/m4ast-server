<?php

// Generates the asteroid log using the smassmit catalog

//change execution time
ini_set('max_execution_time', 30000);

//open output file
$fdoutput = fopen(ROOT . "/mvc/library/smassmitaux/SmassSpec_prov.log", "w");

// Get known observation dates for all asteroids and prepare for search
$a0 = file_get_contents("http://smass.mit.edu/minus.html");
$a1 = explode('<table>', $a0);
$a2 = explode('</table>',$a1[1]);
$data2 = explode('<tr>', $a2[0]);
unset($a0);unset($a1);unset($a2);
foreach ($data2 as $key => $value)
{
    $a1 = explode('<td class="runnumber">',$value);
    if(count($a1) > 1)
    {
        $ref = explode('</td>', $a1[1]);
        $ref = explode('sp', $ref[0]);
        if(count($ref) == 1)
        {
            $ref = explode('dm', $ref[0]);
            $sp[] = "dm[0" . $ref[1] . "]";
        }
        elseif(strlen($ref[1]) == 2)
        {
            $sp[] = "sp[0" . $ref[1] . "]";
        }
        else
        {
            $sp[] = "sp[" . $ref[1] . "]";
        }
    }
    $a1 = explode('<td class="rundate">',$value);
    if(count($a1) > 1)
    {
        $a1 = explode('</td>', $a1[1]);
        $spdate[] = $a1[0];
    }
}

//file_put_contents(ROOT . "/mvc/library/smassmit/smassmit_catalog.txt", file_get_contents("http://smass.mit.edu/catalog.php"));

// Get asteroid catalog
$z0 = file_get_contents("http://smass.mit.edu/catalog.php");
$z1 = explode('<tbody>', $z0);
$z2 = explode('</tbody>',$z1[1]);
$data = $z2[0];
unset($z0);unset($z1);unset($z2);

$z0 = explode('<tr class=', $data);

$i = 0;
$nr = 0;

foreach ($z0 as $key => $value)
{
    $z1 = explode('mpnumber',$value);
    $value = str_replace('<sub>','',$value);
    if(count($z1)>1)
    {    
        $z2=explode('>(',$z1[1]);
        
        $desig0 = '-';//Has asteroid number
        $desig1 = '-';//Has asteroid name
        $desig2 = '-';//Has asteroid provname
        
        // Get asteroid number
        if(count($z2)>1)
        {
            $z3 = explode(')</td',$z2[1]);        
            $desig0 = trim($z3[0]);                                                              
        }

        // Get asteroid name
        $z4 = explode('mpname',$z1[1]);
        $z4 = explode('</td>',$z4[1]);
        $z4 = explode('>',$z4[0]);
        if($z4[1] != '')    $desig1 = trim($z4[1]);

        // Get asteroid prov name
        $z4 = explode('provdesig',$z1[1]);
        $z4 = explode('</td>',$z4[1]);
        $z4 = explode('>',$z4[0]);
        if($z4[1] != '')    $desig2 = trim($z4[1]);

        // Number of entries per asteroid
        $z4 = explode('rowspan="',$z1[1]);
        $z4 = explode('">',$z4[1]);
        $nr = trim($z4[0]);
        
        fwrite($fdoutput, sprintf("%s | ", $nr));//number of entries
        fwrite($fdoutput, sprintf("%s | ", $desig0));//number
        fwrite($fdoutput, sprintf("%s | ", $desig1));//name
        fwrite($fdoutput, sprintf("%s | ", $desig2));//prov name
    }
    
    // Get data type
    $z2 = explode('datalinks">',$value);
    if(count($z2) > 1)
    {
        // Get data link
        $z3 = explode('href="',$z2[2]);
        $z4 = explode('"><',$z3[1]);
        $z5 = explode('/',$z4[0]);
        $z5 = explode('.txt',$z5[3]);
        fwrite($fdoutput, sprintf("%s | ", $z5[0]));//file name
        file_put_contents(ROOT . "/mvc/library/smassmitaux/spectre/" . $z5[0] . ".txt", file_get_contents("http://smass.mit.edu/" . $z4[0]));
        
        //Get plot link, if any
        if(count($z3) == 4)
        {
            $z4 = explode('">',$z3[2]);
            //fwrite($fdoutput, sprintf("%s | ", $z4[0]));
        }
        
        // Publish link
        if(count($z3) != 3) @$z4 = explode('">',$z3[3]);
        else        @$z4 = explode('">',$z3[2]);
        $z5 = explode("#",$z4[0]);
        fwrite($fdoutput, sprintf("%s | ", $z5[1]));

        // Reference
        $z5 = explode('</a></td><td>',$z4[1]);
        //fwrite($fdoutput, sprintf("%s | ", $z5[0]));

        // Date of observation, if any
        require ROOT . '/mvc/functions/smassmitgetdate.php';
    }
    
    if($i == $nr)   
    {
        fwrite($fdoutput, sprintf("\n"));
        $i = 0;
        $nr = 0;
    }
    $i++;                
}

fclose($fdoutput);

//reset execution time
ini_set('max_execution_time', 300);