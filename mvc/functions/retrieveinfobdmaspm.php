<?php

// Retrive informations for Bus DeMeo taxonomy
// This function is executed after runing octave: $out =  shell_exec ("/usr/local/bin/octave /var/www/html/m4ast/output/script_octave".$out_rand.".m");
// Input: $out - the results printed after executed octave script BusDeMeotaxonomy

//Get Reliability informations (number of points on which comparation is made)
$idx_rlb = strpos($out, 'Reliability=');
$reliability = substr($out, $idx_rlb+12, 4); //According with the result outputed by Octave

// Print the result to output
// Print the first five classes found
for($i=1; $i<=5; $i++)
{
    // (format example from ocatve output) BDMfit1Sr=0.028954
    $idx_cls = strpos($out, 'BDMfit'.$i); // Get the i class from the output from the octave execute command
    $aux = substr($out, $idx_cls+6,12);
    $idx_sec = strpos($aux, '=');
    $cls = substr($aux, 1, $idx_sec-1);
    $relclass = substr($aux, $idx_sec+1, 7);
    $aux_cls[$i]=$cls;
    $aux_relclass[$i]=$relclass;
    
    // Print the first five classes together with graph legend
    //switch($i)
    //{
    //case 1:
      //$ret .= $cls."--->". $relclass."(blue line)"."<br/>"; //The first matched class

    //break;
    //case 2:
      //$ret .= $cls."--->". $relclass."(green line)"."<br/>"; //The second matched class
    //break;
    //case 3:
      //$ret .= $cls."--->". $relclass."(cyan line)"."<br/>"; //The third matched class
    //break;
    //default:
      //$ret .= $cls."--->". $relclass."<br/>";
    //break;
    //}
}
return;