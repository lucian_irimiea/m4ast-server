<?php

// Extract orbital elements from astorb.dat file
// Input: - $obj_designation = either name or object number
//		example for the names : Fortuna, 2001 QT166, 5059_T-3, 5059 T-3
// Output:- $obj_designation = modified and returned as output in format for IMCCE querry
//			meaning number, if the asteroid is numberd or name in format 2001_QT166
//	 - Orbital elements
//	     	$Epoch_obj =  Epoch of osculation 'YYYYMMDD'
//	     	$M_obj = Mean anomaly in degres
//	     	$lomega_obj = Argument of perihelion
//	     	$omega_obj =  Longitude of ascending node
//	     	$incl_obj =  Inclination
//	     	$eccen_obj =  Eccentricity
//	     	$sem_obj = Semimajor axis
// For additional informatins regarding the format of the raw see 
// http://www.naic.edu/~nolan/astorb.html
// Also use the link above to update astorb.dat file

// open file identifier for astrorb.dat
$fdastrorb = fopen(ROOT . "/mvc/library/astorb/astorb.dat","r");
// Issue error if astorb.dat is not in the path
if(!$fdastrorb) View::$alert = "Astorb.dat missing on the server or corrupted";
else
{
    foreach ($Obj as $key => $obj_desig)
    {
        $Obj[$key] = trim($obj_desig);
        if(isset($obj_design[4]) && !empty($obj_design[4]))
        {
            if(trim($obj_desig[4]) == "_")
            {
                // if obj_desig is name with underscore, replace  underscore with space 
                // astorb.dat  object name format
                $obj_desig[4] = " ";
                $Obj[$key] = $obj_desig;
            } 
            if ((strlen(trim($obj_desig)) == 0)||(strlen(trim($obj_desig)) >64))  // empty input
            {
                $obj_designation = ''; // Already empty string, but to keep in mind bad name format
                //trigger_error("Please go back and input an object", E_USER_ERROR);
                unset($Obj[$key]);
            }
        }    
    }


    $Obj_data = ''; // Output vector
    $counter_astorb = 0;// to escape from infinit while loop
    while(!feof($fdastrorb)||($counter_astorb>1e6)) // Until designation of the object is found in astorb.dat
    { 
        $counter_astorb ++; // escape from the loop

        $nwline = fgets($fdastrorb, 4096); // read next line from input file
        $Number_obj = trim(substr ($nwline,0,6));  //Object number, blank if unumbered
        $Name_obj = trim(substr($nwline,7,19)); // Object name
        // Check either object name or object number from astorb line match object designation

        foreach ($Obj as $key => $obj_desig)
        {
            $flag = (!strcmp ($obj_desig, $Number_obj))|| (!strcmp ($obj_desig, $Name_obj));
            if($flag) //object found in astorb.dat
            {
                $Epoch_obj = trim(substr($nwline,106,9)); // Epoch of osculation
                //$Epoch_obj is string with format 'YYYYMMDD'
                // and should extract from this  $EpochY; $EpochM;$EpochD
                $M_obj = trim(substr($nwline,115,11)); //Mean anomaly in degres
                $lomega_obj = trim(substr($nwline,126,11)); // Argument of perihelion in deg.
                $omega_obj = trim(substr($nwline,137,11)); // Longitude of ascending node in deg
                $incl_obj = trim(substr($nwline,148,10)); // Inclination in deg (J2000)
                $eccen_obj = trim(substr($nwline,158,11)); // Eccentricity
                $sem_obj = trim(substr($nwline,169,12)); // Semimajor axis
                $H_obj = trim(substr($nwline,42,6)); // Absolute magnitude
                if (strlen($Number_obj) > 0) // select name or number(if exist) of the object
                {
                  $obj_desig = $Number_obj; // Use forward the number if given
                }
                else // if unumbered use the name
                {
                  $obj_desig = $Name_obj; // Use name of the Object if Unnumber
                }

                $Obj_data[$obj_desig]['found'] = 1;
                $Obj_data[$obj_desig]['Epoch'] = $Epoch_obj;
                $Obj_data[$obj_desig]['M'] = $M_obj;
                $Obj_data[$obj_desig]['lomega'] = $lomega_obj;
                $Obj_data[$obj_desig]['omega'] = $omega_obj;
                $Obj_data[$obj_desig]['incl'] = $incl_obj;
                $Obj_data[$obj_desig]['eccen'] = $eccen_obj;
                $Obj_data[$obj_desig]['sem'] = $sem_obj;
                $Obj_data[$obj_desig]['H'] = $H_obj; 
                unset($Obj[$key]);
            }// end if found
        }// end for each object
    } // end while,for search object in astorb.dat
}
fclose($fdastrorb); // close astorb.dat file identifier