<?php

class NameEquiv {
    
    //Gets equivalent names for a given asteroid
    public function nameeq($ast_name,$ast_num)
    {
        $ast_name = explode(" ",$ast_name);
        if(count($ast_name) == 2)    
        {
            $ast_name = $ast_name[0] . "%20" . $ast_name[1];
            $ast_name = explode(" ",$ast_name);
        }
        $ast_name = $ast_name[0];
        @ $data = file_get_contents("http://www.minorplanetcenter.net/db_search/show_object?utf8=%E2%9C%93&object_id=" . $ast_name);
        
        if($data == NULL)
        {
            $data = file_get_contents("http://www.minorplanetcenter.net/db_search/show_object?utf8=%E2%9C%93&object_id=" . $ast_num);
        }
        
        if($data == NULL)
        {
            fwrite($fderror, $ast_name . " - could not find equivalent names - " . date('y-m-d h:m') . " (Y-M-D H:M)\n");
            return $ast_name;
        }
        
        $data = explode('<div id="main">', $data);
        $data = $data[1];
        $data = explode('</h3>', $data);
        $data = $data[0];
        $data = explode('<h3>', $data);
        $data = $data[1];
        $data = trim($data);
        $data = explode('=',$data);
        $data_aux = explode(')',$data[0]);
        if(count($data_aux) == 2)
        {
            $number = explode("(",$data_aux[0]);
            $number = $number[1];
            $data[0] = $data_aux[1];
        }
        foreach ($data as $key => $value) {
            $data[$key] = trim($value);
            if($data[$key] == "") unset($data[$key]);
        }
        $data = implode(",",$data);
        if(isset($number))
        {
            $data = trim($number) . "," . $data;
        }
        if($data == "Data about an object:")
        {
            $data = $ast_name;
        }
        return $data;
    }
    
}