<?php

$data = file(ROOT . '/mvc/library/astorb/G2V.csv');
foreach($data as $key => $value)
{
    $z0 = explode(',',$value);
    
    $ang_sep_cos = sin($dec*M_PI/180)*sin(trim($z0[2])*M_PI/180) + cos($dec*M_PI/180)*cos(trim($z0[2])*M_PI/180)*cos(($ra-trim($z0[1])/15)*M_PI/12);
    $dist[trim($z0[0])] = acos($ang_sep_cos)*180/M_PI;
    $mag[trim($z0[0])] = round(trim($z0[3])*10)/10;
    
    $number = trim($z0[1])/15;
    require_once ROOT . '/mvc/functions/splitdeg.php';
    $RAg2v[trim($z0[0])] = $mysgn.sprintf("%02d",$dec).':'.sprintf("%02d",$min).':'.sprintf("%.2f",$sec);;  
    
    $number = trim($z0[2]);
    require_once ROOT . '/mvc/functions/splitdeg.php';
    $DEg2v[trim($z0[0])] = $mysgn.sprintf("%02d",$dec).':'.sprintf("%02d",$min).':'.sprintf("%.2f",$sec);;  
}
$dist_min = min($dist);
foreach($dist as $key => $value)
{
    if($value==$dist_min) $stardesig = $key;
}
$magstar = $mag[$stardesig];
$out['s'] = $stardesig;
$out['m'] = $magstar;
$out['d'] =  round($dist_min*10)/10;
$out['ra'] = $RAg2v[$stardesig];
$out['de'] = $DEg2v[$stardesig];