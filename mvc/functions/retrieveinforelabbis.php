<?php

$fidr = fopen(ROOT.'/mvc/library/Relab/catalogues/Sample_Catalogue.txt','r');

// Check if Sample_Catalogue exists
if($fidr == 0)
{
    View::$alert = 'Sample_Catalogue.txt missing in' . ROOT . '/mvc/library/Relab/catalogues/';
}
else
{
    while(!feof ($fidr))
    {
        $temp = fgets($fidr);
        $temp = ' ' . $temp;
        $ppos = strpos($temp, strtoupper(trim($sampleid.'	')));
        if($ppos > 0)
        {
            $flag = 1;
            while($flag)
            {
                $pos = strpos($temp, '		');
                if ($pos > 0)
                    $temp = substr($temp, 0, $pos+1) . '-' . substr($temp, $pos+1, strlen($temp)-$pos); 
                else
                    $flag = 0; 
            }

            // Generate table headers
            $headers2 = array(
                '0' => 'SampleID',
                '1' => 'SampleName',
                '2' => 'PI',
                '3' => 'SI',
                '4' => 'SI2',
                '5' => 'PO',
                '6' => 'GeneralType',
                '7' => 'Type',
                '8' => 'SubType',
                '9' => 'MinSize',
                '10' => 'MaxSize',
                '11' => 'Texture',
                '12' => 'Origin',
                '13' => 'Location',
                '14' => 'Chem#',
                '15' => 'Text'
            );

            $z3 = explode('	', $temp);
            @$fields2[0] = $z3[0];
            @$fields2[1] = $z3[1];
            @$fields2[2] = $z3[2];
            @$fields2[3] = $z3[3];
            @$fields2[4] = $z3[4];
            @$fields2[5] = $z3[5];
            @$fields2[6] = $z3[6];
            @$fields2[7] = $z3[7];
            @$fields2[8] = $z3[8];
            @$fields2[9] = $z3[9];
            @$fields2[10] = $z3[10];
            @$fields2[11] = $z3[11];
            @$fields2[12] = $z3[12];
            @$fields2[13] = $z3[13];
            @$fields2[14] = $z3[14];
            @$fields2[15] = $z3[15];
        }
    }
    fclose($fidr);
}