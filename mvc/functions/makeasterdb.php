<?php

    // Insert AsterSpec.log into sql database
    
    // Prepare the sql
    // Sequence used to determine errors in database update---------------------
    $sql = "SELECT * FROM m4ast_database
            ";
    
    $old = 0;// Number of entries already in table
    if (false !== $result = $this->db->query($sql)) 
    {
        while ($row = mysqli_fetch_assoc($result)) 
        {
            $old++;
        }
    }
    //End error determination---------------------------------------------------
    
    //Database generation-------------------------------------------------------
    // Open catalog with informations about all database asteroid spectrums;
    $fdcatalog = fopen(ROOT . "/mvc/library/Spectre/AstSpec.log","r");
    $new = 0;// Number of new entries
    $all = 0;//Number of lines in file
    while(!feof($fdcatalog))
    {
        $catalog_line = trim(fgets($fdcatalog)); // get line from the log, clean blank spaces from begining and end
        $fields = explode('|',$catalog_line);

        if($fields[0] !== "")
        {
            $all++;
            $ast_name = trim($fields[1]);
            if(is_numeric($ast_name[0]) && is_numeric($ast_name[1]) && is_numeric($ast_name[2]) && is_numeric($ast_name[3]))
            {
                for($i = 20;$i > 3;$i--)
                {
                    if(isset($ast_name[$i]))
                    {
                        $ast_name[$i+1] = $ast_name[$i];
                    }
                }
                $ast_name[4] = " ";
            }
            $fields[1] = $ast_name;

            // Prepare the SQL 
            $sql = "INSERT INTO `m4ast`.`m4ast_database` (`FILE_NAME`, `O_DESIG`, `O_NR`, `OBSV_DATE`, `UAI`, `L_MIN`, `L_MAX`, `PUBLISHED`, `NAME_EQUIV`) 
                VALUES ('" . $this->db->escape($fields[0]) . "',
                        '" . $this->db->escape($fields[1]) . "',
                        '" . $this->db->escape($fields[2]) . "',
                        '" . $this->db->escape($fields[3]) . "',
                        '" . $this->db->escape($fields[4]) . "',
                        '" . $this->db->escape($fields[5]) . "',
                        '" . $this->db->escape($fields[6]) . "',
                        '" . $this->db->escape($fields[7]) . "',
                        '" . $this->db->escape($fields[8]) . "'    
                        );
                ";
            // Execute the query
            if (false !== $result = $this->db->query($sql)) 
            {
                View::$success = 'Database updated';
                $new++;
            }
        }
    }
    //End db generation---------------------------------------------------------
    
    //Notifications-------------------------------------------------------------
    if($old + $new < $all)
    {
        // Error
        $n = $all - $old - $new;// Number of lines where error was encountered
        View::$alert = 'Update encountered an error on ' . $n . ' lines';
    }
    View::$info = 'Number of entries already in table: '.$old.'<br>New lines added: '.$new;
    if($old == $all)
    {
        View::$info .= '<br><br>No new lines added<br>Database up to date';
    }
    else 
    {
        View::$info .= '<br>New number of entries in database: '.$all;
    }
    //End notifications---------------------------------------------------------
    
    // Close catalog with informations about all database asteroid spectrums;
    fclose($fdcatalog);