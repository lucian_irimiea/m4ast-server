<?php

// See Meeus 1991 p.151 
/*
----------------------------------------------------------------------------
 Calculeaza coord geoc rect si ecuat ale Soarelui, cunoscind JD.
  v.9 - mai scoate "diamap" diam aparent (') si DistPam (dist S-P in AU) 
  v.9 - updatat cateva formule dupa Meeus 1991 p.151                        
----------------------------------------------------------------------------
*/

$t = ($JD-2451545)/36525;
$rl =  280.46645 + 36000.76983*$t + 0.0003032*pow($t,2); //mean long
$rm = 357.52910 + 35999.05030*$t - 0.0001559*pow($t,2) - 0.00000048*pow($t,3); //mean anom
$re = 0.016708617 - 0.000042037*$t - 0.0000001236*pow($t,2); //ecc Earth orbit
$rmr = $rm*M_PI/180;
$c = (1.91946-0.004817*$t-0.000014*pow($t,2))*sin($rmr) + (0.019993-0.000101*$t)*sin(2*$rmr) +  0.000290*sin(3*$rmr);//eq centr
$rlsun = $rl + $c; //true long
$rlsunr = $rlsun*M_PI/180;
$v = $rm + $c; //true anom
$vr = $v*M_PI/180;
$ro = 1.000001018*(1-pow($re,2))/(1+$re*cos($vr)); //dist Pam-Soare UA
$rla = $rlsun;
$rlar = $rla*M_PI/180;

require_once ROOT . '/mvc/functions/epsilonmp.php';

$ep = $epsilon;
$epr = $ep*M_PI/180;

$alfa = atan(cos($epr)*sin($rlar)/cos($rlar));
$delta = atan(sin($epr)*sin($rlar)/sqrt(1-pow((sin($epr)*sin($rlar)),2)));
$rsa = cos($epr)*sin($rlar)/cos($delta);

if($rsa>=0)
{
        $raa =1;
}
else
{
        $raa = -1;
}
if ($alfa>=0)
{
        $rbb = 1;
}
else
{
        $rbb = -1;
}
$alfa += M_PI-($raa+$rbb)*M_PI/2;

$xsun = $ro*cos($alfa)*cos($delta);
$ysun = $ro*sin($alfa)*cos($delta);
$zsun = $ro*sin($delta);
// the fallowing data are not used in our program    
/*
$DistPam = $ro;

$alfa = $alfa*180/M_PI;
$delta = $delta*180/M_PI;
$alfasun = $alfa/15.0;
$deltasun = $delta;

$RazaS = 696000; //km}
$DiamAp =2.0* atan($RazaS/($ro*149597870))* 60* 180.0/M_PI; //minutes
*/