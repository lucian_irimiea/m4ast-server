<?php

// Read spectrum file and extract the wavelength, the Reflectance and standard deviation(if given)
// Input: ufile -> file name (together with the path) that contain the spectrum
//	line_skipe -> number of lines to be skiped from the beginig of the file (a possible header)
// Output: landa -> array that contain the wavelengths
//	Refl -> array that contain the reflectances at th given wavelength
//	std_dev -> [optional-depend on input file] standard deviation

$fidin = fopen($_FILES["userfile"]["tmp_name"],"r"); // open user file with the spectrum
while($line_skipe) // throw dummy lines
{
    fgets($fidin, 4096);
    $line_skipe--;
}

$k = 0; // index for spectrum vectors (wavelength and reflectance )
while (!feof($fidin)) // read until end of input file
{
    $aux = trim(fgets($fidin)); // get next line, clear blank spaces from the beginig and end of line
    // Keep only the ascii characaters with the code between 48 ('0') si 122('z')
    $in_str=$aux;
    $j = 0;
    $out = "";
    for($i=0; $i<=strlen($in_str)-1; $i++)
    // for the entire length of the string
    {
            if ((ord($in_str[$i]) > 47) && (ord($in_str[$i]) < 123) && ((ord($in_str[$i]) == 32) | (ord($in_str[$i]) == 9)))
            {
    // keep only characters with the ascii code between [48:122]  
                    $out[$j] = $in_str[$i];
                    $j++;
            }
    }
     // convert array to vector if required
    if(is_array($out))
    $in_str = implode($out);
    $out = $in_str;
    $aux=$out;
    if(strlen($aux)) // if not empty line
    {
        //split in wavelength, reflectance, (std, indicator)
        // The spliting character could be space (ascii code 32) or tab (ascii code 9)
        $aux1 = explode(" ",$aux);// spliting character space (default)
        $aux2 = array_values(array_filter($aux1));
        if(count($aux2) < 2) //splitting caracter tab
        {
            $aux1 = explode("\t",$aux);
            $aux2 = array_values(array_filter($aux1));  
        }
        // Get landa, Refelectance and standard deviation
        if(count($aux2)==2) // only 2 colums, landa and reflectance
        {
            $landa[$k] = $aux2[0];
            $Refl[$k] = $aux2[1];
            $k++; // increment idx for the spectrum vectors
        }
        else if(count($aux2)==3) // 3 colums , landa, reflectance and standard deviation
        {
            $landa[$k] = $aux2[0];
            $Refl[$k] = $aux2[1];
            $std_dev[$k] = $aux2[2];
            $k++;        
        }
        else if(count($aux2)==4) // input file with 4 colums, the 4rd is the certinity of the measurements
        {
            if($aux2[3] > 0) // if certinity of the measuremnt is 0 or -1 the measuremnts is not good
            { // keep only the good measurements
             $landa[$k] = $aux2[0];
             $Refl[$k] = $aux2[1];
             $std_dev = $aux2[2];
             $k++;
            }
        }
    }
}
fclose($fidin); // close input file read