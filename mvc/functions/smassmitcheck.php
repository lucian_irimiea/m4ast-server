<?php

// Puts smassmit spectra in same format as the asteroid spectra

//change execution time
ini_set('max_execution_time', 3000);

// Open specrta catalog
$fdcatalog = fopen(ROOT . "/mvc/library/smassmitaux/SmassSpec_prov.log","r");

// Open file for final catalog form
$fdspec = fopen(ROOT . "/mvc/library/smassmit/SmassSpec.log", "w");

// Remove old error file
@unlink(ROOT . "/mvc/library/smassmit/error.log");
$fderror = fopen(ROOT . "/mvc/library/smassmit/error.log","w");
$error = 0;

while(!feof($fdcatalog))
{
    unset($lambda);
    unset($data);
    $catalog_line = trim(fgets($fdcatalog)); // get line from the log, clean blank spaces from begining and end
    $fields = explode('|',$catalog_line);
    $n = trim($fields[0]);// Get number of spectras for asteroid
    
    // Generate final catalog form
    if(count($fields) > 1)
    {
        fwrite($fdspec, sprintf("%s | ", trim($fields[0])));//number of entries
        fwrite($fdspec, sprintf("%s | ", trim($fields[1])));//asteroid number
        fwrite($fdspec, sprintf("%s | ", trim($fields[2])));//asteroid name
        fwrite($fdspec, sprintf("%s | ", trim($fields[3])));//asteroid prov name
    }
    
    $i = 0;
    while($i < $n)   
    {
        // Open spectra file
        $file = file_get_contents(ROOT . "/mvc/library/smassmitaux/spectre/" . trim($fields[$i * 3 + 4]) . ".txt","r");
        
        // Seperate files by type
        $type = explode(".",trim($fields[$i * 3 + 4]));
        switch($type[1])
        {
            case '1':   
                    $data = explode("\n",$file);
                    if(count($data) < 10)
                    {
                        $data = preg_split('/\s+/',$data[0]);
                        for($j = 0 ; $j < count($data) ; $j++)
                        {
                            if($data[$j] == '')
                            {
                                unset($data[$j]);
                            }
                            else
                            {
                                $data_new[] = $data[$j];
                            }
                        }
                        $data = $data_new;
                        unset($data_new);
                        for($j = 1 ; $j < count($data) ; $j = $j + 3)
                        {
                            if($data[$j] > 0)
                            {
                                $lambda[] = $data[$j - 1];
                                $val = $data[$j - 1] . " " . $data[$j] . "\n";
                                $data_aux[] = $val;
                            }
                        }
                        $data = $data_aux;
                        unset($data_aux);
                    }
                    else
                    {
                        foreach($data as $key => $value)
                        {
                            $value = trim($value);
                            if($value != '')
                            {
                                $val = preg_split('/\s+/',$value);
                                if(count($val) == 4)
                                {
                                    unset($val[3]);
                                }
                                if($val[1] > 0)
                                {
                                    $val[0] = number_format((float)$val[0]/10000, 4, '.', '');
                                    $lambda[] = $val[0];
                                    $data[$key] = $val[0] . " " . $val[1] . "\n";
                                }
                                else
                                {
                                    unset($data[$key]);
                                }
                            }
                            else
                            {
                                unset($data[$key]);
                            }
                        }
                    }
                break;
            
            case '9':   
                    $data = explode("\n",$file);
                    if(count($data) < 10)
                    {
                        fwrite($fderror, sprintf("%s\n",trim($fields[$i * 3 +4])));
                        $error = 1;
                        break;
                    }
                    else
                    {
                        foreach($data as $key => $value)
                        {
                            $value = trim($value);
                            if($value != '')
                            {
                                $val = preg_split('/\s+/',$value);
                                if(count($val) == 4)
                                {
                                    unset($val[3]);
                                }
                                if($val[1] > 0)
                                {
                                    $lambda[] = $val[0];
                                    if(count($val) == 3)
                                    {
                                        $data[$key] = number_format((float)$val[0], 4, '.', '') . " " . $val[1] . " " . $val[2] . "\n";
                                    }
                                    else
                                    {
                                        $data[$key] = number_format((float)$val[0], 4, '.', '') . " " . $val[1] . "\n";
                                    }
                                }
                                else
                                {
                                    unset($data[$key]);
                                }
                            }
                            else
                            {
                                unset($data[$key]);
                            }
                        }
                    }
                break;
                
            case 'spfit':
                    $data = explode("\n",$file);
                    if(count($data) < 10)
                    {
                        fwrite($fderror, sprintf("%s\n",trim($fields[$i * 3 +4])));
                        $error = 1;
                        break;
                    }
                    else
                    {
                        foreach($data as $key => $value)
                        {
                            $value = trim($value);
                            if($value != '')
                            {
                                $val = preg_split('/\s+/',$value);
                                if(count($val) == 4)
                                {
                                    unset($val[3]);
                                }
                                if($val[1] > 0)
                                {
                                    $lambda[] = $val[0];
                                    $data[$key] = $val[0] . " " . $val[1] . "\n";
                                }
                                else
                                {
                                    unset($data[$key]);
                                }
                            }
                            else
                            {
                                unset($data[$key]);
                            }
                        }
                    }
                break;

            default:
                    $data = explode("\n",$file);
                    if(count($data) < 10)
                    {
                        $data = implode(' ',$data);
                        $data = preg_split('/\s+/',$data);
                        for($j = 0 ; $j < count($data) ; $j++)
                        {
                            if($data[$j] == '')
                            {
                                unset($data[$j]);
                            }
                            else
                            {
                                $no = 0;
                                for($k = 0 ; $k < strlen($data[$j]) ; $k++)
                                {
                                    if(!in_array($data[$j][$k], array('0','1','2','3','4','5','6','7','8','9','.','-')))
                                    {
                                        unset($data[$j]);
                                        $no = 1;
                                        break;
                                    }
                                }
                                if($no == 0)
                                {
                                    $data_new[] = $data[$j];
                                }
                            }
                        }
                        $data = $data_new;
                        unset($data_new);
                        for($j = 1 ; $j < count($data) ; $j = $j + 4)
                        {
                            if($data[$j] > 0)
                            {
                                $lambda[] = $data[$j - 1];
                                $val = $data[$j - 1] . " " . $data[$j] . " " . $data[$j + 1] . "\n";
                                $data_aux[] = $val;
                            }
                        }
                        $data = $data_aux;
                        unset($data_aux);
                    }
                    else
                    {
                        foreach($data as $key => $value)
                        {
                            $value = trim($value);
                            if($value != '')
                            {
                                $val = preg_split('/\s+/',$value);
                                if(count($val) > 4)
                                {
                                    unset($data[$key]);
                                }
                                else
                                {
                                    if(count($val) == 4)
                                    {
                                        unset($val[3]);
                                    }
                                    if($val[1] > 0)
                                    {
                                        $lambda[] = $val[0];
                                        if(count($val) == 3)
                                        {
                                            $data[$key] = $val[0] . " " . $val[1] . " " . $val[2] . "\n";
                                        }
                                    }
                                    else
                                    {
                                        unset($data[$key]);
                                    }
                                }
                            }
                            else
                            {
                                unset($data[$key]);
                            }
                        }
                    }
                break;
        }

        // Get lambda min max
        $lambda_min = min($lambda);
        $lambda_max = max($lambda);

        // Generate updated smassmit log
        fwrite($fdspec, sprintf("%s | ", trim($fields[$i * 3 + 4])));//file name
        fwrite($fdspec, sprintf("%s | ", trim($fields[$i * 3 + 5])));//publish link
        fwrite($fdspec, sprintf("%s | ", trim($fields[$i * 3 + 6])));//observation date
        fwrite($fdspec, sprintf("%s | ", $lambda_min));//lambda min
        fwrite($fdspec, sprintf("%s | ", $lambda_max));//lambda max
              
        // Remove old spectra file
        @unlink(ROOT . "/mvc/library/smassmit/spectre/" . trim($fields[$i * 3 + 4]) . ".txt");
        
        // Generate header
        $fdoutput = fopen(ROOT . "/mvc/library/smassmit/spectre/" . trim($fields[$i * 3 + 4]) . ".txt", "w");
        fwrite($fdoutput, sprintf("Asteroid designations ( Number, Name, Temporary designation): %s, %s, %s\n", trim($fields[1]),trim($fields[2]),trim($fields[3])));
        fwrite($fdoutput, sprintf("Observation was done on (UT) (format YYYY-MM-DD-hh): %s\n", trim($fields[$i * 3 + 6])));
        fwrite($fdoutput, sprintf("Observation was made by : SMASS survey Website; http://smass.mit.edu/smass.html\n"));
        fwrite($fdoutput, sprintf("At observatory with UAI code: -\n"));
        fwrite($fdoutput, sprintf("Obsv. comments: \"SpeX\" on the NASA Infrared Telescope Facility (IRTF) on Mauna Kea, Hawaii\n"));
        if(trim($fields[$i * 3 + 5]) == "Unpublishd")
        {
            fwrite($fdoutput, sprintf("The spectrum was published in the article: Unpublished\n"));
        }
        else
        {
            fwrite($fdoutput, sprintf("The spectrum was published in the article: http://smass.mit.edu/smass.html#%s\n",trim($fields[$i * 3 + 5])));
        }
        fwrite($fdoutput, sprintf("The normalization of this spectrum was made for the wavelength: -\n"));
        fwrite($fdoutput, sprintf("Original name of the file was the same\n"));
        fwrite($fdoutput, sprintf("Wavelength[um]   Reflectance   Sigma(optional)\n"));
        
        fclose($fdoutput); 
        
        // Add the measurements
        if(isset($data) && !empty($data) && count($data) > 1)
        {
            // Check if spectra monoton, and make it so.
            foreach($data as $key => $value)
            {
                if($key == 0)   $lambdap = 0;
                else            $lambdap = $lambda[$key-1];
                $lambdac = $lambda[$key];
                if($lambdac <= $lambdap)    unset($data[$key]);

            }
            if(count($data) > 1)
            {
                file_put_contents(ROOT . "/mvc/library/smassmit/spectre/" . trim($fields[$i * 3 + 4]) . ".txt",$data,FILE_APPEND); 
            }
            else
            {
                fwrite($fderror, sprintf("%s - Error at checking monotony, all lines deleted in check\n",trim($fields[$i * 3 +4])));
            }
        }
        else
        {
            fwrite($fderror, sprintf("%s\n",trim($fields[$i * 3 +4])));
        }
       
        $i++;
    } 
    
    fwrite($fdspec, sprintf("\n"));//number of entries
}
if($error > 0)
{
    View::$alert = "Files could not be handled<br>" . $fderror;
}

// Rewrite log with added lamda min and max, and UAI code, if possible

fclose($fderror);
fclose($fdspec);
fclose($fdcatalog); 

// Reset execution time
ini_set('max_execution_time', 300);