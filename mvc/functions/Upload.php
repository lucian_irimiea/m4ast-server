<?php

class Upload {

    function my_clear_string($in_str) {
        // Keep only the ascii characaters with the code between 48 ('0') si 122('z')
        // Input: in_str -> string to be cleared
        // Output: String cleared 
        $k = 0;
        $out = "";
        for ($i = 0; $i <= strlen($in_str)-1; $i++) {
            // for the entire length of the string
            if ((ord($in_str[$i]) > 47) & (ord($in_str[$i]) < 123)) {
                // keep only characters with the ascii code between [48:122]  
                $out[$k] = $in_str[$i];
                $k++;
            }
        }
        // convert array to vector if required
        if (is_array($out))
            $in_str = implode($out);
        $out = $in_str;
        return $out; // return cleared string
    }

    // End function my_clear_string

    function stringselect($in_str) {
        $aux = str_split($in_str);

        foreach ($aux as $key => $value)
            if ((ord($value) < 48)) {
                unset($aux[$key]);
            }
        $out = implode($aux); // from array to string
        return $out; //return the result
    }

//-------------------------------------------------
    function getasteroidnames($input, &$asterdesign, &$astername, &$asterno, &$astertmpdesig) {
        // This function use SSODNET service  http://vo.imcce.fr/webservices/ssodnet/?resolver
        // to find if the object is a Solar System object included in the IMCCE database
        // It receives the name of the object given in any standard accepted by the service (see link)
        // It returns the name of the object and the object type
        // c: - object is a comet
        // a: - object is an asteroid
        // SOAP protocol, defined acording to the indications from the site

        $input = $this->stringselect($input); // clear blank spaces
        // prepare the link for VO resolver
        if (!(is_numeric($input))) { // Case the input is temporary designation
            $z0 = substr($input, 0, 4); //extract  the year (first 4 characters)
            $z1 = substr($input, 4, strlen($input) - 4);
            if (is_numeric($z0)) { // if the first 4 characters are number, than is the year and the input given is temporary designation
                $namaster = $z0 . ' ' . $z1; // Character '%20' is the space used in query 
            } else {
                $namaster = $input; // else the input is the name;
            }
        } else {
            $namaster = $input; // else the input is the number
        }
        $inname = $namaster;

        if (!strpos($inname, ':'))
            $inname = 'a:' . $inname; // Default is asteroid

        $param = array(
            'name' => $inname, //Name or designation or number of the target (1)
            'mime' => 'votable', //Mime type of the results
            'epoch' => 'now',
            'maxrec' => 0, //Maximum number of Sso to retrieve. maxrec=0 stands for no limit.
            'ephem' => 0//Boolean to request (=1) or not (=0) the celestial coordinates of the target at epoch
        );
        $from = 'Marcel Popescu';
        // Enables or disables the WSDL caching feature
        ini_set('soap.wsdl_cache_enabled', 1);
        // SOAP version
        $soapVersion = 'SOAP_1_2';
        // SsoDNet namespace
        $namespace = 'http://vo.imcce.fr/webservices/ssodnet';
        // SsoDNet WSDL
        $uriwsdl = $namespace . '/ssodnet.wsdl';

        try {
            // Constructs the client
            $client = new SoapClient($uriwsdl, array('exceptions' => 1, 'soap_version' => $soapVersion));
            // SOAP header
            $header = array('from' => $from, 'hostip' => '');
            $client->__setSoapHeaders(array(new SOAPHeader($namespace, 'clientID', $header)));
            // Call the resolver method
            $response = $client->__soapCall('resolver', array($param));
            // Find the type of the object, acording to returned votable format
            //echo $response->output;
            if (strpos($response->output, 'TD>Comet<')) {
                $z0 = explode('<vot:TD>Comet</vot:TD>', $response->output);
                $objtyp = 'c:'; //Comet 
            } else {
                $z0 = explode('<vot:TD>Asteroid</vot:TD>', $response->output);
                $objtyp = 'a:'; //Asteroid
            }
            //print_r($z0);
            if (count($z0) < 2) { // Return nothing if the object was not found or the server is down
                $asterdesign = '-';
                $astername = '-';
                $asterno = '-';
                $astertmpdesig = '-';
                return;
            }
            // Get Main or usual name of the Solar system object
            //print_r($z0[1]);
            $z1 = explode('<vot:TD>', $z0[1]);
            $z2 = explode('</vot:TD>', $z1[1]);
            $asterdesign = stringselect($z2[0]);

            $z3 = explode('</vot:TD>', $z1[3]);
            $asterno = stringselect($z3[0]); //echo $objdesignbr.'<br>';
            if (is_numeric(substr($asterdesign, 0, 4)))
                $astername = '-';
            else
                $astername = $asterdesign;

            if (is_numeric($asterno)) {
                $z4 = explode('</vot:TD>', $z1[4]);
                $z5 = explode(',', $z4[0]);
                unset($z5[0]);
                $astertmpdesig = implode(',', $z5);
            } else {
                $asterno = '-';
                $z4 = explode('</vot:TD>', $z1[4]);
                $astertmpdesig = $z4[0];
            }
            return;
        } catch (SoapFault $fault) {
            // trigger_error("SOAP Fault: {$fault->getTraceAsString()} (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);

            $asterdesign = '-';
            $astername = '-';
            $asterno = '-';
            $astertmpdesig = '-';
            return;
        }
        $asterdesign = '-';
        $astername = '-';
        $asterno = '-';
        $astertmpdesig = '-';
        return;
    }

//getasteroidnames

    function read_user_spectrum($ufile, $line_skipe, &$landa, &$Refl, &$std_dev) {
// Read spectrum file and extract the wavelength, the Reflectance and standard deviation(if given)
// Input: ufile -> file name (together with the path) that contain the spectrum
//	line_skipe -> number of lines to be skiped from the beginig of the file (a possible header)
// Output: landa -> array that contain the wavelengths
//	Refl -> array that contain the reflectances at th given wavelength
//	std_dev -> [optional-depend on input file] standard deviation

        $fidin = fopen($ufile, "r"); // open user file with the spectrum

        while ($line_skipe) { // throw dummy lines
            fgets($fidin, 4096);
            $line_skipe--;
        }

        $k = 0; // index for spectrum vectors (wavelength and reflectance )
        while (!feof($fidin)) { // read until end of input file
            $aux = trim(fgets($fidin)); // get next line, clear blank spaces from the beginig and end of line
            if (strlen($this->my_clear_string($aux))) { // if not empty line
                //split in wavelength, reflectance, (std, indicator)
                // The spliting character could be space (ascii code 32) or tab (ascii code 9)
                $aux1 = explode(" ", $aux); // spliting character space (default)
                $aux2 = array_values(array_filter($aux1));
                if (count($aux2) < 2) { //splitting caracter tab
                    $aux1 = explode("\t", $aux);
                    $aux2 = array_values(array_filter($aux1));
                }

                // Get landa, Refelectance and standard deviation
                if (count($aux2) == 2) { // only 2 colums, landa and reflectance
                    $landa[$k] = (float) $aux2[0];
                    $Refl[$k] = (float) $aux2[1];
                    $k++; // increment idx for the spectrum vectors
                } else if (count($aux2) == 3) { // 3 colums , landa, reflectance and standard deviation
                    $landa[$k] = (float) $aux2[0];
                    $Refl[$k] = (float) $aux2[1];
                    $std_dev[$k] = (float) $aux2[2];
                    $k++;
                } else if (count($aux2) == 4) { // input file with 4 colums, the 4rd is the certinity of the measurements
                    if ($aux2[3] > 0) { // if certinity of the measuremnt is 0 or -1 the measuremnts is not good // keep only the good measurements
                        $landa[$k] = $aux2[0];
                        $Refl[$k] = $aux2[1];
                        $std_dev = $aux2[2];
                        $k++;
                    }
                }
            }
        }
        fclose($fidin); // close input file read
        return;
    }

// End function read_user_spectrum

    function Spectrum_Normalization($landa_unit, &$landa, &$Refl, &$flag, &$landaN, &$landa_m, &$landa_M) {
// Function to normalize the specrum, and put landa in microns
// This function check also the validity of the input, issuing errors if is not in given format
// Input: - landa_unit -> (1 for microns, 1000 for nanometers), in order to transform landa in microns
//	 - landa -> wavelength
//	 - Refl -> reflectances
// Output: - landa -> wavelength in microns 
//	  - Refl -> reflectances, normalized
//	  - flag -> 0 if input data is valid
//	  - landaN -> wavelength for which we take the Reflectance for normalization
//	  - [landa_m; landa_M] wavelength interval
// *Note; Wavelengths are given in microns;

        $flag = 0; // flag initial 0
        $aux = 999; // dummy variables used to find the closest wavelength to landaN
        $idxN = 0; // index in the array for the value used to normalize
        // Define normalization Wavelength
        if ($landa[0] / $landa_unit < 0.55) { // Visible spectrum includede
            $landaN = 0.55;
        } else if ($landa[0] / $landa_unit < 1.25) { // Only  NearIR and IR spectrum 
            $landaN = 1.25;
        } else if ($landa[0] / $landa_unit < 2.5) { // only IR spectrum  
            $landaN = 2.5;
        } else {
            // the wavelengths does not corespond to interes spectrum
            $landaN = 0;
            $flag = 1; // Data is not valid
            View::$alert = "Wavelength outside the interval<br>";  //issue an error
        }

        // test if landa is sorted in low to high order,
        $landa1 = $landa; // auxiliary variable used to check if wavelength is monoton
        sort($landa1); // sort the wavelength in monoton order
        for ($i = 0; $i < count($landa); $i++) { // for all elements of the array vector
            $flag = $flag + abs($landa1[$i] - $landa[$i]); // if landa is not monoton, increment flag
            if (abs($landa1[$i] - $landa[$i])) {
                // Issue an error if landa is not Monoton
                View::$alert = View::$alert . "Wavelength not monoton at line " . $i . "<br>";
            }

            $landa[$i] = $landa[$i] / $landa_unit; // landa transformed in microns

            if (abs($landa[$i] - $landaN) <= $aux) { // find Normalization Value
                // find the closest value of landa to landaN
                $aux = abs($landa[$i] - $landaN);
                $idxN = $i; // Index in array for normalization value
            }
        }
        if (!$flag) { // If no Error issued until now , normalize the spectrum
            $normaliz_value = $Refl[$idxN];
            for ($i = 0; $i < count($landa); $i++) { // for all elements of the spectrum
                $Refl[$i] = $Refl[$i] / $normaliz_value;  // Normalize the spectrum with the normalization value
            }
            $landa_m = $landa[0]; // minimum and maximum value of the wavelength interval
            $landa_M = $landa[count($landa) - 1];
        } else {
            // Else,data is not valid issue an error
            View::$alert = View::$alert . "Input file could not be handled";
        }
        return;
    }

// End function Spectrum_Normalization

    function getmastfield($logline, &$fileid, &$AsterDesig, &$AsterNo, &$Date, &$UAIcode, &$lmin, &$lmax, &$adsbs) {
// Separate fields from $logline
        $z0 = explode("|", $logline); // see format of line from AstSpec.log
        $fileid = $z0[0];
        $AsterDesig = $z0[1];
        $AsterNo = $z0[2];
        $Date = $z0[3];
        $UAIcode = $z0[4];
        $lmin = $z0[5];
        $lmax = $z0[6];
        $adsbs = $z0[7];
        //print_r($z0);
        return;
    }

    function jdohmp($d, $mo, $an) {
// Computes Julian day at 0h
// Converted from pb2c pascal version
        /* -------------------------------------------------------------------------
          Calculeaza data iuliana(jd0h)corespunzatoare unei date calendaristice
          cunoscuta, introducind d-ziua,mo-luna,y-anul la ora 0h TU a zilei
          a fost verificat
          -------------------------------------------------------------------------
         */
        if ($mo > 2) {
            $a = floor($an / 100);
            $jd1 = floor(365.25 * $an) + floor(30.6001 * ($mo + 1)) + $d + 1720994.5;
            if ($jd1 >= 2299161) {
                $jd1 = $jd1 + 2 - $a + floor($a / 4);
            }
            $jd0h = $jd1;
        } else {
            $an = $an - 1;
            $mo = $mo + 12;
            $a = floor($an / 100);
            $jd1 = floor(365.25 * $an) + floor(30.6001 * ($mo + 1)) + $d + 1720994.5;
            if ($jd1 >= 2299161) {
                $jd1 = $jd1 + 2 - $a + floor($a / 4);
            }
        }
        $jd0h = $jd1;
        return $jd0h;
    }

    function jdaymp($d, $mo, $an, $ora, $minut, $sec) {
// Computes Julian Day for any moment of the day
// Converted from pb2c pascal version
        /* --------------------------------------------------------------------------
          calculeaza data iuliana cunoscind ora(ora), minutul(minut), secunda(sec),
          fusul orar(fus), ora de vara(vara=1 daca sintem in perioada ofic. a
          orei de vara=0,contrar) apelind functia jd0h(zi-d-,luna-mo-,an-an-)
          a fost verificat impreuna cu jd0h
          --------------------------------------------------------------------------
         */
        $jdrest = ($ora + $minut / 60 + $sec / 3600) / 24;
        $jday = $this->jdohmp($d, $mo, $an) + $jdrest;
        return $jday;
    }

    function getjd($Date) {
        $Date = trim($Date);
        $z0 = explode("-", $Date);
        $year = $z0[0];
        $mo = $z0[1];
        $z1 = $z0[2];
        $z2 = explode(":", $z1);
        $day = $z2[0];
        $ho = $z2[1];
        $min = $z2[2];
        $sec = 0;
        $JD = $this->jdaymp($day, $mo, $year, $ho, $min, $sec);
        return $JD;
    }

    function miriadequareymps($nomaster, $JD, $UAI_code, &$RAimcce, &$DECimcce) {
//--------------------------------------------------------------
// This function implement a SOAP client
// It use Miriade Web service
// For more informations see http://vo.imcce.fr/webservices/miriade/?ephemcc#WS
// Inputs: - $JD = Julian Day
//	  - $nomaster = number or name of the desired asteroid in IMCCE format (eg: Ceres or 1999_TC36)
//	  - $JDstep: time step(interval ) between ephemrides, given in days
//	  - $UAI_code = Observatory Codes: http://www.imcce.fr/en/ephemerides/lieugeo/UAI/uai_obscodes.php
//	  - $mynbd = number of dates for each to get the ephemerides
// Outputs: - $RAimcce, Right ascension of the desired object, array, with JDidx +5000 elements (RA in hours with decimal) 
//	   - $DECimcce, Declination degrees with decimals, array with JDidx +5000 elements (DEC)
//	   - $VMag as reporetd by Miriade Server
//*The last 5000 elements from the two output arrays are computed during current run of this function
//--------------------------------------------------------------
        // Client's ID: provide the name of your project or organisation or yourself
        $from = 'M4AST';
        //$mynbd = 5000;// number of dates (5000 is maximum)

        $z0 = substr($nomaster, 0, 4);
        $z1 = substr($nomaster, 4, strlen($nomaster) - 4);
        if (is_numeric($z0) && (!is_numeric($nomaster))) {
            $nomaster = $z0 . '_' . $z1;
        }
// Input parameters
        $param = array(
            'name' => 'a:' . $nomaster, //name or number of the asteroid
            'type' => "",
            'epoch' => $JD, // Julian Day for the first ephemeride moment
            'nbd' => 1, // Number of the output ephemerides
            'step' => '1' . 'd', //time step given in days
            'tscale' => 'UTC', // timescale
            'observer' => $UAI_code, // UAI code
            'theory' => 'INPOP',
            'teph' => 1,
            'tcoor' => 1,
            'rplane' => 1,
            'mime' => 'text',
            'output' => '--jd,--rv'
        );

        // Enables or disables the WSDL caching feature
        ini_set('soap.wsdl_cache_enabled', 1);
        // SOAP version
        $soapVersion = 'SOAP_1_2';
        // SkyBoT namespace
        $namespace = 'http://vo.imcce.fr/webservices/miriade';
        // SkyBoT WSDL
        $uriwsdl = $namespace . '/miriade.wsdl';

        try {
            // Constructs the client
            $client = new SoapClient($uriwsdl, array('exceptions' => 1, 'soap_version' => $soapVersion));
            // SOAP header
            $header = array('from' => $from, 'hostip' => '');
            $client->__setSoapHeaders(array(new SOAPHeader($namespace, 'clientID', $header)));
            // Call the resolver method;
            $response = $client->__soapCall('ephemcc', array($param));
        } catch (SoapFault $fault) {
            // if querry response not valid issue a warnning 
            View::$alert = "SOAP Fault: {" . $fault->getTraceAsString() . "} (faultcode: {" . $fault->faultcode . "}, faultstring: {" . $fault->faultstring . "}";
        }

        // Get data from querry Response
        //--------------------------------------------------------------
        // Extract Right ascension and declination from querry response
        //Get result in a long string
        $DBcontent = $response->result;
        //echo $DBcontent;

        if (strlen($DBcontent) < 50)
            View::$alert = "Could not get the complet response from Miriade Server";
        $arr_idx = (floor($JD * 100)) / 100;
        $arr_idx = $arr_idx . '';

        $z0 = (floor($JD * 100)) / 100;
        $z1 = '/' . ' ' . $z0 . "." . '/';
        $z2 = preg_split($z1, $DBcontent); // find the position of the Julian Day
        if (count($z2) > 1) { // if Querry response is valid, Julian Day is in the querry
            $z22 = substr($z2[1], 0, 80); // substract the line that begins with floor(JD)
            // Format of the line is "72618564824 2 1 52.33970 +16 17 30.4651 1.603531047 19.31 1."
            $unflag = 1; // clean multiple blank space
            while ($unflag) {
                $pos = strpos($z22, '  ');
                if ($pos > 0)
                    $z22 = substr($z22, 0, $pos + 1) . substr($z22, $pos + 2, strlen($z22) - $pos);
                else
                    $unflag = 0;
            }// only one blank space should remain after that
            // a bug appear when using array_filter,0 were eleiminated
            $z3 = explode(" ", $z22); // see format of $z22
            $z4 = array_values($z3);
            $RAimcce = $z4[1] + $z4[2] / 60 + $z4[3] / 3600; // compute Right Ascension hour with decimal
            // Compute declination, hour with decimals
            if ($z4[4][0] . '1' < 0) {//If sign is -
                $DECimcce = (abs($z4[4]) + $z4[5] / 60 + $z4[6] / 3600) * (-1);
            } else {// sign is +
                $DECimcce = $z4[4] + $z4[5] / 60 + $z4[6] / 3600;
            }
        }// end if queery response valid when processing(this is absolutley necessary with trigger_erro Could not ...)
        $RAimcce = $RAimcce * 15 * cos($DECimcce / 180 * M_PI);
        $RAimcce = sprintf("%.7f", $RAimcce);
        $DECimcce = sprintf("%.7f", $DECimcce);
        return;
    }

//END miriadequareymps
//////////////////////////////////////////

    function writevolog($logline) {
// This function is used to write the VOlog catalog
// I kept the same function used to transform the existing catalog (31 -10 -2011) in to VOlog form, in this way some things can be recursive
// Input: $logline - from AstSpec.log catalog
// Output:  it adds in M4ASTlogToVO.log  catalog

        $fid = fopen(ROOT . '/mvc/library/Spectre/M4ASTlogToVO.log', 'a');

//1989YK8_20030330_568_00 | 1989YK8 | 5480 | 2003-03-30:13:41 | 568 | 0.787 | 2.478 | http://adsabs.harvard.edu/abs/2004NewA....9..343B
        $fileid = '';
        $AsterDesig = '';
        $AsterNo = '';
        $Date = '';
        $UAIcode = '';
        $lmin = '';
        $lmax = '';
        $adsbs = '';
        $this->getmastfield($logline, $fileid, $AsterDesig, $AsterNo, $Date, $UAIcode, $lmin, $lmax, $adsbs); // separate fields from AstSpec.log, line catlog

        $AccessReference = 'http://m4ast.imcce.fr/m4ast/Spectre/' . trim($fileid) . '.txt';

        $AccessFormat = 'native';

        $AccessSize = filesize(ROOT . '/mvc/library/Spectre/' . trim($fileid) . '.txt'); // in bytes
        $AccessSize = round($AccessSize / 1000);

        $DatasetDataModel = 'Spectrum-1.0';

        $z0 = file(ROOT . '/mvc/library/Spectre/' . trim($fileid) . '.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        $DatasetLength = count($z0) - 9;

        $DataIDTitle = trim($AsterDesig) . '/' . trim($AsterNo) . ' Observed at(UAI code):' . trim($UAIcode);
        $DataIDCreator = 'M4AST';
        $DataIDCreatorDID = trim($fileid);

        $CurationPublisher = 'M4AST';
        $CurationReference = trim($adsbs);

        $CoordSysSpaceFrameName = 'J2000.0';

        $JD = $this->getjd($Date);
        $MJD = $JD - 2400000.5;
        $CharTimeAxisCoverageLocationValue = sprintf("%.5f", $MJD);

        $RAimcce = '';
        $DECimcce = '';
        $AsterDesig = trim($AsterDesig);
        $UAIcode = trim($UAIcode);
        if (is_numeric(trim($AsterNo))) {
            $this->miriadequareymps(trim($AsterNo), $JD, $UAIcode, $RAimcce, $DECimcce);
        } else {
            $this->miriadequareymps($AsterDesig, $JD, $UAIcode, $RAimcce, $DECimcce);
        }
        $CharSpatialAxisCoverageLocationValue = $RAimcce . ';' . $DECimcce;

        $CharSpatialAxisCoverageBoundsExtent = sprintf("%.4f", 1 / 3600);

        $midpoint = 1e-6 * ($lmin + $lmax) / 2;
        $CharSpectralAxisCoverageLocationValue = sprintf("%.2e", $midpoint);

        $spewidth = ($lmax - $lmin) * 1e-6;
        $CharSpectralAxisCoverageBoundsExtent = sprintf("%.2e", $spewidth);
        $CharSpectralAxisCoverageBoundsStart = sprintf("%.2e", $lmin * 1e-6);
        $CharSpectralAxisCoverageBoundsStop = sprintf("%.2e", $lmax * 1e-6);


        $outline = $AccessReference . ' | ' . $AccessFormat . ' | ' . $AccessSize . ' | ' . $DatasetDataModel . ' | ' . $DatasetLength . ' | ' . $DataIDTitle . ' | ' . $DataIDCreator . ' | ' . $DataIDCreatorDID . ' | ' . $CurationPublisher . ' | ' . $CurationReference . ' | ' . $CoordSysSpaceFrameName . ' | ' . $CharSpatialAxisCoverageLocationValue . ' | ' . $CharSpatialAxisCoverageBoundsExtent . ' | ' . $CharSpectralAxisCoverageLocationValue . ' | ' . $CharSpectralAxisCoverageBoundsExtent . ' | ' . $CharSpectralAxisCoverageBoundsStart . ' | ' . $CharSpectralAxisCoverageBoundsStop . ' | ' . $CharTimeAxisCoverageLocationValue . "\n";

        fwrite($fid, $outline);
        fclose($fid);
        return;
    }

}