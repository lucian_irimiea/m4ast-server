<?php

// Function to normalize the specrum, and put landa in microns
// This function check also the validity of the input, issuing errors if is not in given format
// Input: - landa_unit -> (1 for microns, 1000 for nanometers), in order to transform landa in microns
//	 - landa -> wavelength
//	 - Refl -> reflectances
// Output: - landa -> wavelength in microns 
//	  - Refl -> reflectances, normalized
//	  - flag -> 0 if input data is valid
//	  - landaN -> wavelength for which we take the Reflectance for normalization
//	  - [landa_m; landa_M] wavelength interval
// *Note; Wavelengths are given in microns;

$flag = 0; // flag initial 0
$aux_n = 999; // dummy variables used to find the closest wavelength to landaN
$idxN = 0; // index in the array for the value used to normalize
// Define normalization Wavelength
if ($landa[0] / $landa_unit < 0.55) { // Visible spectrum includede
    $landaN = 0.55;
} else if ($landa[0] / $landa_unit < 1.25) { // Only  NearIR and IR spectrum 
    $landaN = 1.25;
} else if ($landa[0] / $landa_unit < 2.5) { // only IR spectrum  
    $landaN = 2.5;
} else {
    //the wavelengths does not corespond to interes spectrum
    $landaN = 0;
    $error = 1;
    $flag = 1; // Data is not valid
    View::$alert = "Wavelength outside the interval<br>";
}
// test if landa is sorted in low to high order,
$landa1 = $landa; // auxiliary variable used to check if wavelength is monoton
sort($landa1); // sort the wavelength in monoton order
for ($i = 0; $i < count($landa); $i++) { // for all elements of the array vector
    $flag = $flag + abs($landa1[$i] - $landa[$i]); // if landa is not monoton, increment flag
    if (abs($landa1[$i] - $landa[$i])) {
        // Issue an error if landa is not Monoton
        $error = 1;
        View::$alert = View::$alert . "Wavelength not monoton at line " . $i . "<br>";
    }

    $landa[$i] = $landa[$i] / $landa_unit; // landa transformed in microns
    if (abs($landa1[$i] - $landaN) <= $aux_n) { // find Normalization Value
        // find the closest value of landa to landaN
        $aux_n = abs($landa1[$i] - $landaN);
        $idxN = $i; // Index in array for normalization value
    }
}
if (!$flag) { // If no Error issued until now , normalize the spectrum
    $normaliz_value = $Refl[$idxN];
    for ($i = 0; $i < count($landa); $i++) { // for all elements of the spectrum
        $Refl[$i] = $Refl[$i] / $normaliz_value;  // Normalize the spectrum with the normalization value
    }
    $landa_m = $landa[0]; // minimum and maximum value of the wavelength interval
    $landa_M = $landa[count($landa) - 1];
} else {
    // Else,data is not valid issue an error
    $error = 1;
    View::$alert = View::$alert . "Input file could not be handled";
}