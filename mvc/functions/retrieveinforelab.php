<?php

$z0 = explode('$',$out);
$z0[0] = '';
$z0 = array_values(array_filter($z0));

$headers = array(
        '0' => 'Index',
        '1' => 'Coef.',
        '2' => 'Relab File',
        '3' => 'Sample ID',
        '4' => 'Sample name',
        '5' => 'GeneralType',
        '6' => 'Type',
        '7' => 'SubType'
    );

for($i=0;$i<count($z0);$i++)
{
    $z1 = explode('#',$z0[$i]);
    $coef = $z1[0];
    $rlfname = $z1[1];
    
    $fidr = fopen(ROOT . '/mvc/library/Relab/catalogues/Sample_Catalogue.txt','r');
    $fflag = 1;
    
    $sampleid = '-'; //Init the output result (in case is not found in Spectra catalog - bad file name input)

    while($fflag)
    {
        $temp = fgets($fidr);
        $temp = ' '. $temp;
        $ppos = strpos($temp, strtoupper(trim($file_name)));
        if($ppos>0)
        {
            $flag = 1;
            while($flag)
            {
                $pos = strpos($temp, '		');
                if ($pos > 0)
                    $temp = substr($temp, 0, $pos+1). '-'.substr($temp, $pos+1, strlen($temp)-$pos); 
                else
                    $flag = 0; 
            }  
            $z3 = explode('	', $temp);
            $sampleid = $z3[1];
        }
        $fflag =  (!feof ($fidr)) || ($ppos>0);
    }
    fclose($fidr);
    
    $smpname = '-'; 
    $smpgtyp = '-'; 
    $smptyp = '-'; 
    $smpstyp = '-'; 
    
    $fidr = fopen(ROOT . '/mvc/library/Relab/catalogues/Sample_Catalogue.txt','r');
    
    while( (!feof ($fidr)))
    {
        $temp = fgets($fidr);
        $temp = ' '. $temp;
        $ppos = strpos($temp, strtoupper(trim($sampleid.'	')));
        if($ppos>0)
        {
            $flag = 1;
            while($flag)
            {
                $pos = strpos($temp, '		');
                if ($pos > 0)
                    $temp = substr($temp, 0, $pos+1). '-'.substr($temp, $pos+1, strlen($temp)-$pos); 
                else
                    $flag = 0; 
            }
            $z3 = explode('	', $temp);
            $smpname = $z3[1];
            $smpgtyp = $z3[6];
            $smptyp = $z3[7];
            $smpstyp = $z3[8];
        } 
    }
    fclose($fidr);
    
    $fields[$i][0]=$i+1;
    $fields[$i][1]=$coef;
    $fields[$i][2]=$rlfname;
    $fields[$i][3]=$sampleid;
    $fields[$i][4]=$smpname;
    $fields[$i][5]=$smpgtyp;
    $fields[$i][6]=$smptyp;
    $fields[$i][7]=$smpstyp;
}