<?php

// Search in asteroid spectrum database catalog for name of the files containing 
// asteroids spectrums according with  some criterias
// The results will be printed on screen

if ((strlen($crit1) == 0)&&(strlen($crit2) == 0)&&(strlen($crit3) == 0)) 
{
    //Information alert
    View::$info="Please give an input parameter to search in database.";
}
 else 
{
    
    if (strlen($crit1) == 0) $crit1 = '|'; else $crit1 = '| '.trim($crit1);
    if (strlen($crit2) == 0) $crit2 = '|'; else $crit2 = '| '.trim($crit2);
    if (strlen($crit3) == 0) $crit3 = '|'; else $crit3 = '| '.trim($crit3);

    // Open catalog with informations about all database asteroid spectrums;
    $fdcatalog = fopen(ROOT . "/mvc/library/Spectre/AstSpec.log","r");
    
    // Generate table headers
    $headers = array(
        '0' => 'File Name',
        '1' => 'Obj. Design',
        '2' => 'Obj. No.',
        '3' => 'Obsv. Date',
        '4' => 'UAI Code',
        '5' => '<font size=5>'.'&#955;'.'</font>'.'<sub>min</sub>',
        '6' => '<font size=5>'.'&#955;'.'</font>'.'<sub>max</sub>',
        '7' => 'Published'
    );
    
    //For each line in the catalog (each line contain info about one file with asteroid spectrum)
    // verify,if it represents a searched spectrum
    $kdisp = 0;
    while(!feof($fdcatalog))
    {
        $catalog_line = trim(fgets($fdcatalog)); // get line from the log, clean blank spaces from begining and end
        $cond1 = strpos(" ".$catalog_line,$crit1); // verify first criteria
        $cond2 = strpos(" ".$catalog_line,$crit2); // verify second criteria
        $cond3 = strpos(" ".$catalog_line,$crit3); // verify third criteria
        //if criteria matches, write file to output
        
        // kdisp = variable that controls the amount of results you can get from file
        if($cond1 && $cond2 && $cond3 && ($kdisp<50))
        {
            // Save data from file row
            $fields[] = explode('|',$catalog_line);
            $kdisp++;
        }
    }
    if($kdisp === 0)
    {
        // No results found
        View::$alert="No results for the entered criteria";
    }
    else
    {
        // Search finished, results found
        $search_done = 1;
        View::$success = "Search successful<br>The following are the files in the database, acording to your criteria:";
    }
    
    // Close catalog with informations about all database asteroid spectrums;
    fclose($fdcatalog);
}