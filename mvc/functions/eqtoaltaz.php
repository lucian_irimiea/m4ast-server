<?php

$ut = (($JD -0.5) - floor($JD-0.5))*24;
$ra = $ra*15;
$dec = $dec*M_PI/180;

$lon = $geocoord['lon']*M_PI/180;
$lat = $geocoord['lat']*M_PI/180;

$JD2000 = 2451545.00000;
$d = $JD - $JD2000;
$lst_c = 100.46 + 0.985647*$d + $geocoord['lon'] + 15*$ut;
$ha = $lst_c - $ra;

$ha = $ha*M_PI/180;

$sin_alt = sin($dec)*sin($lat) + cos($dec)*cos($lat)*cos($ha);
$alt = asin($sin_alt);

$cos_a = (sin($dec) - sin($alt)*sin($lat))/(cos($alt)*cos($lat));

$a = acos($cos_a);
$alt = $alt*180/M_PI;
$a = $a*180/M_PI;

if(sin($ha)<0)
$az = $a;
else
$az = 360-$a;

$altaz['alt'] = $alt;
$altaz['az'] = $az;