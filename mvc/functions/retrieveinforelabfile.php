<?php

$fidr = fopen(ROOT.'/mvc/library/Relab/catalogues/Spectra_Catalogue.txt','r');

// Check if Spectra_Catalogue exists
if($fidr == 0)
{
    View::$alert = 'Spectra_Catalogue.txt missing in' . ROOT . '/mvc/library/Relab/catalogues/';
}
else
{
    while(!feof ($fidr) || ($ppos>0))
    {
        $temp = fgets($fidr);
        $temp = ' ' . $temp;
        $ppos = strpos($temp, strtoupper(trim($rlfname)));
        if($ppos > 0)
        {
            $flag = 1;
            while($flag)
            {
                $pos = strpos($temp, '		');
                if ($pos > 0)
                    $temp = substr($temp, 0, $pos+1) . '-' . substr($temp, $pos+1, strlen($temp)-$pos); 
                else
                    $flag = 0; 
            }

            // Generate table headers
            $headers = array(
                '0' => 'RelabFile',
                '1' => 'SampleID',
                '2' => 'Date',
                '3' => 'ReleaseDate',
                '4' => 'Spectrometer',
                '5' => 'Start',
                '6' => 'Stop',
                '7' => 'Resolution',
                '8' => 'SourceAngle',
                '9' => 'DetectAngle',
                '10' => 'Spinning',
                '11' => 'Aperture',
                '12' => 'Scrambler',
                '13' => 'Depolarizer',
                '14' => 'User',
                '15' => 'NASA_PI/Sponsor',
                '16' => 'ResearchType',
                '17' => 'TextNotes'
            );

            $z3 = explode('	', $temp);
            @$fields[0] = $z3[0];
            @$fields[1] = $z3[1];
            @$fields[2] = $z3[2];
            @$fields[3] = $z3[3];
            @$fields[4] = $z3[4];
            @$fields[5] = $z3[5];
            @$fields[6] = $z3[6];
            @$fields[7] = $z3[7];
            @$fields[8] = $z3[8];
            @$fields[9] = $z3[9];
            @$fields[10] = $z3[10];
            @$fields[11] = $z3[11];
            @$fields[12] = $z3[12];
            @$fields[13] = $z3[13];
            @$fields[14] = $z3[14];
            @$fields[15] = $z3[15];
            @$fields[16] = $z3[16];
            @$fields[17] = $z3[17];
            $sampleid = $z3[1];
        }
    }
    fclose($fidr);
}