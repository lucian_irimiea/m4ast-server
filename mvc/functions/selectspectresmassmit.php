<?php

// Search in SMASSMIT spectrum database catalog for name of the files containing 
// asteroids spectrums according with  some criterias
// The results will be printed on screen

if ((strlen($crit1s) == 0)&&(strlen($crit2s) == 0)&&(strlen($crit3s) == 0)) 
{
    //Information alert
    View::$info="Please give an input parameter to search in database.";
}
 else 
{
    
    if (strlen($crit1s) == 0) $crit1s = '|'; else $crit1s = '| '.trim($crit1s);
    if (strlen($crit2s) == 0) $crit2s = '|'; else $crit2s = '| '.trim($crit2s);
    if (strlen($crit3s) == 0) $crit3s = '|'; else $crit3s = '| '.trim($crit3s);

    // Open catalog with informations about all database asteroid spectrums;
    $fdcatalog = fopen(ROOT . "/mvc/library/smassmit/AstSpec.log","r");
    
    // Generate table headers
    $headers = array(
        '0' => 'File Name',
        '1' => 'Obj. Design',
        '2' => 'Obj. No.',
        '3' => 'Obsv. Date',
        '4' => 'UAI Code',
        '5' => '<font size=5>'.'&#955;'.'</font>'.'<sub>min</sub>',
        '6' => '<font size=5>'.'&#955;'.'</font>'.'<sub>max</sub>',
        '7' => 'Published'
    );
    
    //For each line in the catalog (each line contain info about one file with asteroid spectrum)
    // verify,if it represents a searched spectrum
    $kdisp = 0;
    while(!feof($fdcatalog))
    {
        $catalog_line = trim(fgets($fdcatalog)); // get line from the log, clean blank spaces from begining and end
        $cond1 = strpos(" ".$catalog_line,$crit1s); // verify first criteria
        $cond2 = strpos(" ".$catalog_line,$crit2s); // verify second criteria
        $cond3 = strpos(" ".$catalog_line,$crit3s); // verify third criteria
        //if criteria matches, write file to output
        
        // kdisp = variable that controls the amount of results you can get from file
        if($cond1 && $cond2 && $cond3 && ($kdisp<50))
        {
            // Save data from file row
            $fields[] = explode('|',$catalog_line);
            $kdisp++;
        }
    }
    if($kdisp === 0)
    {
        // No results found
        View::$alert="No results for the entered criteria";
    }
    else
    {
        // Search finished, results found
        $search_done = 1;
        View::$success = "Search successful<br>The following are the files in the database, acording to your criteria:";
    }
    
    // Close catalog with informations about all database asteroid spectrums;
    fclose($fdcatalog);
}