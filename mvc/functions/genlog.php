<?php

// Change execution time
ini_set('max_execution_time', 30000);

$fdcatalog = fopen(ROOT . '/mvc/library/Spectre/AstSpec.log', 'r');
$fdcatalog_new = fopen(ROOT . '/mvc/library/AstSpec.log', 'w');
$fderror = fopen(ROOT . '/mvc/library/error.log', 'w');

while(!feof($fdcatalog))
{
    $catalog_line = trim(fgets($fdcatalog)); // get line from the log, clean blank spaces from begining and end

    $ast_name = explode('|',$catalog_line);
    $ast_num = trim($ast_name[2]);
    $ast_name = trim($ast_name[1]);
    
    if(is_numeric($ast_name[0]) && is_numeric($ast_name[1]) && is_numeric($ast_name[2]) && is_numeric($ast_name[3]))
    {
        for($i = 20;$i > 3;$i--)
        {
            if(isset($ast_name[$i]))
            {
                $ast_name[$i+1] = $ast_name[$i];
            }
        }
        $ast_name[4] = " ";
    }
    
    $nameeq = $this->nameeq->nameeq($ast_name,$ast_num);
    
    fwrite($fdcatalog_new, sprintf("%s | %s\n",$catalog_line,$nameeq));
}
fclose($fderror);
fclose($fdcatalog);
fclose($fdcatalog_new);

// Change execution time back
ini_set('max_execution_time', 300);