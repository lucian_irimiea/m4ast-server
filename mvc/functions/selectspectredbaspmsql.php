<?php

// Search in asteroid spectrum database catalog for name of the files containing 
// asteroids spectrums according with  some criterias
// The results will be printed on screen

if ((strlen($crit1) == 0) && (strlen($crit2) == 0) && (strlen($crit3) == 0)) {
    //Information alert
    View::$info = "Please give an input parameter to search in database.";
} else {

    if (strlen($crit1) == 0)
        $crit1 = '|';
    else
        $crit1 = trim($crit1);
    if (strlen($crit2) == 0)
        $crit2 = '|';
    else
        $crit2 = trim($crit2);
    if (strlen($crit3) == 0)
        $crit3 = '|';
    else
        $crit3 = trim($crit3);
    // Prepare the sql
    $sql = " 
            SELECT      * 
            FROM        m4ast_database as A
            WHERE       A.O_DESIG LIKE '%" . $crit1 . "%' 
               OR       A.O_DESIG LIKE '%" . $crit2 . "%' 
               OR       A.O_DESIG LIKE '%" . $crit3 . "%' 
               OR       A.FILE_NAME LIKE '%" . $crit1 . "%'
               OR       A.FILE_NAME LIKE '%" . $crit2 . "%'
               OR       A.FILE_NAME LIKE '%" . $crit3 . "%'
               OR       A.O_NR LIKE '%" . $crit1 . "%'
               OR       A.O_NR LIKE '%" . $crit2 . "%'
               OR       A.O_NR LIKE '%" . $crit3 . "%' 
               OR       A.NAME_EQUIV LIKE '%" . $crit1 . "%'
               OR       A.NAME_EQUIV LIKE '%" . $crit2 . "%'
               OR       A.NAME_EQUIV LIKE '%" . $crit3 . "%'     
            ORDER BY    A.FILE_NAME
    ";

    // Prepare the data
    $fields = array();

    // Generate table headers
    $headers = array(
        'FILE_NAME' => 'File Name',
        'O_DESIG' => 'Obj. Design',
        'O_NR' => 'Obj. No.',
        'OBSV_DATE' => 'Obsv. Date',
        'UAI' => 'UAI Code',
        'L_MIN' => '<font size=5>' . '&#955;' . '</font>' . '<sub>min</sub>',
        'L_MAX' => '<font size=5>' . '&#955;' . '</font>' . '<sub>max</sub>',
        'PUBLISHED' => 'Published (adsabs.harvard.edu)'
    );
    // Get the result, max 50
    if (false !== $result = $this->db->query($sql)) {

        $kdisp = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            if ($kdisp < 50) {
                $fields[] = $row;
                $kdisp++;
            } else {
                break;
            }
        }
        
        if ($kdisp === 0) {
            // No results found
            View::$alert = "No results for the entered criteria";
        } else {
            // Search finished, results found
            $search_done = 1;
            View::$success = "Search successful<br>The following are the files in the database, acording to your criteria:";
        }
    }
    else 
    {
        View::$alert = 'SQL query returned false';
    }

}