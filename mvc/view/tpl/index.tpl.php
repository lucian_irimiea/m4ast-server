<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<meta name="Author" content="Mirel Birlan">
	<meta name="SCIENCE" content="ASTROMOMY">
	<meta name="GENERATOR" content="Mirel BIRLAN">
        <title><?php echo View::$title; ?></title>
        <link rel="stylesheet" type="text/css" href="/m4ast/mvc/view/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/m4ast/mvc/view/css/bootstrap-theme.min.css" />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="/m4ast/mvc/view/js/bootstrap.min.js"></script>
        <style type="text/css">
        body{
            background: url(/m4ast/mvc/view/tpl/Files/bkg.jpg)center center  no-repeat fixed; 
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            width:100%;
            height:100%;
        }
        font.pills{
            color:#eee;
            padding:15px;
        }
        font.pills:hover{
            color:#3366BB;
            padding:15px;
        }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <h3><font size="76" color="#eee"><b><?php echo View::$title; ?></b></font></h3><br>
                <nav>
                    <ul class="nav nav-pills pull-right">
                        <li role="presentation"><a href="/m4ast/index.php/index/home"><font class="pills" size="5"><b>Home</b></font></a></li>
                        <li role="presentation"><a href="/m4ast/index.php/index/start"><font class="pills" size="5"><b>Start</b></font></a></li>
                        <li role="presentation"><a href="/m4ast/index.php/index/analyze"><font class="pills" size="5"><b>Analyze</b></font></a></li>
                        <li role="presentation"><a href="/m4ast/index.php/index/database"><font class="pills" size="5"><b>Database</b></font></a></li>
                        <!--<li role="presentation"><a href="/m4ast/index.php/index/observing"><font class="pills" size="5"><b>Observing</b></font></a></li>
                        <li role="presentation"><a href="/m4ast/index.php/index/help"><font class="pills" size="5"><b>Help</b></font></a></li>-->
                        <?php if (isset($_SESSION['auth'])):?>
                        <li role="presentation"><a href="/m4ast/index.php/index/admin"><font class="pills" size="5"><b>Admin</b></font></a></li>
                        <li role="presentation"><a href="/m4ast/index.php/index/logout"><font class="pills" size="5"><b>Log out (<?php echo $_SESSION['data']['USERNAME'];?>)</b></font></a></li>
                        <?php else:?>
                        <li role="presentation"><a href="/m4ast/index.php/index/login"><font class="pills" size="5"><b>Login</b></font></a></li>
                        <?php endif;?>
                    </ul>
                </nav>
            </div>
            <br>
            
            
        </div>
        <div class="container">
            <div class="jumbotron" style="position: relative;">
                <?php echo View::$body; ?>
            </div>
            <footer class="footer">
                <p><font color=#eee>&copy; Lucian Irimiea <?php echo date('Y');?></font></p>
            </footer>
        </div>
    </body>
</html>