<p><font size="4">
    Run one of the following tasks and find the result at the bottom of the page.
    <br>
    Click on the resulted file name to access the spectral analyzing tools.
</font></p>
<table cellspacing="3" cellpadding="3">
            
        <td valign="top" style="position: relative; top: 20px;">
            <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
            <div class="btn-group-vertical">
                    <!--<button type="submit" class="btn btn-default" name="page" value="plan">Planning</button>
                    <button type="submit" class="btn btn-default" name="page" value="plan_sem">Semestrial Planning</button>-->
                    <button type="submit" class="btn btn-default" name="page" value="plan_n">Night Planning</button>
                    <button type="button" class="btn btn-default"><a href="/m4ast">
                            <img src="/m4ast/mvc/view/tpl/Files/m4ast.png" width='270'/>
                    </a></button>
            </div>
            </form>
        </td>
        
        <?php switch ($page_num) 
        {
            case "plan":?>
                
                <td width="70s" valign="top" style="position:relative; left:20px;">
                    <h3> 1.Planning:</h3>
                    <h4>Option is not yet implemented<br>We apologize for the inconvenience</h4>
                </td>
        
            <?php break;?>
        
            <?php case "plan_sem":?>
                
                <td width="705" valign="top" style="position: relative; left: 20px;">
                    <h3> 2.Semestrial planning:</h3>
                    <h4>Option is not yet implemented<br>We apologize for the inconvenience</h4>
                </td>
                
            <?php break;?> 
                
            <?php case "plan_n":?>
                
                <td width="705" valign="top" style="position: relative; left: 20px;">
                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                        <h3> 3.Night planning:</h3>
                        <label> Date of observation (UT): </label><br>
                        <div class="form-inline">
                                <label for="inputEmail3" > Year: </label>
                                <select name="yyyy" class="form-control">
                                    <?php if(!empty($yyyy)): ?> <option selected="selected"><?php echo $yyyy;?></option>
                                    <?php endif;?>
                                    <?php for($i=2014;$i<2029;$i++):?>
                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php endfor;?>
                                </select>
                                <label for="inputEmail3" > Month(no.): </label>
                                <select name="mm" class="form-control">
                                    <?php if(!empty($mm)): ?> <option selected="selected"><?php echo $mm;?></option>
                                    <?php endif;?>
                                    <?php for($i=1;$i<13;$i++):?>
                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php endfor;?>
                                </select>
                                <label for="inputEmail3" > Day: </label>
                                <select name="dd" class="form-control">
                                    <?php if(!empty($dd)): ?> <option selected="selected"><?php echo $dd;?></option>
                                    <?php endif;?>
                                    <?php for($i=1;$i<32;$i++):?>
                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php endfor;?>
                                </select>   
                        </div>
                        <div class="form-inline">
                            <label for="inputEmail3" >Faintest app. magnitude (Mag. V): </label>
                            <input type="text" class="form-control" name="maglim"  maxlength="4" size="4" placeholder="limiting magnitude" value="<?php echo $Maglim;?>">            
                        </div>
                        <div class="form-inline">
                            <label for="inputEmail3" >Minimum altitude (deg < 90.0):&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input type="text" class="form-control" name="malt"  maxlength="4" size="4" placeholder="minimum altitude" value="<?php echo $malt;?>">
                        </div>
                        <div class="form-inline">
                            <label for="inputEmail3" >UAI code of the observer: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <input type="text" class="form-control" name="uai"  maxlength="4" size="4" placeholder="UAI code" value="<?php echo $uai;?>">
                        </div>
                        <input type="hidden" name="MAX_FILE_SIZE" value="200000" >
                        <label>Choose file with objects desig. to upload:</label> 
                        <input name="userfile" type="file" ><br>
                        <div class="form-inline">
                            <input type="hidden" name="plan" value="1">
                            <button type="submit" class="btn btn-default" name="page" value="plan_n">Plan observations</button>
                        </div>
                        <br>Please be patient while gathering and sorting information!<br><br>
                    </form>
                </td>
                
            <?php break;?>     
            
            <?php default:?>
                
                <td width="705" valign="top" style="position: relative; left: 20px;">
                    <h3>First select an action from the left menu</h3>
                </td>
                
            <?php break;?> 
                
        <?php }?>
        
</table>        

<?php if (isset(View::$alert) && !empty(View::$alert)): ?>
    <div class="alert alert-danger" role="alert" ><font size="4"><?php echo View::$alert; ?></font></div>
<?php endif; ?>
<?php if (isset(View::$success) && !empty(View::$success)): ?>
    <div class="alert alert-success" role="alert" id="scroll"><font size="4"><?php echo View::$success; ?></font></div>
<?php endif; ?>
<?php if (isset(View::$info) && !empty(View::$info)): ?>
    <div class="alert alert-info" role="alert"><font size="4"><?php echo View::$info; ?></font></div>
<?php endif; ?>

</form>

<?php if ($page_num == "plan_n") 
    { 
        if ($fields != ""):?>
            <table class="table table-striped" style="position: relative; left: -15px;">
                <thead>
                    <tr>
                        <?php foreach ($headers as $headerName): ?>
                            <th><font size=4><?php echo $headerName; ?></font></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($fields as $fieldsData): ?>
                        <tr>
                            <?php foreach (array_keys($headers) as $fieldKey): ?>
                                <td>
                                    <?php if($fieldKey == 0): ?>
                                    <a href="/m4ast/index.php/index/analyze?file_name=<?php echo $fieldsData[$fieldKey]; ?>"target="_blank"><?php echo $fieldsData[$fieldKey]; ?></a>
                                    <?php else:?> 
                                        <?php if($fieldKey == 7 && strlen($fieldsData[$fieldKey]) > 12):?>
                                            <a href="<?php echo $fieldsData[$fieldKey]; ?>"target="_blank"><?php echo $fieldsData[$fieldKey]; ?></a>
                                        <?php else:?>
                                            <?php echo $fieldsData[$fieldKey]; ?>
                                        <?php endif;?>
                                    <?php endif;?>
                                </td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <br> 
            <h4>The following inputs where not found in current version of astorb.dat:</h4><br>
            <?php if(count($Obj)): 
                    foreach($Obj as $key => $value): 
                        echo $value.'<br>';
                    endforeach; 
                  endif; ?>
        <?php endif;?>        
    <?php } ?>