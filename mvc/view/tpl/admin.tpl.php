<p><font size="4">
    Authorized personnel only.<br>
    This tasks can destroy the database, use with caution.<br>
    Run one of the following tasks and find the result at the bottom of the page.<br>
</font></p>
<table cellspacing="3" cellpadding="3">
            
        <td valign="top" style="position: relative; top: 20px;">
            <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
            <div class="btn-group-vertical">
                    <button type="submit" class="btn btn-default" name="page" value="upload">Upload spectrum</button>
                    <button type="submit" class="btn btn-default" name="page" value="upm4ast">Update m4ast database</button>
                    <button type="submit" class="btn btn-default" name="page" value="upsmass">Update SMASS MIT database</button>
                    <button type="submit" class="btn btn-default" name="page" value="adduser">Add new user</button>
                    <button type="submit" class="btn btn-default" name="page" value="pass">Change password</button>
                    <?php if(isset($_SESSION['data']['ADMIN']) && $_SESSION['data']['ADMIN'] == 1 && $_SESSION['data']['USERNAME'] == "Lucian Irimiea" && $_SESSION['data']['PASSWORD'] == "d5f7891d336db7d86b2f3ee7303c3169"):?>
                        <button type="submit" class="btn btn-default" name="page" value="make"><font color="red">Make database</font></button>
                        <button type="submit" class="btn btn-default" name="page" value="removeuser"><font color="red">Remove users</font></button>
                    <?php endif;?>
                    <button type="button" class="btn btn-default"><a href="/m4ast">
                            <img src="/m4ast/mvc/view/tpl/Files/m4ast.png" width='270'/>
                    </a></button>
            </div>
            </form>
        </td>
        
        <?php switch ($page_num) 
        {
            case "upload":?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h3> 1.Upload spectrum:</h3>
                    <form class="form-inline" method="post" action="" enctype="multipart/form-data">
                        <br>
                        <div class="form-group">
                            <label for="inputEmail3">Choose a file:</label>
                            <input type="file" name="userfile">
                            <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
                        </div>
                        <br><br>
                        <div class="form-group">
                            <label for="inputEmail3"><font color="red">*</font>Asteroid Name(Ex: 1917 or Cuyo or 1968 AA) <a href=http://vo.imcce.fr/webservices/ssodnet/?forms target="_blank"> <font size = 3> [Check] </font> </a> :</label><br>
                            <input type="text" class="form-control" name="aster_udesig" value="<?php echo $aster_udesig;?>" maxlength="16" size="93" placeholder="Asteroid name">
                        </div>
                        <br><br>
                        <label><font color="red">*</font>Date of observation (UT):</label><br>
                        <div class="form-group">
                            <input type="date" class="form-control" name="date" value="<?php echo $date;?>" maxlength="16" size="93">
                        </div>
                        <div class="form-group" style="position: relative; left:34px">
                            <label for="exampleInputName2">Hour:</label>
                            <select class="form-control" name="hrs" style="position: relative; left: 10px;">
                                <?php for($i = 0; $i < 24; $i++): ?>   
                                <option value="<?php echo $i; ?>" <?php if($i == $hrs) echo "selected";?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="form-group" style="position: relative; left:75px">
                            <label for="exampleInputName2">Min:</label>
                            <select class="form-control" name="min" style="position: relative; left: 10px;">
                                <?php for($i = 0; $i < 60; $i++): ?>   
                                <option value="<?php echo $i; ?>" <?php if($i == $min) echo "selected";?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <label for="inputEmail3">Person who made the observation:</label><br>
                            <input type="text" class="form-control" name="obsv" value="<?php echo $obsv;?>" maxlength="32" size="42" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3">E-mail:</label><br>
                            <input type="text" class="form-control" name="em_obsv" value="<?php echo $em_obsv;?>" maxlength="32" size="42" placeholder="E-mail">
                        </div>
                        <br><br>
                        <div class="form-group">
                            <label for="inputEmail3"><font color="red">*</font>Observatory (3-digit MPC code):</label><br>
                            <input type="text" class="form-control" name="uai_code" value="<?php echo $uai_code;?>" maxlength="3" size="3" placeholder="Code">
                            <label><a href="http://www.cfa.harvard.edu/iau/lists/ObsCodes.html" target=_blank> <font size = 2> List of Observatories </font> </a></label>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <label for="inputEmail3">Spectrum was published in :<a href="http://adsabs.harvard.edu/abstract_service.html" target=_blank> <font size = 2> ( ADS-ABS ) </font> </a></label>
                            <input type="text" class="form-control" name="article" value="<?php if($article != "Unpublished") echo $article;?>" maxlength="64" size="93" placeholder="Article name">
                        </div>
                        <br><br>
                        <div class="form-group">
                            <label for="inputEmail3">Wavelength was given in:</label>
                            <select class="form-control" name="landa_unit" style="position: relative; left: 6px;">
                                <option value="u" <?php if($landa_unit == "u") echo "selected";?>>um</option>
                                <option value="n" <?php if($landa_unit == "n") echo "selected";?>>nm</option>
                                <option value="a" <?php if($landa_unit == "a") echo "selected";?>>A</option>
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="inputEmail3">Number of lines from the input to be skipped:</label>
                            <input type="text" class="form-control" name="line_skipe" value="<?php echo $line_skipe;?>" maxlength="3" size="3" placeholder="Line nr">
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="inputEmail3">Comments:</label>
                            <input type="text" class="form-control" name="comment" value="<?php echo $comment;?>" maxlength="128" size="93" placeholder="Comments">
                        </div>
                        <br><br>
                        <div class="form-group">
                            <button type="submit" name="page" value="upload" class="btn btn-default" >Submit Spectrum</button><br><br>
                            <label for="inputEmail3">Fields marked with <font color="red">*</font> are required.</label><br>
                            <label for="inputEmail3">The status of the submitted file wile be displayed below.</label><br>
                        </div>
                    </form>
                </td>
        
            <?php break;?>
        
            <?php case "upm4ast":?>    
                
                <td width="705" valign="top" style="position: relative; left: 20px;">
                    <h3>2.Update m4ast database</h3>
                    <p>
                        <font size="4">
                        This action will upload the m4ast spectra catalog in to the sql database.<br>
                        It will not overwrite entries that already exist, so any changes to existing lines in the log will not be registered.
                        </font>
                    </p>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="page" value="upm4ast|update" class="btn btn-default">Update m4ast database</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <p>
                        <font size="5">
                        Last hard reset: <?php echo($date);?><br>
                        </font>
                    </p>
                    <p>
                        <font size="4">
                        This action will upload the m4ast spectra catalog in to the sql database.<br>
                        It <font color="red">WILL</font> overwrite entries that already exist. <br>
                        Any catalog lines that were removed in the updated version of the catalog you are trying to upload, will also be removed in the sql database.<br>
                        Use with caution.<br>
                        <br>
                        This action will take a few minutes, depending on catalog size.
                        </font>
                    </p>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="page" value="upm4ast|hardupdate" class="btn btn-default">HARD update for m4ast</button>
                            </div>
                        </div>
                    </form>
                </td>
                
            <?php break;?> 
                
            <?php case "upsmass":?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h3> 3.Update SMASS MIT database:</h3>
                    <p>
                        <font size="4">
                        This action will upload the SMASS MIT spectra catalog in to the sql database.<br>
                        It will not overwrite entries that already exist, so any changes to existing lines in the log will not be registered.
                        </font>
                    </p>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="page" value="upsmass|update" class="btn btn-default">Update SMASS MIT database</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <p>
                        <font size="4">
                        This action will upload the SMASS MIT spectra catalog in to the sql database.<br>
                        It <font color="red">WILL</font> overwrite entries that already exist. <br>
                        Any catalog lines that were removed in the updated version of the catalog you are trying to upload, will also be removed in the sql database.<br>
                        Use with caution.<br>
                        <br>
                        This action will take a few minutes, depending on catalog size.
                        </font>
                    </p>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="page" value="upsmass|hardupdate" class="btn btn-default">HARD update for SMASS MIT</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <p>
                        <font size="5">
                        Last regeneration: <?php echo($date);?><br>
                        </font>
                    </p>
                    <p>
                        <font size="4">
                        This action will regenerate the SMASS MIT database completely.<br>
                        It <font color="red">WILL</font> delete existing database and rebuild it. <br>
                        Use with caution.<br>
                        <br>
                        This action will take up to <font color="red">one hour</font>, during which the SMASS MIT database will be unusable.
                        </font>
                    </p>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="page" value="upsmass|regenerate" class="btn btn-default">Regenerate SMASS MIT database</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <p>
                        <font size="4">
                        The last action can be done in a sequence, though I recommend using the automatic regeneration.<br>
                        The first action is to download the database.<br>
                        It will not affect the current database.
                        </font>
                    </p>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="page" value="upsmass|download" class="btn btn-default">Download SMASS MIT database</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <p>
                        <font size="4">
                        The second action is to alter the dwnloaded spectra, so they can be usable by the software.<br>
                        It <font color="red">WILL</font> affect the current database.
                        </font>
                    </p>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="page" value="upsmass|check" class="btn btn-default">Check SMASS MIT database</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <p>
                        <font size="4">
                        The last action is to upload th spectra catalog that was previously generated to an sql format<br>
                        It <font color="red">WILL</font> affect the current database.
                        </font>
                    </p>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="page" value="upsmass|update" class="btn btn-default">Upload SMASS MIT database</button>
                            </div>
                        </div>
                    </form>
                </td>
        
            <?php break;?>
            
            <?php case "adduser":?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h3> 4.Add new user:</h3>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="username" placeholder="Username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">New user password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="password" placeholder="New user password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Your password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="pass" placeholder="Your password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" name="page" value="adduser" class="btn btn-default">Register user</button>
                            </div>
                        </div>
                    </form>
                </td>
        
            <?php break;?>    
                
            <?php case "pass":?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h3> 5.Change password:</h3>
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Old password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="oldp" placeholder="Old password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">New password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="newp" placeholder="New password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-3 control-label">Enter password again</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="newp2" placeholder="New password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" name="page" value="pass" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </td>
        
            <?php break;?>    
                
            <?php case "make":?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h3> 5.Make database:</h3>
                    <form class="form-horizontal" method="post" action="">
                        <p>
                            <font size="4">
                            Hidden form. Admin only. Generates database. Use only once. Run twice.
                            </font>
                        </p>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" name="page" value="make|make" class="btn btn-default">Make database</button>
                            </div>
                        </div>
                    </form>
                </td>
        
            <?php break;?>    
                
            <?php case "removeuser":?>
                
                <td width="705" valign="top" style="position:relative; left:20px; top:20px;">
                    <?php if ($fields != ""):?>
                        <div class="panel panel-default">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <?php foreach ($headers as $headerName): ?>
                                            <th><font size=4><?php echo $headerName; ?></font></th>
                                        <?php endforeach; ?>
                                            <th><font size=4>Delete user</font></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($fields as $fieldsData): ?>
                                        <tr>
                                            <?php foreach (array_keys($headers) as $fieldKey): ?>
                                                <td>
                                                    <?php echo $fieldsData[$fieldKey]; ?>
                                                </td>
                                            <?php endforeach; ?>
                                            <td>
                                                <form method="post" action="" style="display: inline-block; text-align: left;">
                                                    <input type="hidden" name="actionId" value="<?php echo $fieldsData['USERNAME']; ?>" />
                                                    <button type="submit" name="page" value="removeuser" class="btn btn-default" aria-label="Left Align">
                                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php endif;?>
                </td>
                
            <?php break;?>        
                
            <?php default:?>
                
                <td width="705" valign="top" style="position: relative; left: 20px;">
                    <h3>First select an action from the left menu</h3>
                </td>
                
            <?php break;?> 
                
        <?php }?>
        
</table>

<div style="position:relative; top:20px;">
    <?php if (isset(View::$alert) && !empty(View::$alert)): ?>
        <div class="alert alert-danger" role="alert" ><font size="4"><?php echo View::$alert; ?></font></div>
    <?php endif; ?>
    <?php if (isset(View::$success) && !empty(View::$success)): ?>
        <div class="alert alert-success" role="alert" id="scroll"><font size="4"><?php echo View::$success; ?></font></div>
    <?php endif; ?>
    <?php if (isset(View::$info) && !empty(View::$info)): ?>
        <div class="alert alert-info" role="alert"><font size="4"><?php echo View::$info; ?></font></div>
    <?php endif; ?>
</div>