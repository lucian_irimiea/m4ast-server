<?php if (isset(View::$alert) && !empty(View::$alert)): ?>
    <div class="alert alert-danger" role="alert" ><font size="4"><?php echo View::$alert; ?></font></div>
<?php endif; ?>
<?php if (isset(View::$success) && !empty(View::$success)): ?>
    <div class="alert alert-success" role="alert" id="scroll"><font size="4"><?php echo View::$success; ?></font></div>
<?php endif; ?>
<?php if (isset(View::$info) && !empty(View::$info)): ?>
    <div class="alert alert-info" role="alert"><font size="4"><?php echo View::$info; ?></font></div>
<?php endif; ?>

<table cellspacing="3" cellpadding="3">
    <td width="705" valign="top" style="position:relative; left:20px;">
        <form class="form-horizontal" method="post" action="">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-5">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Sign in</button>
                </div>
            </div>
        </form>
    <td>   
    <td width="300" valign="top" style="position:relative; left:20px;">
        <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
            <div class="btn-group-vertical">
                <button type="button" class="btn btn-default"><a href="/m4ast">
                        <img src="/m4ast/mvc/view/tpl/Files/m4ast.png" width='270'/>
                </a></button>
            </div>
        </form>
    </td>
</table>        
