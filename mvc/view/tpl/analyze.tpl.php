<?php if (isset(View::$alert) && !empty(View::$alert)): ?>
<div class="alert alert-danger" role="alert" ><font size="4"><?php echo View::$alert; ?></font></div>
<?php endif; ?>
<?php if (isset(View::$success) && !empty(View::$success)): ?>
    <div class="alert alert-success" role="alert" id="scroll"><font size="4"><?php echo View::$success; ?></font></div>
<?php endif; ?>
<?php if (isset(View::$info) && !empty(View::$info)): ?>
    <div class="alert alert-info" role="alert"><font size="4"><?php echo View::$info; ?></font></div>
<?php endif; ?>

<form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
<h4>Spectrum File (from database):</h4>
<input type="text" class="form-control" name="file_name" maxlength="64" size="64" placeholder="File name" value="<?php echo $file_name;?>">
    
<h4>Select action to be performed on spectrum: </h4>

<table cellspacing="3" cellpadding="3">

    <td valign="top" style="position: relative; top: 10px;">
    <div class="btn-group-vertical">
        <button type="submit" class="btn btn-default" name="action_type" value="conc">Concatenate spectra</button>
        <button type="submit" class="btn btn-default" name="action_type" value="plt">Plot spectrum</button>
        <!--<button type="submit" class="btn btn-default" name="action_type" value="scorr">Spectral corrections</button> -->
        <button type="submit" class="btn btn-default" name="action_type" value="tax">Taxonomic classification</button>
        <button type="submit" class="btn btn-default" name="action_type" value="rel">Comparison with Relab database</button> 
        <!--<button type="submit" class="btn btn-default" name="action_type" value="ast">Comparison with asteroids spectra</button>-->
        <button type="submit" class="btn btn-default" name="action_type" value="bmin">Band parameters (if present)</button> 
        <button type="submit" class="btn btn-default" name="action_type" value="clout">Mineralogical analysis</button> 
        <!--<button type="submit" class="btn btn-default" name="action_type" value="all">Run complete analysis</button>-->
        <button type="button" class="btn btn-default"><a href="/m4ast">
                    <img src="/m4ast/mvc/view/tpl/Files/m4ast.png" width='270'/>
        </a></button>
    </div>
    </td>

    <?php 
        if(!empty($file_name) && @$file_exists == 1 && $out != NULL)
    {
        switch ($action_type) 
    {
        case "plt":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
            <h4><b>Plotting spectrum from file: <?php echo $file_name; ?></b></h4>
            <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>.jpg" alt="Spectrum" width="600" height="450" /><br><br>
            <h4><b>Informations about this spectrum:</b></h4><br>
            <?php foreach ($ast_header as $header): ?>
                <h5><?php echo $header; ?></h5>
            <?php endforeach; ?>
            </td>    

        <?php break;?>

        <?php case "conc":?>

            <td width="705" valign="top" style="position:relative; left:20px; top:8px;">
                <p><font size="2" color="blue"> Insert the file name for the spectrum you wish to concatenate with your initial spectrum:</br>
                    ex:  - Spectrum: Heracles_19911214_697_00</br> 
                    You can choose the name of the concatenated spectrum.</br> 
                    Or leave the field blank and receive a generic name.</br>
                </font></p>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-5 control-label">Spectrum you wish to concatenate:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="spec1" maxlength="32" size="32" placeholder="Spectrum" value="<?php echo $rem1;?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-5 control-label">Name of concatenated spectrum:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" name="f_name" maxlength="32" size="32" placeholder="File name" value="<?php echo $rem2;?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-10">
                        <button type="submit" name="action_type" value="conc" class="btn btn-default">Concatenate</button>
                    </div>
                </div>
            </td>

        <?php break;?>     

        <?php case "scorr":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4>Option is not yet implemented<br>We apologize for the inconvenience</h4>
            </td>

        <?php break;?>  

        <?php case "tax":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4>Select the method:</h4>
                <div class="btn-group-horizontal" style="position:relative; top: 5px;">
                    <button type="submit" class="btn btn-default" name="action_type" value="bdm"/>
                        Bus-DeMeo
                    </button> 
                    <a href="http://adsabs.harvard.edu/abs/2009Icar..202..160D" target=_blank>(details)</a>
                    <font size = 3> using </font>
                    <select name="bdm_method">
                        <option value="0" <?php echo $rem1_1;?>>Std. dev.</option>
                        <option value="1" <?php echo $rem1_2;?>>Chi-square</option>
                        <option value="2" <?php echo $rem1_3;?>>Mean sq err</option>
                    </select> 
                </div>
                <div class="btn-group-horizontal" style="position:relative; top: 5px;">
                    <button type="submit" class="btn btn-default" name="action_type" value="g13t"/>
                        G13&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </button>
                    <a href="http://adsabs.harvard.edu/abs/2000Icar..146..204F" target=_blank>(details)</a>
                </div>
                <div class="btn-group-horizontal" style="position:relative; top: 5px;">
                    <button type="submit" class="btn btn-default" name="action_type" value="g9t"/>
                        G9&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </button>
                    <a href="http://adsabs.harvard.edu/abs/2000Icar..146..204F" target=_blank>(details)</a>
                </div>
            </td>    

        <?php break;?>      

        <?php case "g9t":?>

            <?php if($nopointinspec)    {?>
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h4><b>Classification according to G-mode analysis - 9 classes variant </b></h4>
                    <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>.jpg" alt="Spectrum" width="600" height="450" /><br>
                    <h5><?php echo($reliability) ?>%</h5><br>
                    <h4><b>Informations about this spectrum:</b></h4><br>
                    <?php foreach ($ast_header as $header): ?>
                        <h5><?php echo $header; ?></h5>
                    <?php endforeach; ?>
                </td>
            <?php } else {?>
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h4><b>Classification according to G-mode analysis - 9 classes variant </b></h4>
                    <h5>No points for this classification. Wavelength should be less than 1um for this classification.</h5><br>
                    <h4><b>Informations about this spectrum:</b></h4><br>
                    <?php foreach ($ast_header as $header): ?>
                        <h5><?php echo $header; ?></h5>
                    <?php endforeach; ?>
                </td>
            <?php } ?>

        <?php break;?> 

        <?php case "g13t":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4><b>Classification according to G-mode analysis - 13 classes variant </b></h4>
                <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>.jpg" alt="Spectrum" width="600" height="450" /><br>
                <h5><?php echo($reliability) ?>%</h5><br>
                <h4><b>Informations about this spectrum:</b></h4><br>
                <?php foreach ($ast_header as $header): ?>
                    <h5><?php echo $header; ?></h5>
                <?php endforeach; ?>
            </td>

        <?php break;?>

        <?php case "bdm":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4><b>Classification based on Bus - DeMeo taxonomy</b></h4>
                <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>.jpg" alt="Spectrum" width="600" height="450" /><br>
                <h4><b>Informations about Bus-DeMeo classification for this spectrum:</b></h4><br>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><font size=4>Class</font></th>
                            <th><font size=4>Std. dev.</font></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=1; $i<=5; $i++){ ?>
                            <tr>
                                <td><?php echo($aux_cls[$i]); ?></td>
                                <td><?php echo($aux_relclass[$i]); ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <h5>Reliability(Number of points used for comparison/Total number of points) for this classification is: </h5>
                <h5><?php echo($reliability) ?>%</h5><br>
                <h4><b>Informations about this spectrum:</b></h4><br>
                <?php foreach ($ast_header as $header): ?>
                    <h5><?php echo $header; ?></h5>
                <?php endforeach; ?>
            </td>

        <?php break;?>    

        <?php case "rel":?>

            <td width="705" valign="top" style="position: relative; left: 20px;">
                <h4>
                    <a href="http://www.planetary.brown.edu/relabdocs/relab_disclaimer.htm" target="_blank"> <font size = 3>  Details about the Relab database  </font> </a> <br><br>
                </h4>
                <font size = 3 style="position: relative; top: -5px;"> Choose method: </font>
                <select name="model_method" style="position: relative; top: -5px;">
                    <option value="0" <?php echo $rem2_1;?>>Std. dev.</option>
                    <option value="1" <?php echo $rem2_2;?>>Chi-square</option>
                    <option value="2" <?php echo $rem2_3;?>>Corr. coef.</option>
                    <option value="3" <?php echo $rem2_4;?>>Combined</option>
                    <option value="4" <?php echo $rem2_5;?>>Mean sq. err.</option>
                </select>
                <br>
                <div class="btn-group-horizontal" style="position: relative; top: -3px;">
                    <button type="submit" class="btn btn-default" name="action_type" value="relab"/>
                        All spectra
                    </button> 
                    <button type="submit" class="btn btn-default" name="action_type" value="relabm"/>
                        Meteorites spectra
                    </button>
                </div>
            </td>

        <?php break;?>

        <?php case "relab":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4><b>Informations about this spectrum:</b></h4><br>
                <?php foreach ($ast_header as $header): ?>
                    <h5><?php echo $header; ?></h5>
                <?php endforeach; ?><br>
                <h4><b>Matching with spectra from Relab database:</b></h4><br>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <?php foreach ($headers as $headerName): ?>
                                <th><font size=4><?php echo $headerName; ?></font></th>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($fields as $fieldsData): ?>
                            <tr>
                                <?php foreach (array_keys($headers) as $fieldKey): ?>
                                    <td>
                                        <?php if($fieldKey == 2): ?>
                                            <a href="/m4ast/index.php/index/relab?rlfname=<?php echo $fieldsData[$fieldKey]; ?>&faster=<?php echo $file_name; ?>" target="_blank"><?php echo $fieldsData[$fieldKey]; ?></a>
                                        <?php else:?> 
                                            <?php echo $fieldsData[$fieldKey]; ?>
                                        <?php endif;?>
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </td>

        <?php break;?>     

        <?php case "relabm":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4><b>Informations about this spectrum:</b></h4><br>
                <?php foreach ($ast_header as $header): ?>
                    <h5><?php echo $header; ?></h5>
                <?php endforeach; ?><br>
                <h4><b>Matching with spectra from Relab database:</b></h4><br>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <?php foreach ($headers as $headerName): ?>
                                <th><font size=4><?php echo $headerName; ?></font></th>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($fields as $fieldsData): ?>
                            <tr>
                                <?php foreach (array_keys($headers) as $fieldKey): ?>
                                    <td>
                                        <?php if($fieldKey == 2): ?>
                                            <a href="/m4ast/index.php/index/relab?rlfname=<?php echo $fieldsData[$fieldKey]; ?>&faster=<?php echo $file_name; ?> "target="_blank"><?php echo $fieldsData[$fieldKey]; ?></a>
                                        <?php else:?> 
                                            <?php echo $fieldsData[$fieldKey]; ?>
                                        <?php endif;?>
                                    </td>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </td>

        <?php break;?> 

        <?php case "ast":?>    

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4>Option is not yet implemented<br>We apologize for the inconvenience</h4>
            </td>

        <?php break;?> 

        <?php case "bmin":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4><b>Informations about this spectrum:</b></h4><br>
                <?php foreach ($ast_header as $header): ?>
                    <h5><?php echo $header; ?></h5>
                <?php endforeach; ?><br>
                <h4><b>Band parameters (also check the figure!)</b></h4><br>
                <?php if($ok == 0) {?>
                    <h4><b>Wavelength interval to small for this computation</b></h4><br>
                <?php } else {?>
                    <h5>BI min[&#956;m]  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-  <?php echo $b1?>&#177;<?php echo $b1s?></h5>
                    <h5>BII min[&#956;m]  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-  <?php echo $b2?>&#177;<?php echo $b2s?></h5>
                    <h5>Band separation[&#956;m]  -  <?php echo $bsp?></h5><br>
                <?php }?>
                <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>.jpg" alt="Spectrum" width="600" height="450" /><br>
            </td>

        <?php break;?>

        <?php case "clout":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4><a href="http://adsabs.harvard.edu/abs/1986JGR....9111641C" target=_blank> Cloutis model </a> </h4>
                <h4><b>Informations about this spectrum:</b></h4><br>
                <?php foreach ($ast_header as $header): ?>
                    <h5><?php echo $header; ?></h5>
                <?php endforeach; ?><br>
                <h4><b>Mineralogical analysis - parameters from Cloutis model (also check the figures!)</b></h4><br>
                <?php if($ok == 0) {?>
                    <h4><b>Wavelength interval to small for this computation</b></h4><br>
                <?php } else {?>
                    <h5>BI min[&#956;m]  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-  <?php echo $b1?>&#177;<?php echo $b1s?></h5>
                    <h5>BII min[&#956;m]  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-  <?php echo $b2?>&#177;<?php echo $b2s?></h5>
                    <h5>Band separation[&#956;m]  -  <?php echo $bsp?></h5><br>

                    <h5>BI center[&#956;m]  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-  <?php echo $bc1?>&#177;<?php echo $bc1s?></h5>
                    <h5>BII center[&#956;m]  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-  <?php echo $bc2?>&#177;<?php echo $bc2s?></h5>
                    <h5>Band areea ratio[&#956;m]  -  <?php echo $bar?>&#177;<?php echo $bars?></h5>
                    <h5>OPX/(OPX+OL)[&#956;m]  &nbsp;&nbsp;-  <?php echo $opx?></h5><br>
                <?php }?>
                <h4>Band minima</h4>
                <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>.jpg" alt="Spectrum" width="600" height="450" />
                <h4>Linear continuum tangential to spectral curve</h4>
                <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>a.jpg" alt="Spectrum" width="600" height="450" />
                <h4>Wavelength position of the centers of the two absorption bands.<br> The regions enclosed correspond to the band centers computed for the H, L, and LL chondrites.</h4>
                <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>c.jpg" alt="Spectrum" width="600" height="450" />
                <h4>Band area ratio(BAR) versus band I center. <br>
                    The regions enclosed by continuous lines correspond to the values computed for basaltic achondrites(BA), ordinary chondrites(OC), and olivine-rich meteorites(Ol) (Gaffey et al. 1993).</h4>
                <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>b.jpg" alt="Spectrum" width="600" height="450" />
            </td>

        <?php break;?>

        <?php case "all":?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4>Option is not yet implemented<br>We apologize for the inconvenience</h4>
            </td>

        <?php break;?>  

    <?php } } else {?>

            <td width="705" valign="top" style="position:relative; left:20px;">
                <h4><b>Plot spectrum:</b></h4>
                <h5>Plots the reflectance as a function of wavelength.</h5><br>
                <h4><b>Taxonomy:</b></h4>
                <h5>Classify the spectrum according to different taxonomies like Bus-DeMeo, G13 and G9.</h5>
                <h5>The results of this application consist in the first three classes that match the asteroid spectrum, together with some matching quantitative values (coefficients).</h5>
                <h5>In addition, the asteroid spectrum is plotted together with standard spectra corresponding to the best matches.</h5><br>
                <h4><b>Search matching with spectra from the Relab database:</b></h4>
                <h5>Performs spectral comparison with spectra from Relab database.</h5>
                <h5>This application provides the first 50 laboratory spectra that matched the spectrum (in order of the matching coefficient).</h5><br>
                <!--<h4><b>Space weathering effects:</b></h4>
                <h5>Uses the space weathering model defined by Brunetto et al (2006).</h5>
                <h5>The results consists in computing the parameters of the model and de-reddening the spectrum.</h5>
                <h5>The de-reddening (removal of space weathering effects) is done by dividing the spectrum by its continuum.</h5><br>-->
                <h4><b>Band parameters and mineralogical analysis:</b></h4>
                <h5>Computes the spectral parameters defined by Cloutis et al (1986).</h5>
                <h5>If only the infrared part of the spectrum is given, the algorithm computes the band minima.</h5>
                <h5>If the spectrum contains both V and NIR regions, all the parameters are calculated.</h5>
            </td>

    <?php }?>

</table>

</form>
