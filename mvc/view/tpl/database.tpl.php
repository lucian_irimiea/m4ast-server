<p><font size="4">
    Run one of the following tasks and find the result at the bottom of the page.
    <?php if($search_done == 1): ?>
        <a href="#scroll"><font color="blue" size="5" style="position: relative; top:20px; left:160px;">Search results</font></a>
    <?php endif; ?>
    <br>
    Click on the resulted file name to access the spectral analyzing tools.
</font></p>
<table cellspacing="3" cellpadding="3">
            
        <td valign="top" style="position: relative; top: 20px;">
            <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
            <div class="btn-group-vertical">
                    <button type="submit" class="btn btn-default" name="page" value="2">Download spectrum</button>
                    <button type="submit" class="btn btn-default" name="page" value="1">Upload spectrum</button>
                    <button type="submit" class="btn btn-default" name="page" value="3">Statistics</button>
                    <!--<button type="submit" class="btn btn-default" name="page" value="4">Browse for information</button>-->
                    <button type="button" class="btn btn-default"><a href="/m4ast">
                            <img src="/m4ast/mvc/view/tpl/Files/m4ast.png" width='270'/>
                    </a></button>
            </div>
            </form>
        </td>
        
        <?php switch ($page_num) 
        {
            case 1:?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data"> 
                    <h3> 1.Upload your spectrum in database:</h3>
                    <p><font size="2" color="blue">	Your file should contain at least 2 columns: wavelengt and reflectance.<br> 
                        First column is interpreted as wavelength.<br> 
                        The second one as reflectance.<br> 
                        Size is limited to 10Mb.<br> 
                    </font></p>
                    <div class="form-group">
                        <label for="inputEmail3" style="position: relative; left: 20px;">Choose a file:</label>
                        <input type="file" name="userfile" style="position: relative; left: 20px;"/>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" style="position: relative; left: 20px;">Wavelength is given in :</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="landa_unit" style="position: relative; left: 6px;">
                                    <option value="u" <?php echo $rem3_2;?>>um</option>
                                    <option value="n" <?php echo $rem3_1;?>>nm</option>
                                    <option value="a" <?php echo $rem3_3;?>>A</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" style="position: relative; left: 20px;">Number of lines from the input file to be skipped :</label>
                        <div class="col-sm-10">    
                            <input type="text" class="form-control" name="line_skipe" maxlength="3" size="3" placeholder="0" value="<?php echo $rem1;?>" default="0" style="position: relative; left: 6px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" style="position: relative; left: 20px;">Session name - optional (Ex.: MySpec) :</label>
                        <div class="col-sm-10">    
                            <input type="text" class="form-control" name="mytempname" maxlength="16" size="16" placeholder="Temporary name" value="<?php echo $rem2;?>" style="position: relative; left: 6px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="page" value="1" class="btn btn-default" style="position: relative; left: 20px;">Submit Spectrum</button>
                    </div>
                    </form>
                </td>
        
            <?php break;?>
        
            <?php case 2:?>
            <?php case 6:?>
            <?php case 7:?> 
            <?php case 8:?>    
                
                <td width="705" valign="top" style="position: relative; left: 20px;">
                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
                        <h3> 1.Download Spectra from database:</h3>
                        <p><font size="2" color="blue"> Insert file name, which can result from a search in database or by uploading/obtaining a temporary file</br>
                            ex: - Heracles_20020829_950_00</br> 
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- tempspec1719</br> 
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- tempspec_MySpec
                        </font></p>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Select file</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="ftodown" maxlength="32" size="32" placeholder="File name" value="<?php echo $rem1;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="download" value="1">
                                <button type="submit" name="page" value="8" class="btn btn-default">Download Spectra</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Search</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="crit" maxlength="24" size="24" placeholder="Search criteria" value="<?php echo $rem2;?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <input type="hidden" name="download2" value="1">
                                <button type="submit" name="page" value="6" class="btn btn-default">Search Spectra</button>
                            </div>
                        </div>
                    </form>
                </td>
                
            <?php break;?> 
                
            <?php case 3:?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h3> 3.Statistics:</h3>
                    <h4> Data on m4ast database:</h4>
                    <ul>
                        <font size="3">
                            <li>Number of asteroids = <?php echo $m4ast_astnr;?></li>
                            <li>Number of spectra = <?php echo $m4ast_specnr;?></li>
                            <ul>
                            <li>Number of visible spectra = <?php echo $m4ast_vis;?></li>
                            <li>Number of near infrared spectra = <?php echo $m4ast_nir;?></li>
                            <li>Number of infrared spectra = <?php echo $m4ast_ir;?></li>
                            <li>Number of visible + near infrared spectra = <?php echo $m4ast_vis_nir;?></li>
                            <li>Number of visible + near infrared + infrared spectra = <?php echo $m4ast_vis_nir_ir;?></li>
                            <li>Number of near_infrared + infrared spectra = <?php echo $m4ast_nir_ir;?></li>
                            </ul>
                        </font>
                    </ul>
                    <br>
                    <h4> Data on SMASS MIT database:</h4>
                    <ul>
                        <font size="3">
                            <li>Number of asteroids = <?php echo $smassmit_astnr;?></li>
                            <li>Number of spectra = <?php echo $smassmit_specnr;?></li>
                            <ul>
                            <li>Number of visible spectra = <?php echo $smassmit_vis;?></li>
                            <li>Number of near infrared spectra = <?php echo $smassmit_nir;?></li>
                            <li>Number of infrared spectra = <?php echo $smassmit_ir;?></li>
                            <li>Number of visible + near infrared spectra = <?php echo $smassmit_vis_nir;?></li>
                            <li>Number of visible + near infrared + infrared spectra = <?php echo $smassmit_vis_nir_ir;?></li>
                            <li>Number of near_infrared + infrared spectra = <?php echo $smassmit_nir_ir;?></li>
                            </ul>
                        </font>
                    </ul>
                </td>
        
            <?php break;?>
            
            <?php case 4:?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h3> 4.Browse for information:</h3>
                    To Do ??
                </td>
        
            <?php break;?>
                
            <?php default:?>
                
                <td width="705" valign="top" style="position: relative; left: 20px;">
                    <h3>First select an action from the left menu</h3>
                </td>
                
            <?php break;?> 
                
        <?php }?>
        
        
</table>

<?php if (isset(View::$alert) && !empty(View::$alert)): ?>
    <div class="alert alert-danger" role="alert" ><font size="4"><?php echo View::$alert; ?></font></div>
<?php endif; ?>
<?php if (isset(View::$success) && !empty(View::$success)): ?>
    <div class="alert alert-success" role="alert" id="scroll"><font size="4"><?php echo View::$success; ?></font></div>
<?php endif; ?>
<?php if (isset(View::$info) && !empty(View::$info)): ?>
    <div class="alert alert-info" role="alert"><font size="4"><?php echo View::$info; ?></font></div>
<?php endif; ?>

<?php if ($page_num == 2 || $page_num == 6 || $page_num == 7 || $page_num == 8): 
    if ($fields != ""):?>
        <table class="table table-striped" style="position: relative; left: -15px;">
            <thead>
                <tr>
                    <?php foreach ($headers as $headerName): ?>
                        <th><font size=4><?php echo $headerName; ?></font></th>
                    <?php endforeach; ?>
                        <th><font size=4>Dl.</font></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($fields as $fieldsData): ?>
                    <tr>
                        <?php foreach (array_keys($headers) as $fieldKey): ?>
                            <td>
                                <?php if($fieldKey == 'FILE_NAME'): ?>
                                <a href="/m4ast/index.php/index/modelspec?file_name=<?php echo $fieldsData[$fieldKey]; ?>"target="_blank"><?php echo $fieldsData[$fieldKey]; ?></a>
                                <?php else:?> 
                                    <?php if($fieldKey == 'PUBLISHED' && strlen($fieldsData[$fieldKey]) > 12):?>
                                        <a href="<?php echo $fieldsData[$fieldKey]; ?>"target="_blank"><?php echo $fieldsData[$fieldKey]; ?></a>
                                    <?php else:?>
                                        <?php echo $fieldsData[$fieldKey]; ?>
                                    <?php endif;?>
                                <?php endif;?>
                            </td>
                        <?php endforeach; ?>
                        <td>
                            <form method="post" action="" style="display: inline-block; text-align: left;">
                                <input type="hidden" name="action" value="download"/>
                                <input type="hidden" name="actionId" value="<?php echo $fieldsData['FILE_NAME']; ?>" />
                                <button type="submit" name="page" value="7" class="btn btn-default" aria-label="Left Align">
                                    <span class="glyphicon glyphicon glyphicon-download" aria-hidden="true"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif;?>
<?php endif;?>