<div style="position:relative; top:20px;">
    <?php if (isset(View::$alert) && !empty(View::$alert)): ?>
        <div class="alert alert-danger" role="alert" ><font size="4"><?php echo View::$alert; ?></font></div>
    <?php endif; ?>
    <?php if (isset(View::$success) && !empty(View::$success)): ?>
        <div class="alert alert-success" role="alert" id="scroll"><font size="4"><?php echo View::$success; ?></font></div>
    <?php endif; ?>
    <?php if (isset(View::$info) && !empty(View::$info)): ?>
        <div class="alert alert-info" role="alert"><font size="4"><?php echo View::$info; ?></font></div>
    <?php endif; ?>
</div>

<?php if($out != NULL):?>
    <img border="0" src="<?php echo OctaveImg?>/mvc/output/asterspec<?php echo $out_rand; ?>.jpg" alt="Spectrum" width="600" height="450" />
<?php endif;?>
    
<br><br>

<form class="form-vertical" method="post" action="" enctype="multipart/form-data">
    <div class="form-group">
        <label for="inputEmail3" class="control-label">Download RELAB spectrum file:</label>
        <div>
            <button type="submit" name="download" value="1" class="btn btn-default">Download</button>
        </div>
    </div>
</form>

<br>

<a href="http://www.planetary.brown.edu/relabdocs/relab_disclaimer.htm" target="_blank"> 
    <font size = 3> Please read the disclaimer and the acknowledgment for Relab here!</font> 
</a>

<br><br>

<table class="table table-striped">
    <tr>
        <th colspan="2">
            <font size="4">Informations about Asteroid spectrum</font>
        </th>
    </tr>
    <?php foreach ($ast_header as $header): ?>   
        <tr>
            <th><h5><?php echo $header; ?></h5></th>
        </tr>
    <?php endforeach; ?>    
</table>

<table cellspacing="3" cellpadding="3">
            
    <td valign="top" style="width: 35%;">
        <?php if ($fields != ""):?>
            <div class="panel panel-default">
                <table class="table table-striped">
                    <tr>
                        <th colspan="2">
                            <font size="4">Informations about Relab spectrum acquisition</font>
                        </th>
                    </tr>
                    <?php foreach($fields as $key => $value): ?>    
                        <tr>
                            <th><font size=4><?php echo $headers[$key];?></font></th>
                            <td><?php echo $value;?></td>
                        </tr>
                    <?php endforeach; ?>    
                </table>
            </div>
        <?php endif;?>
    </td>
    <td valign="top" style="width: 5%;">
        
    </td>
    <td valign="top" style="width: 35%;">
        <?php if ($fields2 != ""):?>
            <div class="panel panel-default">
                <table class="table table-striped">
                    <tr>
                        <th colspan="2">
                            <font size="4">Informations about Relab spectrum</font>
                        </th>
                    </tr>
                    <?php foreach($fields2 as $key => $value): ?>    
                        <tr>
                            <th><font size=4><?php echo $headers2[$key];?></font></th>
                            <td><?php echo $value;?></td>
                        </tr>
                    <?php endforeach; ?>    
                </table>
            </div>
        <?php endif;?>
    </td>
    
</table>

<br>

<a href="<?php echo OctaveImg;?>/mvc/library/Relab/catalogues/Catalogue_README.html" target=_blank> <font size = 3>  Description of given parameters </font> </a>