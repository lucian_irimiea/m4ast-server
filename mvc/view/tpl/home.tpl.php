<img src="/m4ast/mvc/view/tpl/Files/home_img2.png" width="100%" style="position: relative;">
<img src="/m4ast/mvc/view/tpl/Files/m4ast.png" width="35%" style="position: absolute; top: 38px; right: 50px; background-color: #eee; padding: 10px;"><!--574-->

<h3>
    <b>M4AST (Modeling for Asteroids) is a free on-line tool for modeling visible and near-ir spectra of atmosphereless bodies.</b>
</h3>

<h4> <a href="http://adsabs.harvard.edu/cgi-bin/nph-data_query?bibcode=2012A%26A...544A.130P&link_type=ARTICLE&db_key=AST&high="> Detailed description and how to (.pdf file).</a> </h4>
<br>
<h4>
    M4AST allows to model temporary (on-the-fly) anonymous file submission.<br><br>
    M4AST proposes several routines for spectral analysis: <br>
    - plotting the data;<br>
    - merging spectra;<br>
    - taxonomy comparison;<br>
    - comparison to laboratory measurements(RELAB database);<br>
    - de-reddening of spectra and comparison.<br><br>

    M4AST interface was tested under Google Chrome, Firefox 3.6 and Internet Explorer 9 browsers.
</h4>
<br>
<h4>If you use M4AST tools in your work then please cite:<br><br>
"Modeling of asteroid spectra - M4AST", Astronomy and Astrophysics, Volume 544.<br>
Bibliographic code <a href="http://adsabs.harvard.edu/abs/2012A%26A...544A.130P"> here</a>.
</h4>
<br>
<font size="2">Authors: Marcel Popescu, Mirel Birlan.</font>
<img src="/m4ast/mvc/view/tpl/Files/logo_imcce.jpg" style="position: absolute; bottom: 0; right: 0;">