<p><font size="4">
    Run one of the following tasks and find the result at the bottom of the page.
    <?php if($search_done == 1): ?>
        <a href="#scroll"><font color="blue" size="5" style="position: relative; top:20px; left:120px;">Search results</font></a>
    <?php endif; ?>
    <br>
    Click on the resulted file name to access the spectral analyzing tools.
</font></p>
<table cellspacing="3" cellpadding="3">
            
        <td valign="top" style="position: relative; top: 20px;">
            <form class="form-horizontal" method="post" action="" enctype="multipart/form-data">
            <div class="btn-group-vertical">
                <button type="submit" class="btn btn-default" name="page" value="3">Upload your spectrum</button>
                <button type="submit" class="btn btn-default" name="page" value="1">Search spectrum in database</button>
                <button type="submit" class="btn btn-default" name="page" value="2">Search in SMASS MIT database</button>
                <button type="submit" class="btn btn-default" name="page" value="5">Clear temporary file</button>
                <button type="button" class="btn btn-default"><a href="/m4ast">
                    <img src="/m4ast/mvc/view/tpl/Files/m4ast.png" width='270'/>
                </a></button>
            </div>
            </form>
        </td>
       
        <?php switch ($page_num) 
        {
            case 1:?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data"> 
                    <h3> 2.Search for spectrum in database:</h3>
                    <p><font size="2" color="blue">	The logical AND operation is performed between the three criteria (empty means "all").<br> 
                        First 50 entries are shown.<br> 
                        As criteria can be any of the following:<br> 
                        &nbsp;	- asteroid number (ex: 1, 5143)<br> 
                        &nbsp;	- asteroid designation (ex: 1989YK8)<br> 
                        &nbsp;	- UAI code of the telescope (ex: 586)<br> 
                        &nbsp;	- observation date (ex: 2003-03-31)<br> 
                        &nbsp;	- the article where the spectrum was published (ex: http://adsabs.harvard.edu/abs/2012A%26A...544A.130P).</font></p>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label" onkeydown="if (event.keyCode == 13) <?php var_dump($page_num);?>">Criterion 1</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="crit1" maxlength="24" size="24" placeholder="criterion 1" value="<?php echo $rem1;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Criterion 2</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="crit2" maxlength="24" size="24" placeholder="criterion 2" value="<?php echo $rem2;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Criterion 3</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="crit3" maxlength="24" size="24" placeholder="criterion 3" value="<?php echo $rem3;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default" name="page" value="1">Search Spectra</button>
                        </div>
                    </div>
                    </form>
                </td>
        
            <?php break;?>
                
            <?php case 2:?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data"> 
                    <h3> 3.Search for spectrum in SMASS MIT database:</h3>
                    <div class="alert alert-info" role="alert" >
                    <p align="justify"><font size="2" color="">	<b>
                        All (or part) of the data utilized in this publication were obtained and made available by the The MIT-UH-IRTF Joint Campaign
                        for NEO Reconnaissance. The IRTF is operated by the University of Hawaii under Cooperative Agreement no. NCC 5-538 with the National Aeronautics
                        and Space Administration, Office of Space Science, Planetary Astronomy Program. The MIT component of this work is supported by NASA grant 09-NEOO009-0001,
                        and by the National Science Foundation under Grants Nos. 0506716 and 0907766.</b>
                    </font></p>
                    </div>
                    <p><font size="2" color="blue">	The logical AND operation is performed between the three criteria (empty means "all").<br> 
                        First 50 entries are shown.<br> 
                        As criteria can be any of the following:<br> 
                        &nbsp;	- asteroid number (ex: 1, 5143)<br> 
                        &nbsp;	- asteroid designation (ex: 1989YK8)</font></p>    
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Criterion 1</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="crit1s" maxlength="24" size="24" placeholder="criterion 1" value="<?php echo $rem1s;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Criterion 2</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="crit2s" maxlength="24" size="24" placeholder="criterion 2" value="<?php echo $rem2s;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Criterion 3</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="crit3s" maxlength="24" size="24" placeholder="criterion 3" value="<?php echo $rem3s;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default" name="page" value="2">Search Spectra</button>
                        </div>
                    </div>
                    </form>
                </td>
                
            <?php break;?> 
                
            <?php case 3:?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data"> 
                    <h3> 1.Upload your spectrum in database:</h3>
                    <p><font size="2" color="blue">	Your file should contain at least 2 columns: wavelengt and reflectance.<br> 
                        First column is interpreted as wavelength.<br> 
                        The second one as reflectance.<br> 
                        Size is limited to 10Mb.<br> 
                    </font></p>
                    <div class="form-group">
                        <label for="inputEmail3" style="position: relative; left: 20px;">Choose a file:</label>
                        <input type="file" name="userfile" style="position: relative; left: 20px;"/>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" style="position: relative; left: 20px;">Wavelength is given in :</label>
                        <div class="col-sm-10">
                            <select class="form-control" name="landa_unit" style="position: relative; left: 6px;">
                                    <option value="u" <?php echo $rem3_2;?>>um</option>
                                    <option value="n" <?php echo $rem3_1;?>>nm</option>
                                    <option value="a" <?php echo $rem3_3;?>>A</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" style="position: relative; left: 20px;">Number of lines from the input file to be skipped :</label>
                        <div class="col-sm-10">    
                            <input type="text" class="form-control" name="line_skipe" maxlength="3" size="3" placeholder="0" value="<?php echo $rem1;?>" default="0" style="position: relative; left: 6px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" style="position: relative; left: 20px;">Session name - optional (Ex.: MySpec) :</label>
                        <div class="col-sm-10">    
                            <input type="text" class="form-control" name="mytempname" maxlength="16" size="16" placeholder="Temporary name" value="<?php echo $rem2;?>" style="position: relative; left: 6px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" name="page" value="3" class="btn btn-default" style="position: relative; left: 20px;">Submit Spectrum</button>
                    </div>
                    </form>
                </td>
                    
            <?php break;?> 
            
            <?php case 5:?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <form class="form-horizontal" method="post" action="" enctype="multipart/form-data"> 
                    <h3> 4. Clear temporary file</h3>
                    <p><font size="2" color="blue"> Insert your temporary filename (ex: tempspec1719, tempspec_MySpec).
                    </font></p>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">File name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="tempdel" maxlength="32" size="32" placeholder="File name">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" name="page" value="5" class="btn btn-default">Delete file</button>
                        </div>
                    </div>
                    </form>
                </td>
                
            <?php break;?>   
                
            <?php default:?>
                
                <td width="705" valign="top" style="position:relative; left:20px;">
                    <h3>First select an action from the left menu</h3>
                </td>
                
            <?php break;?> 
                
        <?php }?>
       
</table>

<div style="position:relative; top:20px;">
    <?php if (isset(View::$alert) && !empty(View::$alert)): ?>
        <div class="alert alert-danger" role="alert" ><font size="4"><?php echo View::$alert; ?></font></div>
    <?php endif; ?>
    <?php if (isset(View::$success) && !empty(View::$success)): ?>
        <div class="alert alert-success" role="alert" id="scroll"><font size="4"><?php echo View::$success; ?></font></div>
    <?php endif; ?>
    <?php if (isset(View::$info) && !empty(View::$info)): ?>
        <div class="alert alert-info" role="alert"><font size="4"><?php echo View::$info; ?></font></div>
    <?php endif; ?>
</div>

<?php if ($page_num == 1):
    if ($fields != ""):?>
    <div class="pannel pannel-default">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <?php foreach ($headers as $headerName): ?>
                            <th><font size=4><?php echo $headerName; ?></font></th>
                        <?php endforeach; ?>
                            <th><font size=4>Dl.</font></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($fields as $fieldsData): ?>
                        <tr>
                            <?php foreach (array_keys($headers) as $fieldKey): ?>
                                <td>
                                    <?php if($fieldKey == 'FILE_NAME'): ?>
                                    <a href="/m4ast/index.php/index/analyze?file_name=<?php echo $fieldsData[$fieldKey]; ?>"target="_blank"><?php echo $fieldsData[$fieldKey]; ?></a>
                                    <?php else:?> 
                                        <?php if($fieldKey == 'PUBLISHED' && strlen($fieldsData[$fieldKey]) > 12):?>
                                            <a href="<?php echo $fieldsData[$fieldKey]; ?>"target="_blank"><?php $aux = explode("/",$fieldsData[$fieldKey]); echo ("/" . $aux[3] . "/" . $aux[4]); ?></a>
                                        <?php else:?>
                                            <?php echo $fieldsData[$fieldKey]; ?>
                                        <?php endif;?>
                                    <?php endif;?>
                                </td>
                            <?php endforeach; ?>
                            <?php if($page_num == 1){?>    
                                <td>
                                     <form method="post" action="" style="display: inline-block; text-align: left;">
                                         <input type="hidden" name="action" value="download"/>
                                         <input type="hidden" name="actionId" value="<?php echo $fieldsData['FILE_NAME']; ?>" />
                                         <button type="submit" name="page" value="7" class="btn btn-default" aria-label="Left Align">
                                             <span class="glyphicon glyphicon glyphicon-download" aria-hidden="true"></span>
                                         </button>
                                     </form>
                                 </td>
                            <?php }?>    
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <?php endif;?>
<?php endif; ?>
    
<?php if ($page_num == 2) 
{ 
    if ($fields != ""):?>
        <table class="table table-striped">
            <thead>
                <tr>
                    <?php foreach ($headers as $headerName): ?>
                        <th><font size=4><?php echo $headerName; ?></font></th>
                    <?php endforeach; ?>
                        <th><font size=4>Dl.</font></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($fields as $fieldsData): ?>
                    <tr>
                        <?php foreach (array_keys($headers) as $fieldKey): ?>
                            <td>
                                <?php if($fieldKey == 'FILE_NAME'): ?>
                                <a href="/m4ast/index.php/index/analyze?file_name=<?php echo $fieldsData[$fieldKey]; ?>"target="_blank"><?php echo $fieldsData[$fieldKey]; ?></a>
                                <?php else:?> 
                                    <?php if($fieldKey == 'PUBLISHED' && strlen($fieldsData[$fieldKey]) == 6):?>
                                        <a href="http://smass.mit.edu/smass.html#<?php echo $fieldsData[$fieldKey]; ?>" target="_blank"><?php echo $fieldsData[$fieldKey]; ?></a>
                                    <?php else:?>
                                        <?php echo $fieldsData[$fieldKey]; ?>
                                    <?php endif;?>
                                <?php endif;?>
                            </td>
                        <?php endforeach; ?>
                        <?php if($page_num == 2){?>    
                            <td>
                                 <form method="post" action="" style="display: inline-block; text-align: left;">
                                     <input type="hidden" name="action" value="downloadsmass"/>
                                     <input type="hidden" name="actionId" value="<?php echo $fieldsData['FILE_NAME']; ?>" />
                                     <button type="submit" name="page" value="7" class="btn btn-default" aria-label="Left Align">
                                         <span class="glyphicon glyphicon glyphicon-download" aria-hidden="true"></span>
                                     </button>
                                 </form>
                             </td>
                        <?php }?>    
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php endif;?>
<?php } ?>