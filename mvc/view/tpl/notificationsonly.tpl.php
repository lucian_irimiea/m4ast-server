<div style="position:relative; top:20px;">
    <?php if (isset(View::$alert) && !empty(View::$alert)): ?>
        <div class="alert alert-danger" role="alert" ><font size="4"><?php echo View::$alert; ?></font></div>
    <?php endif; ?>
    <?php if (isset(View::$success) && !empty(View::$success)): ?>
        <div class="alert alert-success" role="alert" id="scroll"><font size="4"><?php echo View::$success; ?></font></div>
    <?php endif; ?>
    <?php if (isset(View::$info) && !empty(View::$info)): ?>
        <div class="alert alert-info" role="alert"><font size="4"><?php echo View::$info; ?></font></div>
    <?php endif; ?>
</div>

