<?php

class ControllerIndex {

    public function home() {
        phpinfo();
        // Main page, for presentation
        // Set the page title
        View::$title = 'Home';

        // Output the HTML
        require_once ROOT . '/mvc/view/tpl/home.tpl.php';
    }

    public function start() {

        // Set the title page
        View::$title = 'Start';

        $page_num = "";
        $search_done = 0;
        $del_done = 0;

        // Holds table content
        $fields = "";

        // Variables that hold previous search parameters
        $rem1 = "";
        $rem2 = "";
        $rem3 = "";
        $rem1s = "";
        $rem2s = '';
        $rem3s = '';
        $rem3_1 = "";
        $rem3_2 = "";
        $rem3_3 = "";

        $error = 0;
        //var_dump($_POST);
        if (isset($_POST['page']) && !empty($_POST['page'])) {
            $page_num = $_POST['page'];
            switch ($page_num) {

                // Search m4ast database----------------------------------------
                case 1:

                    // Verify if search parameters were entered
                    if (isset($_POST['crit1']))
                        $crit1 = $_POST['crit1'];
                    else
                        $crit1 = "";
                    if (isset($_POST['crit2']))
                        $crit2 = $_POST['crit2'];
                    else
                        $crit2 = "";
                    if (isset($_POST['crit3']))
                        $crit3 = $_POST['crit3'];
                    else
                        $crit3 = "";
                    // Memorize search parameters
                    $rem1 = $crit1;
                    $rem2 = $crit2;
                    $rem3 = $crit3;

                    // Search for Spectrum Files in Data Base
                    $aux = str_split($crit1); //from string to array
                    foreach ($aux as $key => $value)
                        if ((ord($value) < 33)) {
                            unset($aux[$key]);
                        }
                    $crit1 = implode($aux); // from array to string

                    $aux = str_split($crit2); //from string to array
                    foreach ($aux as $key => $value)
                        if ((ord($value) < 33)) {
                            unset($aux[$key]);
                        }
                    $crit2 = implode($aux); // from array to string

                    $aux = str_split($crit3); //from string to array
                    foreach ($aux as $key => $value)
                        if ((ord($value) < 33)) {
                            unset($aux[$key]);
                        }
                    $crit3 = implode($aux); // from array to string
                    // Require code to serch database and extract wanted information
                    require_once ROOT . '/mvc/functions/selectspectredbaspmsql.php';

                    break;
                // End search m4ast database------------------------------------
                // Search smassmit database-------------------------------------
                case 2:

                    // Verify if search parameters were entered
                    if (isset($_POST['crit1s']))
                        $crit1s = $_POST['crit1s'];
                    else
                        $crit1s = "";
                    if (isset($_POST['crit2s']))
                        $crit2s = $_POST['crit2s'];
                    else
                        $crit2s = "";
                    if (isset($_POST['crit3s']))
                        $crit3s = $_POST['crit3s'];
                    else
                        $crit3s = "";
                    // Memorize search parameters
                    $rem1s = $crit1s;
                    $rem2s = $crit2s;
                    $rem3s = $crit3s;

                    // Search for Spectrum Files in Data Base
                    $aux = str_split($crit1s); //from string to array
                    foreach ($aux as $key => $value)
                        if ((ord($value) < 33)) {
                            unset($aux[$key]);
                        }
                    $crit1s = implode($aux); // from array to string

                    $aux = str_split($crit2s); //from string to array
                    foreach ($aux as $key => $value)
                        if ((ord($value) < 33)) {
                            unset($aux[$key]);
                        }
                    $crit2s = implode($aux); // from array to string

                    $aux = str_split($crit3s); //from string to array
                    foreach ($aux as $key => $value)
                        if ((ord($value) < 33)) {
                            unset($aux[$key]);
                        }
                    $crit3s = implode($aux); // from array to string
                    // Require code to serch database and extract wanted information
                    require_once ROOT . '/mvc/functions/selectspectresmassmitsql.php';

                    break;
                // End search smassmit database---------------------------------    
                // Upload temporary spectrum to database------------------------    
                case 3:

                    if (isset($_POST['landa_unit'])) {
                        //Get information from html form
                        $landa_unit = trim($_POST['landa_unit']); // landa factor to transform in microns the input wavelengths
                        switch ($landa_unit) {
                            case "n": // if nanometers
                                $l_unit = 1000;
                                $rem3_1 = "selected";
                                $rem3_2 = "";
                                $rem3_3 = "";
                                break;
                            case "u": // microns
                                $l_unit = 1;
                                $rem3_1 = "";
                                $rem3_2 = "selected";
                                $rem3_3 = "";
                                break;
                            case "a": //angstroms
                                $l_unit = 10000;
                                $rem3_1 = "";
                                $rem3_2 = "";
                                $rem3_3 = "selected";
                                break;
                        }
                        $line_skipe = trim($_REQUEST['line_skipe']); // number of lines to be skiped
                        $mytempname = trim($_REQUEST['mytempname']); // temporary name
                        // Remember entered values
                        $rem1 = $line_skipe;
                        $rem2 = $mytempname;
                    }
                    if (isset($_FILES['userfile']) && ($_FILES['userfile']['size'] > 0)) {
                        // Input file is not valid, issue an error
                        if (($_FILES["userfile"]["error"] > 0) && ($_FILES["userfile"]["size"] < 5e6)) {
                            View::$alert = "Upload failed" . "</br>" . "The file is not valid";
                        } else {
                            //Read Input file (Extract the wavelengths and Reflectances)
                            $landa[0] = 0; //initialize the array of wavelengths
                            $Refl[0] = 0; // initialize the array of reflectances
                            $std_dev[0] = 0; // initialize the array of standard deviations if will be given
                            require_once ROOT . '/mvc/functions/readuserspectrum.php';

                            // initialize the flag to confirm the validity of data
                            $flag = 0;
                            $landa_min = 0;
                            $landa_max = 0;
                            $landa_norm = 0;

                            // Normalize the spectrum
                            $landa_unit = $l_unit;
                            $landaN = $landa_norm;
                            $landa_m = $landa_min;
                            $landa_M = $landa_max;
                            require_once ROOT . '/mvc/functions/normalizespectrum.php';
                            $l_unit = $landa_unit;
                            $landa_norm = $landaN;
                            $landa_min = $landa_m;
                            $landa_max = $landa_M;

                            if (strlen($mytempname) >= 1) {
                                $out_rand = $mytempname;
                            } else {
                                $out_rand = rand(1000, 9999);
                            }

                            $aux = str_split($out_rand); //from string to array
                            foreach ($aux as $key => $value)
                                if ((ord($value) < 33)) {
                                    unset($aux[$key]);
                                }
                            $out_rand = implode($aux); // from array to string
                            // open output file (result file that contain the spectrum)
                            $fdoutput = fopen(ROOT . '/mvc/library/Spectre/tempspec_' . $out_rand . '.txt', "w");

                            // Write Header of output file
                            fwrite($fdoutput, sprintf("Name (or temporary designation) of the asteroid:File uploaded by user\n"));
                            fwrite($fdoutput, sprintf("Number of the asteroid: Unknown\n"));
                            fwrite($fdoutput, sprintf("Observation was done on (UT) (format YYYY-MM-DD-hh):Unknown\n"));
                            fwrite($fdoutput, sprintf("Observation was made by :Unknown\n"));
                            fwrite($fdoutput, sprintf("At observatory with UAI code:Unknown\n"));
                            fwrite($fdoutput, sprintf("The spectrum was published in the article: Unknown\n"));
                            fwrite($fdoutput, sprintf("The normalization of this spectrum was made for the wavelength: %.3f microns.\n", $landa_norm));
                            fwrite($fdoutput, sprintf("Old name of the file was: %s\n", $_FILES["userfile"]["name"]));
                            fwrite($fdoutput, sprintf("Wavelength[um]	Reflectance	sigma(optional)\n"));

                            // Write Data in the Output file spectrum
                            if (count($std_dev) == count($landa)) {// standard deviation is given in input file 
                                for ($i = 0; $i < count($landa); $i++) {
                                    // write landa Reflectence and standard deviation
                                    fwrite($fdoutput, sprintf("%.5f %.5f %.5f\n", $landa[$i], $Refl[$i], $std_dev[$i]));
                                }
                            } else {
                                // write only  wavelength and reflectance
                                for ($i = 0; $i < count($landa); $i++) {
                                    fwrite($fdoutput, sprintf("%.5f %.5f\n", $landa[$i], $Refl[$i]));
                                }
                            }
                            fclose($fdoutput);

                            if ($error == 0)
                                View::$success = "Upload succesful" . "</br>" . "You can find the file with temporary name:" . '<a href="/m4ast/index.php/index/analyze?file_name=tempspec_' . $out_rand . '"target="_blank">tempspec_' . $out_rand . '</a>';
                        }
                    }
                    else {
                        View::$info = "Please select the input file";
                    }

                    break;
                // End upload temporary spectrum to database--------------------    
                // Clear temporary file form database---------------------------   
                case 5:

                    if (isset($_POST['tempdel']) && @$_POST['tempdel'] != "") {
                        $tempdel = trim($_REQUEST['tempdel']);

                        $aux = str_split($tempdel); //from string to array
                        foreach ($aux as $key => $value)
                            if ((ord($value) < 33)) {
                                unset($aux[$key]);
                            }
                        $tempdel = implode($aux); // from array to string

                        $rem = $tempdel;

                        $path = ROOT . '/mvc/library/Spectre/' . $tempdel . '.txt';
                        if (!file_exists($path))
                            $path = ROOT . '/mvc/library/Spectre/tempspec_' . $tempdel . '.txt';
                        if (file_exists($path)) {
                            $out = shell_exec('rm ' . $path);

                            if ($out == NULL) {
                                View::$alert = "Command coult not be executed, file was not deleted";
                            } else {
                                View::$success = "File deleted succesfuly";
                            }
                        } else {
                            View::$alert = "File does not exist";
                        }
                    } else {
                        View::$info = "Please enter the name of the file you wish to remove";
                    }

                    break;
                // End clear temporary file from database-----------------------
                // Download file------------------------------------------------
                case 7:

                    // Sequence for table inline download button
                    if (@ $_POST['action'] == 'download') {
                        @ $ftodown = trim($_POST['actionId']);
                        require_once ROOT . '/mvc/functions/downloadspectredbaspm.php';
                    }

                    // Sequence for table inline download button SMASSMIT search
                    if (@ $_POST['action'] == 'downloadsmass') {
                        @ $ftodown = trim($_POST['actionId']);
                        require_once ROOT . '/mvc/functions/downloadspectresmassmit.php';
                    }

                    break;
                // End download file--------------------------------------------    

                default:
                    break;
            }
        }

        // Output the html
        require_once ROOT . '/mvc/view/tpl/start.tpl.php';
    }

    public function analyze() {

        // Set the title page
        View::$title = 'Analyze';

        // Initializa variables
        $file_name = "";
        $bdm_method = "0";
        $bdm_method_sel = "Std. dev.";
        $model_method = "0";
        $model_method_sel = "Std. dev.";
        $action_type = "";

        // Variables that hold previous parameters
        $rem1 = "";
        $rem2 = "";
        $rem1_1 = "";
        $rem1_2 = "";
        $rem1_3 = "";
        $rem2_1 = "";
        $rem2_2 = "";
        $rem2_3 = "";
        $rem2_4 = "";
        $rem2_5 = "";

        // Save file_name
        if (isset($_REQUEST['file_name'])) {
            $file_name = trim($_REQUEST['file_name']);
        }

        // Save data from previous post
        if (isset($_POST['model_method'])) {
            switch ($_POST['model_method']) {
                case "0":
                    $rem2_1 = "selected";
                    $rem2_2 = "";
                    $rem2_3 = "";
                    $rem2_4 = "";
                    $rem2_5 = "";
                    break;
                case "1":
                    $rem2_1 = "";
                    $rem2_2 = "selected";
                    $rem2_3 = "";
                    $rem2_4 = "";
                    $rem2_5 = "";
                    break;
                case "2":
                    $rem2_1 = "";
                    $rem2_2 = "";
                    $rem2_3 = "selected";
                    $rem2_4 = "";
                    $rem2_5 = "";
                    break;
                case "3":
                    $rem2_1 = "";
                    $rem2_2 = "";
                    $rem2_3 = "";
                    $rem2_4 = "selected";
                    $rem2_5 = "";
                    break;
                case "4":
                    $rem2_1 = "";
                    $rem2_2 = "";
                    $rem2_3 = "";
                    $rem2_4 = "";
                    $rem2_5 = "selected";
                    break;
            }
        }
        if (isset($_POST['bdm_method'])) {
            switch ($_POST['bdm_method']) {
                case "0":
                    $rem1_1 = "selected";
                    $rem1_2 = "";
                    $rem1_3 = "";
                    break;
                case "1":
                    $rem1_1 = "";
                    $rem1_2 = "selected";
                    $rem1_3 = "";
                    break;
                case "2":
                    $rem1_1 = "";
                    $rem1_2 = "";
                    $rem1_3 = "selected";
                    break;
            }
        }

        if (isset($_POST['file_name'])) {

            if (!empty($_POST['file_name'])) {
                $file_name = $_POST['file_name'];
                if ((isset($_POST['action_type']) && !empty($_POST['action_type'])) || (isset($_POST['action_type_2']) && !empty($_POST['action_type_2'])) && isset($_POST['file_name']) && !empty($_POST['file_name'])) {
                    $action_type = $_POST['action_type'];

                    // Get data from post
                    $file_name = trim($_POST['file_name']);

                    //File exists
                    $file_exists = 0;

                    if (file_exists(ROOT . '/mvc/library/Spectre/' . $file_name . '.txt')) {
                        $path_file = ROOT . '/mvc/library/Spectre/' . $file_name . '.txt';
                        $file_exists = 1;
                    } elseif (!file_exists(ROOT . '/mvc/library/smassmit/spectre/' . $file_name . '.txt')) {
                        View::$alert = "Spectrum does not exist, check the given name";
                    } else {
                        $path_file = ROOT . '/mvc/library/smassmit/spectre/' . $file_name . '.txt';
                        $file_exists = 1;
                    }
                    if ($file_exists == 1) {
                        $out_rand = rand(1000, 9999);
                        if (file_exists(ROOT . '/mvc/output/script_octave' . $out_rand . '.m'))
                            shell_exec('rm ' . ROOT . '/mvc/output/script_octave' . $out_rand . '.m');
                        if (file_exists(ROOT . '/mvc/output/asterspec' . $out_rand . '.jpg'))
                            shell_exec('rm ' . ROOT . '/mvc/output/asterspec' . $out_rand . '.jpg');

                        //Write matlab script for data processing
                        $fid = fopen(ROOT . '/mvc/output/script_octave' . $out_rand . '.m', 'w');

                        switch ($action_type) {
                            // Plot spectrum------------------------------------
                            case "plt":

                                fwrite($fid, sprintf('plot_asteroid_spectrum_SpecDb("' . $path_file . '",' . $out_rand . ');'));
                                fclose($fid);

                                // Execute matlab script for processing spectrum
                                $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                // Check if script executed succesfully
                                if ($out == NULL) {
                                    View::$alert = "Octave script could not be executed";
                                } else {
                                    require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';
                                }

                                break;
                            // End plot spectrum--------------------------------
                            // Concatenate spectra------------------------------
                            case "conc":

                                // Force $out = 1 as to display template correctly
                                $out = 1;

                                if (isset($_POST['spec1']) && !empty($_POST['spec1'])) {
                                    $spec1 = trim($_REQUEST['spec1']);

                                    if (!empty($_POST['f_name'])) {
                                        $f_name = $_POST['f_name'];
                                        $rem2 = $f_name;
                                    } else {
                                        $f_name = $out_rand;
                                    }
                                    // Memorize search parameters
                                    $rem1 = $spec1;

                                    // Second file exists, for concatenation
                                    $file_exists_concat = 0;

                                    if (file_exists(ROOT . '/mvc/library/Spectre/' . $spec1 . '.txt')) {
                                        $path_file_concat = ROOT . '/mvc/library/Spectre/' . $spec1 . '.txt';
                                        $file_exists_concat = 1;
                                    } elseif (!file_exists(ROOT . '/mvc/library/smassmit/spectre/' . $spec1 . '.txt')) {
                                        View::$alert = "Second spectra entered does not exist, check again";
                                    } else {
                                        $path_file_concat = ROOT . '/mvc/library/smassmit/spectre/' . $spec1 . '.txt';
                                        $file_exists_concat = 1;
                                    }

                                    if ($file_exists_concat == 1) {
                                        if (isset($_POST['spec1']) && isset($_POST['file_name']) && $_POST['file_name'] != "" && $_POST['spec1'] != "") {
                                            fwrite($fid, sprintf('concat_spec_SpecDB("' . $path_file . '","' . $path_file_concat . '","' . ROOT . '/mvc/library/Spectre/tempspec_' . $f_name . '.txt");'));
                                            fclose($fid);
                                            $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                            // Check if script executed succesfully
                                            if ($out == NULL) {
                                                View::$alert = "Octave script could not be executed";
                                            } else {
                                                if (strpos($out, 'CFDone')) {
                                                    View::$success = "Concatenation finished" . "<br>" . "<a href=/m4ast/index.php/index/analyze?file_name=tempspec_" . $f_name . '"target="_blank">tempspec' . $out_rand . '</a>';
                                                }
                                            }
                                        }
                                        $out = 1;
                                    }
                                } else {
                                    View::$info = "Please enter the second spectrum";
                                }

                                break;
                            // End concatenate spectra--------------------------    
                            // Spectral corrections-----------------------
                            case "scorr":

                                // To Do
                                $out = 1;

                                break;
                            // End spectral corrections-------------------------    
                            // Taxonomy type selection tab----------------------    
                            case "tax":

                                // No octave script to run, force $out = 1 as to display template correctly
                                $out = 1;

                                if (isset($_POST['bdm_method']) && !empty($_POST['bdm_method'])) {
                                    $bdm_method = trim($_REQUEST['bdm_method']);
                                }

                                break;
                            // End taxonomy type selection tab------------------
                            // Taxonomy using g9t method------------------------    
                            case "g9t":

                                fwrite($fid, sprintf('Gtaxonom_SpecDb("' . $path_file . '",9,' . $out_rand . ');'));
                                fclose($fid);

                                // Execute octave script
                                $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                // Check if script executed succesfully
                                if ($out == NULL) {
                                    View::$alert = "Octave script could not be executed";
                                } else {
                                    $nopointinspec = strpos($out, "Reliability of this classification is");

                                    if ($nopointinspec) {
                                        $idx_tr = strpos($out, 'Reliability');
                                        $reliability = substr($out, $idx_tr, 45);
                                    }

                                    require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';
                                }

                                break;
                            // End taxonomy using g9t method--------------------
                            // Taxonomy using g13t method-----------------------
                            case "g13t":

                                fwrite($fid, sprintf('Gtaxonom_SpecDb("' . $path_file . '",13,' . $out_rand . ');'));
                                fclose($fid);

                                // Execute octave script
                                $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                // Check if script executed succesfully
                                if ($out == NULL) {
                                    View::$alert = "Octave script could not be executed";
                                } else {
                                    $idx_tr = strpos($out, 'Reliability');
                                    $reliability = substr($out, $idx_tr, 45);

                                    require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';
                                }

                                break;
                            // End taxonomy using g13t method-------------------
                            // Tabonomy using bus-demeo method------------------
                            case "bdm":

                                $bdm_method = trim($_REQUEST['bdm_method']); //select method
                                fwrite($fid, sprintf('DeMeotaxonom_SpecDb("' . $path_file . '",' . $bdm_method . ',' . $out_rand . ');'));
                                fclose($fid);

                                // Execute octave script
                                $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                // Check if script executed succesfully
                                if ($out == NULL) {
                                    View::$alert = "Octave script could not be executed";
                                } else {
                                    require_once ROOT . '/mvc/functions/retrieveinfobdmaspm.php';

                                    require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';
                                }

                                break;
                            // End taxonomy using bus-demeo method--------------
                            // Relab type selection tab-------------------------
                            case "rel":

                                // No octave script to run, force $out = 1 as to display template correctly
                                $out = 1;

                                if (isset($_POST['model_method']) && !empty($_POST['model_method'])) {
                                    $model_method = trim($_REQUEST['model_method']);
                                }

                                break;
                            // End relab type selection tab---------------------
                            // Comparison with all relab spectra----------------     
                            case "relab":

                                $model_method = trim($_REQUEST['model_method']); // landa factor to transform in microns the input wavelengths
                                fwrite($fid, sprintf('relab_compare_SpecDb("' . $path_file . '",0,' . $model_method . ');')); // All spectra
                                // Execute octave script
                                $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                // Check if script executed succesfully
                                if ($out == NULL) {
                                    View::$alert = "Octave script could not be executed";
                                } else {
                                    require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';

                                    require_once ROOT . '/mvc/functions/retrieveinforelab.php';
                                }

                                break;
                            // End comparison with all relab spectra------------
                            // Comparison with relab meteorite spectra----------
                            case "relabm":

                                $model_method = trim($_REQUEST['model_method']); //select method
                                fwrite($fid, sprintf('relab_compare_SpecDb("' . $path_file . '",1,' . $model_method . ');')); //Meteorite spectra
                                // Execute octave script
                                $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                // Check if script executed succesfully
                                if ($out == NULL) {
                                    View::$alert = "Octave script could not be executed";
                                } else {
                                    require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';

                                    require_once ROOT . '/mvc/functions/retrieveinforelab.php';
                                }

                                break;
                            // End comparison with relab meteorites spectra-----
                            // Comparison with asteroids spectra----------------
                            case "ast":

                                // To Do
                                $out = 1;

                                break;
                            // End comparison with asteroids spectra------------    
                            // Nici cea mai vaga idee
                            case "spwth":

                                fwrite($fid, sprintf('spaceweather_SpecDb("' . $path_file . '",' . $out_rand . ');'));

                                // Execute octave script
                                $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                // Check if script executed succesfully
                                if ($out == NULL) {
                                    View::$alert = "Octave script could not be executed";
                                } else {
                                    require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';

                                    $z0 = explode('@', $out);
                                    $z0[0] = '';
                                    $z0[1] = '';
                                    $z0 = array_values(array_filter($z0));
                                }

                                break;
                            // End
                            // Calcutate band parameters------------------------
                            case "bmin":

                                fwrite($fid, sprintf('bandminima("' . $path_file . '",' . $out_rand . ');'));

                                // Execute octave script
                                $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                // Check if script executed succesfully
                                if ($out == NULL) {
                                    View::$alert = "Octave script could not be executed";
                                } else {
                                    require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';

                                    if (strpos($out, 'Tosmall')) {
                                        $ok = 0;
                                    } else {
                                        $ok = 1;
                                        $z0 = explode('@', $out);
                                        $z0[0] = '';
                                        $z0[1] = '';
                                        $b1 = $z0[2];
                                        $b1s = $z0[3];
                                        $b2 = $z0[4];
                                        $b2s = $z0[5];
                                        $bsp = $b2 - $b1;
                                    }
                                }

                                break;
                            // End calculate band parameters--------------------
                            // Mineralogical analysis---------------------------    
                            case "clout":

                                fwrite($fid, sprintf('cloutismodel("' . $path_file . '",' . $out_rand . ');'));

                                // Execute octave script
                                $out = shell_exec(Octave . ROOT . "/mvc/output/script_octave" . $out_rand . ".m");

                                // Check if script executed succesfully
                                if ($out == NULL) {
                                    View::$alert = "Octave script could not be executed";
                                } else {
                                    require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';

                                    if (strpos($out, 'Tosmall')) {
                                        $ok = 0;
                                    } else {
                                        $ok = 1;
                                        $z0 = explode('@', $out);
                                        $z0[0] = '';
                                        $z0[1] = '';
                                        $b1 = $z0[2];
                                        $b1s = $z0[3];
                                        $b2 = $z0[4];
                                        $b2s = $z0[5];
                                        $bsp = $b2 - $b1;

                                        $bc1 = $z0[7];
                                        $bc1s = $z0[8];
                                        $bc2 = $z0[9];
                                        $bc2s = $z0[10];
                                        $bar = $z0[11];
                                        $bars = $z0[12];
                                        $opx = 0.4187 * ($bar + 0.125);
                                    }
                                }

                                break;
                            // End mineralogical analysis-----------------------   
                            // Complete analysis--------------------------------
                            case "all":

                                // To Do
                                $out = 1;

                                break;
                            // End complete analysis----------------------------    
                        }
                    }
                } else {
                    View::$info = "Please enter asteroid name<br>Be patient while gathering and sorting information, the run might take up to 3 minutes...<br>The results are displayed bellow (section Results) after processing.";
                }
            } else {
                View::$alert = "Please enter asteroid name";
            }
        } else {
            View::$info = "Please enter asteroid name<br>Be patient while gathering and sorting information, the run might take up to 3 minutes...<br>The results are displayed bellow (section Results) after processing.";
        }
        // Output the html
        require_once ROOT . '/mvc/view/tpl/analyze.tpl.php';
    }

    public function database() {

        // Set the title page
        View::$title = 'Database';

        $page_num = "";
        $search_done = 0;
        $del_done = 0;

        // Holds table content
        $fields = "";

        // Variables that hold previous search parameters
        $rem1 = "";
        $rem2 = "";
        $rem3_1 = "";
        $rem3_2 = "";
        $rem3_3 = "";

        $error = 0;
        //var_dump($_POST);
        if (isset($_POST['page']) && !empty($_POST['page'])) {
            $page_num = $_POST['page'];

            switch ($page_num) {

                // Upload form
                case 1:

                    if (isset($_POST['landa_unit'])) {
                        //Get information from html form
                        $landa_unit = trim($_POST['landa_unit']); // landa factor to transform in microns the input wavelengths
                        switch ($landa_unit) {
                            case "n": // if nanometers
                                $l_unit = 1000;
                                $rem3_1 = "selected";
                                $rem3_2 = "";
                                $rem3_3 = "";
                                break;
                            case "u": // microns
                                $l_unit = 1;
                                $rem3_1 = "";
                                $rem3_2 = "selected";
                                $rem3_3 = "";
                                break;
                            case "a": //angstroms
                                $l_unit = 10000;
                                $rem3_1 = "";
                                $rem3_2 = "";
                                $rem3_3 = "selected";
                                break;
                        }
                        $line_skipe = trim($_REQUEST['line_skipe']); // number of lines to be skiped
                        $mytempname = trim($_REQUEST['mytempname']); // temporary name
                        // Remember entered values
                        $rem1 = $line_skipe;
                        $rem2 = $mytempname;
                    }
                    if (isset($_FILES['userfile']) && ($_FILES['userfile']['size'] > 0)) {
                        // Input file is not valid, issue an error
                        if (($_FILES["userfile"]["error"] > 0) && ($_FILES["userfile"]["size"] < 5e6)) {
                            View::$alert = "Upload failed" . "</br>" . "The file is not valid";
                        } else {
                            //Read Input file (Extract the wavelengths and Reflectances)
                            $landa[0] = 0; //initialize the array of wavelengths
                            $Refl[0] = 0; // initialize the array of reflectances
                            $std_dev[0] = 0; // initialize the array of standard deviations if will be given
                            require_once ROOT . '/mvc/functions/readuserspectrum.php';

                            // initialize the flag to confirm the validity of data
                            $flag = 0;
                            $landa_min = 0;
                            $landa_max = 0;
                            $landa_norm = 0;

                            // Normalize the spectrum
                            $landa_unit = $l_unit;
                            $landaN = $landa_norm;
                            $landa_m = $landa_min;
                            $landa_M = $landa_max;
                            require_once ROOT . '/mvc/functions/normalizespectrum.php';
                            $l_unit = $landa_unit;
                            $landa_norm = $landaN;
                            $landa_min = $landa_m;
                            $landa_max = $landa_M;

                            if (strlen($mytempname) >= 1) {
                                $out_rand = $mytempname;
                            } else {
                                $out_rand = rand(1000, 9999);
                            }

                            $aux = str_split($out_rand); //from string to array
                            foreach ($aux as $key => $value)
                                if ((ord($value) < 33)) {
                                    unset($aux[$key]);
                                }
                            $out_rand = implode($aux); // from array to string
                            // open output file (result file that contain the spectrum)
                            $fdoutput = fopen(ROOT . '/mvc/library/Spectre/tempspec_' . $out_rand . '.txt', "w");

                            // Write Header of output file
                            fwrite($fdoutput, sprintf("Name (or temporary designation) of the asteroid:File uploaded by user\n"));
                            fwrite($fdoutput, sprintf("Number of the asteroid: Unknown\n"));
                            fwrite($fdoutput, sprintf("Observation was done on (UT) (format YYYY-MM-DD-hh):Unknown\n"));
                            fwrite($fdoutput, sprintf("Observation was made by :Unknown\n"));
                            fwrite($fdoutput, sprintf("At observatory with UAI code:Unknown\n"));
                            fwrite($fdoutput, sprintf("The spectrum was published in the article: Unknown\n"));
                            fwrite($fdoutput, sprintf("The normalization of this spectrum was made for the wavelength: %.3f microns.\n", $landa_norm));
                            fwrite($fdoutput, sprintf("Old name of the file was: %s\n", $_FILES["userfile"]["name"]));
                            fwrite($fdoutput, sprintf("Wavelength[um]	Reflectance	sigma(optional)\n"));

                            // Write Data in the Output file spectrum
                            if (count($std_dev) == count($landa)) {// standard deviation is given in input file 
                                for ($i = 0; $i < count($landa); $i++) {
                                    // write landa Reflectence and standard deviation
                                    fwrite($fdoutput, sprintf("%.5f %.5f %.5f\n", $landa[$i], $Refl[$i], $std_dev[$i]));
                                }
                            } else {
                                // write only  wavelength and reflectance
                                for ($i = 0; $i < count($landa); $i++) {
                                    fwrite($fdoutput, sprintf("%.5f %.5f\n", $landa[$i], $Refl[$i]));
                                }
                            }
                            fclose($fdoutput);

                            if ($error == 0)
                                View::$success = "Upload succesful" . "</br>" . "You can find the file with temporary name:" . '<a href="/m4ast/index.php/index/analyze?file_name=tempspec_' . $out_rand . '"target="_blank">tempspec_' . $out_rand . '</a>';
                        }
                    }
                    else {
                        View::$info = "Please select the input file";
                    }

                    break;

                // Cases 2,6,7 and 8 are for the download phase (both in index and template)
                // They are a workaroun for an error I was encountering
                // Case 2 = initial form
                // Case 6 = search
                // Case 7 = download from table, after search is done
                // Case 8 = download spectra with given name    
                case 2:

                    //Initialize download form
                    //Display information for next step
                    View::$info = "Please choose an option" . "<br>" . "Enter a file name or search for a file to download";

                    break;

                case 6:

                    // verify if search criteria war given
                    if (isset($_POST['crit']))
                        $crit1 = $_POST['crit'];
                    else
                        $crit1 = "";

                    // verify if file name was given
                    if (isset($_POST['ftodown']))
                        $ftodown = trim($_POST['ftodown']);
                    else
                        $ftodown = "";

                    // Memorize search parameters
                    $rem1 = $ftodown;
                    $rem2 = $crit1;

                    // Search for a file, if crit was given
                    if ($crit1 != "" && $_POST['download2'] == '1') {
                        // Prepare criteria for search
                        $aux = str_split($crit1); //from string to array
                        foreach ($aux as $key => $value)
                            if ((ord($value) < 33)) {
                                unset($aux[$key]);
                            }
                        $crit1 = implode($aux); // from array to string
                        // Extra 2 criteria, search used is the same with that of the search page, which has 3 criteria
                        $crit2 = "";
                        $crit3 = "";

                        // Search for Spectrum Files in Data Base
                        require_once ROOT . '/mvc/functions/selectspectredbaspmsql.php';
                    }

                    break;

                case 7:

                    // verify if search criteria war given
                    if (isset($_POST['crit']))
                        $crit1 = $_POST['crit'];
                    else
                        $crit1 = "";

                    // verify if file name was given
                    if (isset($_POST['ftodown']))
                        $ftodown = trim($_POST['ftodown']);
                    else
                        $ftodown = "";

                    // Memorize search parameters
                    $rem1 = $ftodown;
                    $rem2 = $crit1;

                    // Sequence for table inline download button
                    if (@ $_POST['action'] == 'download') {
                        @ $ftodown = trim($_POST['actionId']);
                        require_once ROOT . '/mvc/functions/downloadspectredbaspm.php';
                    }

                    break;

                case 8:

                    // verify if search criteria war given
                    if (isset($_POST['crit']))
                        $crit1 = $_POST['crit'];
                    else
                        $crit1 = "";

                    // verify if file name was given
                    if (isset($_POST['ftodown']))
                        $ftodown = trim($_POST['ftodown']);
                    else
                        $ftodown = "";

                    // Memorize search parameters
                    $rem1 = $ftodown;
                    $rem2 = $crit1;

                    // Download sequence, if ftodown war given
                    if ($ftodown != "" && $_POST['download'] == '1') {
                        $ftodown = trim($_POST['ftodown']);
                        require_once ROOT . '/mvc/functions/downloadspectredbaspm.php';
                    }

                    break;
                // End download code--------------------------------------------  
                // Statistics---------------------------------------------------   
                case 3:

                    // Prepare the sql for number of spectra in m4ast database
                    $sql = "SELECT COUNT(FILE_NAME)
                            FROM m4ast_database
                           ";
                    $m4ast_specnr = mysqli_fetch_assoc($this->db->query($sql));
                    $m4ast_specnr = implode("", $m4ast_specnr);

                    // Prepare the sql for number of asteroids in m4ast database
                    $sql = "SELECT COUNT(DISTINCT O_NR)
                            FROM m4ast_database
                           ";
                    $m4ast_astnr = mysqli_fetch_assoc($this->db->query($sql));
                    $m4ast_astnr = implode("", $m4ast_astnr);

                    // Prepare the sql for number of  VIS, NIR and IR spectra in m4ast databas
                    $sql = "SELECT L_MIN,L_MAX
                            FROM m4ast_database
                           ";
                    $m4ast_vis = 0;
                    $m4ast_nir = 0;
                    $m4ast_ir = 0;
                    $m4ast_vis_nir = 0;
                    $m4ast_vis_nir_ir = 0;
                    $m4ast_nir_ir = 0;
                    $result = $this->db->query($sql);
                    while ($row = mysqli_fetch_assoc($result)) {
                        $L_MIN = $row['L_MIN'];
                        $L_MAX = $row['L_MAX'];
                        if ($L_MIN < 0.8 && $L_MAX < 1) {
                            $m4ast_vis++;
                        }
                        if ($L_MIN < 0.8 && $L_MAX < 2.5 && $L_MAX > 1) {
                            $m4ast_vis_nir++;
                        }
                        if ($L_MIN < 0.8 && $L_MAX > 2.5) {
                            $m4ast_vis_nir_ir++;
                        }
                        if ($L_MIN < 2.5 && $L_MIN > 0.8 && $L_MAX < 2.5) {
                            $m4ast_nir++;
                        }
                        if ($L_MIN < 2.5 && $L_MIN > 0.8 && $L_MAX > 2.5) {
                            $m4ast_nir_ir++;
                        }
                        if ($L_MIN > 2.5) {
                            $m4ast_ir++;
                        }
                    }

                    // Prepare the sql for number of spectra in SMASS MIT database
                    $sql = "SELECT COUNT(ID)
                            FROM smassmit_spectra
                           ";
                    $smassmit_specnr = mysqli_fetch_assoc($this->db->query($sql));
                    $smassmit_specnr = implode("", $smassmit_specnr);

                    // Prepare the sql for number of asteroids in m4ast database
                    $sql = "SELECT COUNT(ID)
                            FROM smassmit_asteroids
                           ";
                    $smassmit_astnr = mysqli_fetch_assoc($this->db->query($sql));
                    $smassmit_astnr = implode("", $smassmit_astnr);

                    // Prepare the sql for number of  VIS, NIR and IR spectra in SMASS MIT databas
                    $sql = "SELECT L_MIN,L_MAX
                            FROM smassmit_spectra
                           ";
                    $smassmit_vis = 0;
                    $smassmit_vis_nir = 0;
                    $smassmit_vis_nir_ir = 0;
                    $smassmit_nir_ir = 0;
                    $smassmit_nir = 0;
                    $smassmit_ir = 0;
                    $result = $this->db->query($sql);
                    while ($row = mysqli_fetch_assoc($result)) {
                        $L_MIN = $row['L_MIN'];
                        $L_MAX = $row['L_MAX'];
                        if ($L_MIN < 0.8 && $L_MAX < 1) {
                            $smassmit_vis++;
                        }
                        if ($L_MIN < 0.8 && $L_MAX < 2.5 && $L_MAX > 1) {
                            $smassmit_vis_nir++;
                        }
                        if ($L_MIN < 0.8 && $L_MAX > 2.5) {
                            $smassmit_vis_nir_ir++;
                        }
                        if ($L_MIN < 2.5 && $L_MIN > 0.8 && $L_MAX < 2.5) {
                            $smassmit_nir++;
                        }
                        if ($L_MIN < 2.5 && $L_MIN > 0.8 && $L_MAX > 2.5) {
                            $smassmit_nir_ir++;
                        }
                        if ($L_MIN > 2.5) {
                            $smassmit_ir++;
                        }
                    }

                    break;
                // End statistics-----------------------------------------------
                // Browse for information---------------------------------------
                case 4:

                    //to do
                    break;
                // End browse for informations----------------------------------

                default:
                    break;
            }
        }

        // Output the html
        require_once ROOT . '/mvc/view/tpl/database.tpl.php';
    }

    public function observing() {

        // Set the title page
        View::$title = 'Observing';

        $page_num = "";

        // Holds table content
        $fields = "";

        // Variables that hold previous search parameters
        $rem1 = "";
        $rem2 = "";
        $rem3 = "";

        $error = 0;

        if (isset($_POST['page']) && !empty($_POST['page'])) {
            $page_num = $_POST['page'];

            switch ($page_num) {

                case "plan":

                    //to do

                    break;

                case "plan_sem":

                    //to do

                    break;

                case "plan_n":


                    $today = getdate();

                    if (!isset($_POST['yyyy']))
                        $rem1 = $today['year'];
                    else
                        $rem1 = $_POST['yyyy'];
                    if (!isset($_POST['mm']))
                        $rem2 = $today['mon'];
                    else
                        $rem2 = $_POST['mm'];
                    if (!isset($_POST['dd']))
                        $rem3 = $today['mday'];
                    else
                        $rem3 = $_POST['dd'];

                    $yyyy = $rem1;
                    $mm = $rem2;
                    $dd = $rem3;

                    if (!isset($_POST['maglim']))
                        $Maglim = 18; // default value
                    else
                        $Maglim = $_POST['maglim']; // Limiting magnitude
                    if (!isset($_POST['malt']))
                        $malt = 25; // default value
                    else
                        $malt = $_POST['malt']; // Minimum number of hours for the object above minimum altitude
                    if (!isset($_POST['uai']))
                        $uai = '073'; // default value
                    else
                        $uai = $_POST['uai']; // Number of days for the compuation

























                        
// Verify if search parameters were entered
                    if (isset($_POST['plan'])) {
                        if (!empty($_FILES['userfile'])) {
                            // Get Julian day
                            $ora = 12;
                            $minut = 0;
                            $sec = 0;
                            $fus = 0;
                            $vara = 0;

                            // Get julian day
                            require_once ROOT . '/mvc/functions/julianday.php';

                            // Load asteroid designations
                            $Obj = file($_FILES["userfile"]["tmp_name"]); // load objects in a single array from the user input file
                            $headers = array(
                                '0' => 'Obj. Desig.',
                                '1' => 'V_Mag',
                                '2' => 'miu["/min]',
                                '3' => 'RA[h]',
                                '4' => 'DEC[deg]',
                                '5' => 'UT_s[h]',
                                '6' => 'Alt_s[deg]',
                                '7' => 'UT_m[h]',
                                '8' => 'Alt_m[deg]',
                                '9' => 'UT_e[h]',
                                '10' => 'Alt_e[deg]',
                                '11' => 'S.A.',
                                '12' => 'S.A.mag',
                                '13' => 'Dist',
                                '14' => 'RA[hh:mm:ss]',
                                '15' => 'DEC[dd:mm:ss]'
                            );

                            // Get orbital elements
                            require_once ROOT . '/mvc/functions/orbitelmmp.php';
                            // $Obj_data matrix contains the orbital elements extracted from file

                            var_dump($Obj_data);

                            // Populates table
                            require_once ROOT . '/mvc/functions/runsmplan.php';


                            /*
                              // Get Julian day
                              $JD = $this->np->jdaymp($dd,$mm,$yyyy,12,0,0,0,0);

                              // Load asteroid designations
                              $Obj = file($_FILES["userfile"]["tmp_name"]); // load objects in a single array from the user input file
                              $headers = array(
                              '0' => 'Obj. Desig.',
                              '1' => 'V_Mag',
                              '2' => 'miu["/min]',
                              '3' => 'RA[h]',
                              '4' => 'DEC[deg]',
                              '5' => 'UT_s[h]',
                              '6' => 'Alt_s[deg]',
                              '7' => 'UT_m[h]',
                              '8' => 'Alt_m[deg]',
                              '9' => 'UT_e[h]',
                              '10' => 'Alt_e[deg]',
                              '11' => 'S.A.',
                              '12' => 'S.A.mag',
                              '13' => 'Dist',
                              '14' => 'RA[hh:mm:ss]',
                              '15' => 'DEC[dd:mm:ss]'
                              );

                              // Get orbital elements
                              $Obj_data = $this->np->orbitelmmp($Obj);

                              // Populates table
                              $this->np->runsmplan($Obj_data,$JD,$Maglim,$malt,$uai,$ret);
                             */
                        } else {
                            View::$alert = 'No input file given';
                        }
                    } else {
                        View::$info = "Please select an input file and paramaters";
                    }

                    break;

                default:
                    break;
            }
        }

        // Output the html
        require_once ROOT . '/mvc/view/tpl/observing.tpl.php';
    }

    public function help() {

        // Set the title page
        View::$title = 'Help';

        //require_once ROOT . '/mvc/functions/genlog.php';
        // Output the html
        require_once ROOT . '/mvc/view/tpl/help.tpl.php';
    }

    public function relab() {

        // Set the title page
        View::$title = 'Relab spectrum';


        if (isset($_GET['rlfname']) && isset($_GET['faster'])) {
            // Get relab file name
            $rlfname = $_GET['rlfname'];
            $faster = $_GET['faster'];

            $out_rand = rand(1000, 9999);

            // Write octave script
            $fid = fopen(ROOT . '/mvc/output/script_octave' . $out_rand . '.m', 'w');
            fwrite($fid, sprintf('plotcmprelab_SpecDb("' . $faster . '.txt","' . $rlfname . '.txt",' . $out_rand . ');'));
            fclose($fid);

            // Execute octave script
            $out = shell_exec(Octave . ROOT ."/mvc/output/script_octave" . $out_rand . ".m");

            // Check if script executed succesfully
            if ($out == NULL) {
                View::$alert = "Octave script could not be executed";
            }

            // Download sequence
            if (isset($_POST['download'])) {
                $download = $_POST['download'];
                if ($download == 1) {
                    require_once ROOT . '/mvc/functions/downloadspectrerelab.php';
                }
            }

            // Get asteroid header
            if (file_exists(ROOT . '/mvc/library/Spectre/' . $faster . '.txt')) {
                $path_file = ROOT . '/mvc/library/Spectre/' . $faster . '.txt';
            } else {
                $path_file = ROOT . '/mvc/library/smassmit/spectre/' . $faster . '.txt';
            }
            require_once ROOT . '/mvc/functions/retrieveinfospecaspm.php';

            $sampleid = '';
            require_once ROOT . '/mvc/functions/retrieveinforelabfile.php';
            require_once ROOT . '/mvc/functions/retrieveinforelabbis.php';

            // Output the html
            require_once ROOT . '/mvc/view/tpl/relab.tpl.php';
        } else {
            View::$info = "No relab file name and/or asteroid name were given";

            // Output simple, error html
            require_once ROOT . '/mvc/view/tpl/notificationsonly.tpl.php';
        }
    }

    public function admin() {
        // Not an admin, go home
        if (!isset($_SESSION['auth'])) {
            // Redirect to home
            header('location: /m4ast');
            exit();
        }

        //Set page title
        View::$title = "Admin pannel";

        $page_num = "";

        // Variables that hold previous search parameters
        $rem1 = "";
        $rem2 = "";
        $rem3 = "";

        if (isset($_POST['page']) && !empty($_POST['page'])) {
            $values = explode("|", $_POST["page"]);
            $page_num = $values[0];

            switch ($page_num) {

                case "upload":

                    if (isset($_POST['aster_udesig'])) {
                        $aster_udesig = $_POST['aster_udesig'];
                    } else {
                        $aster_udesig = "";
                    }
                    if (isset($_POST['date'])) {
                        $date = $_POST['date'];
                    } else {
                        $date = "";
                    }if (isset($_POST['hrs'])) {
                        $hrs = $_POST['hrs'];
                    } else {
                        $hrs = "";
                    }if (isset($_POST['min'])) {
                        $min = $_POST['min'];
                    } else {
                        $min = "";
                    }if (isset($_POST['obsv'])) {
                        $obsv = $_POST['obsv'];
                    } else {
                        $obsv = "";
                    }if (isset($_POST['em_obsv'])) {
                        $em_obsv = $_POST['em_obsv'];
                    } else {
                        $em_obsv = "";
                    }if (isset($_POST['uai_code'])) {
                        $uai_code = $_POST['uai_code'];
                    } else {
                        $uai_code = 568;
                    }if (isset($_POST['landa_unit'])) {
                        $landa_unit = $_POST['landa_unit'];
                    } else {
                        $landa_unit = "u";
                    }if (isset($_POST['line_skipe'])) {
                        $line_skipe = $_POST['line_skipe'];
                    } else {
                        $line_skipe = 0;
                    }if (isset($_POST['article'])) {
                        $article = $_POST['article'];
                    } else {
                        $article = "Unpublished";
                    }
                    if (isset($_POST['comment'])) {
                        $comment = $_POST['comment'];
                    } else {
                        $comment = "";
                    }
                    if (isset($_FILES['userfile']) && ($_FILES['userfile']['size'] > 0)) {
                        // Input file is not valid, issue an error
                        if (($_FILES["userfile"]["error"] > 0) && ($_FILES["userfile"]["size"] < 5e6)) {
                            View::$alert = "Upload failed" . "</br>" . "The file is not valid";
                        } else {
                            if (isset($_POST['aster_udesig']) && !empty($_POST['aster_udesig'])) {
                                if (isset($_POST['date']) && !empty($_POST['date'])) {
                                    if (isset($_POST['uai_code']) && !empty($_POST['uai_code'])) {
                                        $aster_udesig = trim($_POST['aster_udesig']); // input designation of the asteroid
                                        
                                        $data = file_get_contents("http://vo.imcce.fr/webservices/ssodnet/resolver.php?name=".$aster_udesig);
                                        var_dump("http://vo.imcce.fr/webservices/ssodnet/resolver.php?name=".$aster_udesig);
                                        $data = explode("TABLEDATA", $data);
                                        var_dump($data[1]);
                                        $data = $data[1];
                                        $data = explode("<vot:TD>", $data);
                                        $data = implode("", $data);
                                        $data = explode("</vot:TD>", $data);
                                        $asterdesign = trim($data[2]);
                                        $data = explode(",", $data[5]);
                                        $asterno = trim($data[0]);
                                        unset($data[0]);
                                        for ($i = 1; $i <= count($data); $i++)
                                            $data[$i] = trim($data[$i]);
                                        $data = implode(",", $data);
                                        $astertmpdesig = $data;
                                        $astername = $asterdesign;
                                        var_dump($data);

                                        // Date
                                        $date = trim($_POST['date']); // date
                                        $min = trim($_POST['min']); // minutes
                                        $hrs = trim($_POST['hrs']); // hours
                                        // Observer
                                        $obsv = trim($_POST['obsv']); // Name of the person who did the observation
                                        $em_obsv = trim($_POST['em_obsv']); // Email of the person who did the observation
                                        // For the email maybe I should put something to check if is @
                                        // UAI code
                                        $uai_code = trim($_POST['uai_code']); // UAI code of the Observatory
                                        // Wavelength units
                                        $landa_unit = trim($_POST['landa_unit']); // landa factor to transform in microns the input wavelengths
                                        switch ($landa_unit) {
                                            case "n": // if nanometers
                                                $l_unit = 1000;
                                                break;
                                            case "u"://  microns
                                                $l_unit = 1;
                                                break;
                                            case "a": //angstroms
                                                $l_unit = 10000;
                                                break;
                                        }

                                        // ADS abstract link
                                        $article = trim($_POST['article']); // article in which the spectrum was published (default Unpublished)
                                        // Number of lines to be skiped
                                        $line_skipe = trim($_POST['line_skipe']); // number of lines to be skiped
                                        // Comment
                                        $comment = trim($_POST['comment']); // number of lines to be skiped
                                        //==========================================
                                        //Read Input file (Extract the wavelengths and Reflectances)
                                        //==========================================
                                        $landa[0] = 0; //initialize the array of wavelengths
                                        $Refl[0] = 0; // initialize the array of reflectances
                                        $std_dev[0] = 0; // initialize the array of standard deviations if will be given
                                        // Get wavelengths, reflectances,and (standard deviations if given)
                                        $this->upload->read_user_spectrum($_FILES["userfile"]["tmp_name"], $line_skipe, $landa, $Refl, $std_dev);


                                        $flag = 0; // initialize the flag to confirm the validity of data
                                        $landa_min = 0;
                                        $landa_max = 0;
                                        $landa_norm = 0;
                                        // Normalize the spectrum
                                        $this->upload->Spectrum_Normalization($l_unit, $landa, $Refl, $flag, $landa_norm, $landa_min, $landa_max);
                                        //==========================================
                                        // Build Catalog line
                                        $date_aux = explode("-", $date);
                                        $catalog_line = $asterdesign . " | " . $asterno . " | " . $date_aux[0] . sprintf("-%02d", $date_aux[1]) . sprintf("-%02d:", $date_aux[2]);
                                        $catalog_line = $catalog_line . sprintf("%02d:%02d", $hrs, $min) . " | ";
                                        $catalog_line = $catalog_line . $uai_code . " | " . sprintf("%.3f", $landa_min) . " | " . sprintf("%.3f", $landa_max) . " | ";
                                        // Check if input is already in catalog
                                        $aux_cat = 0;
                                        @$fdcatalog = fopen(ROOT . "/mvc/library/Spectre/AstSpec.log", "r");

                                        if ($fdcatalog > 0)
                                            while (!feof($fdcatalog)) {
                                                $ref_catalog = trim(fgets($fdcatalog));
                                                $aux_cat = $aux_cat + strpos($ref_catalog, $catalog_line);
                                                if ($aux_cat) {
                                                    $flag++;
                                                    // Issue an error if observation already in catalog
                                                    View::$alert = "Observation already in the catalog";
                                                }
                                            }
                                        @fclose($fdcatalog);

                                        $flag = $flag + strpos('q' . $asterdesign, '-'); // Check the asteroid designation

                                        if ($flag == 0) { // if  data is valid <=> flag = 0
                                            $obsv_number = 0; // this variable is used to upload more observation for the same night
                                            // The observations are not necessary sorted by data
                                            // check if a file with a simillar name already exist, in order to update the counter 
                                            // for the number of observations submited for the same night
                                            $spe_fname = $asterdesign . "_" . $date_aux[0] . sprintf("%02d", $date_aux[1]) . sprintf("%02d", $date_aux[2]) . "_" . $uai_code . "_" . sprintf("%02d", $obsv_number); //.".txt";
                                            $fexist = @fopen(ROOT . '/mvc/library/Spectre/' . $spe_fname . '.txt', "r"); // with @ to do not give warnning
                                            while ($fexist) { //while file exist increment the counter for the observations in the same night 
                                                $obsv_number ++;
                                                $spe_fname = $asterdesign . "_" . $date . "_" . $uai_code . "_" . sprintf("%02d", $obsv_number);
                                                $fexist = @fopen(ROOT . '/mvc/library/Spectre/' . $spe_fname . '.txt', "r");
                                            }
                                            $fdoutput = fopen(ROOT . '/mvc/library/Spectre/' . $spe_fname . '.txt', "w"); // open output file (result file that contain the spectrum)
                                            // Write Header of output file
                                            fwrite($fdoutput, sprintf("Asteroid designations ( Number, Name, Temporary designation): %s, %s, %s\n", $asterno, $astername, $astertmpdesig));
                                            fwrite($fdoutput, sprintf("Observation was done on (UT) (format YYYY-MM-DD-hh): %d - %02d - %02d - %02d:%02d\n", $date_aux[0], $date_aux[1], $date_aux[2], $hrs, $min));
                                            fwrite($fdoutput, sprintf("Observation was made by : %s E-mail; %s\n", $obsv, $em_obsv));
                                            fwrite($fdoutput, sprintf("At observatory with UAI code: $uai_code\n"));
                                            fwrite($fdoutput, sprintf("The spectrum was published in the article: %s\n", $article));
                                            fwrite($fdoutput, sprintf("Obsv. comments: %s\n", $comment));
                                            fwrite($fdoutput, sprintf("The normalization of this spectrum was made for the wavelength: %.3f microns.\n", $landa_norm));
                                            fwrite($fdoutput, sprintf("Original name of the file was: %s\n", $_FILES["userfile"]["name"]));
                                            fwrite($fdoutput, sprintf("Wavelength[um]	Reflectance	sigma(optional)\n"));

                                            // Write Data in the Output file spectrum
                                            if (count($std_dev) == count($landa)) {// standard deviation is given in input file 
                                                for ($i = 0; $i < count($landa); $i++) {// write landa Reflectence and standard deviation
                                                    fwrite($fdoutput, sprintf("%.3f %.5f %.5f\n", $landa[$i], $Refl[$i], $std_dev[$i]));
                                                }
                                            } else { // write only  wavelength and reflectance
                                                for ($i = 0; $i < count($landa); $i++) {
                                                    fwrite($fdoutput, sprintf("%.3f %.5f\n", $landa[$i], $Refl[$i]));
                                                }
                                            }
                                            fclose($fdoutput);

                                            // Write in catalog
                                            $fdcatalog = fopen(ROOT . "/mvc/library/Spectre/AstSpec.log", "a");

                                            $ast_name = trim($asterdesign);
                                            if (is_numeric($ast_name[0]) && is_numeric($ast_name[1]) && is_numeric($ast_name[2]) && is_numeric($ast_name[3])) {
                                                for ($i = 20; $i > 3; $i--) {
                                                    if (isset($ast_name[$i])) {
                                                        $ast_name[$i + 1] = $ast_name[$i];
                                                    }
                                                }
                                                $ast_name[4] = " ";
                                            }
                                            $asterdesign = $ast_name;
                                            $nameeq = $this->nameeq->nameeq($asterdesig, $asterno);

                                            $catalog_line = $spe_fname . " | " . $asterdesign . " | " . $asterno . " | " . $date;
                                            $catalog_line = $catalog_line . sprintf(":%02d:%02d", $hrs, $min) . " | ";
                                            $catalog_line = $catalog_line . $uai_code . " | " . sprintf("%.3f", $landa_min) . " | " . sprintf("%.3f", $landa_max) . " | ";
                                            $catalog_line = $catalog_line . $article . "\n";
                                            fwrite($fdcatalog, $catalog_line . " | " . $nameeq);
                                            fclose($fdcatalog);

                                            // Write VO catalog
                                            $this->upload->writevolog($catalog_line);

                                            View::$success = 'Observation successfully submitted<br>For the asteroid with the name ' . $astername;
                                            View::$success = View::$success . ', number ' . $asterno . ', and temporary designation ' . $astertmpdesig;
                                            View::$success = View::$success . ', you uploaded the file ' . $_FILES["userfile"]["name"] . '<br>';
                                            View::$success = View::$success . 'The result file can be found with the name <a href="/m4ast/index.php/index/analyze.php?file_name=' . $spe_fname . 'target="_blank">' . $spe_fname . '</a><br>';
                                            View::$success = View::$success . 'Please update m4ast database finish your submission';
                                        } // END IF flag
                                        else {
                                            if (strpos('q' . $asterdesign, '-')) {
                                                View::$alert = 'Wrong asteroid designation<br>';
                                            }
                                        }
                                    } else {
                                        View::$alert = 'UAI code of the observatory where the observation was made must be given';
                                    }
                                } else {
                                    View::$alert = 'Observation date must be given';
                                }
                            } else {
                                View::$alert = 'Asteroid name must be given';
                            }
                        }
                    } else {
                        View::$info = 'Please select the file you wish to upload and complete the required fields';
                    }

                    break;

                case "upm4ast":

                    //last update time
                    $date = filemtime(ROOT . "/mvc/library/m4ast_uptime.txt");
                    $date = gmdate("Y-m-d", $date);

                    if (isset($values[1]) && !empty($values[1])) {

                        if ($values[1] == "update") {
                            //updates asteroid db to sql
                            require_once ROOT . '/mvc/functions/makeasterdb.php';
                        }
                        if ($values[1] == "hardupdate") {

                            // Update last update time
                            touch(ROOT . "/mvc/library/m4ast_uptime.txt");

                            // Clear asteroid database
                            // Prepare the SQL
                            $sql = "DELETE FROM `m4ast_database`
                                    WHERE 1
                                ";

                            // Execute the query
                            if ($result = $this->db->query($sql)) {
                                //updates asteroid db to sql
                                require_once ROOT . '/mvc/functions/makeasterdb.php';
                            } else {
                                // Check again
                                View::$alert = 'Operation could not be completed.<br>Please try again';
                            }
                        }
                    }

                    break;

                case "upsmass":

                    //last update time
                    $date = filemtime(ROOT . "/mvc/library/smass_uptime.txt");
                    $date = gmdate("Y-m-d", $date);


                    if (isset($values[1]) && !empty($values[1])) {
                        if ($values[1] == "update") {
                            //update smassmit db to sql
                            require_once ROOT . '/mvc/functions/makesmassmitdb.php';
                        }
                        if ($values[1] == "hardupdate") {
                            // Clear SMASS MIT database
                            // Prepare the SQL
                            $sql = "DELETE FROM `smassmit_asteroids`
                                    WHERE 1
                                ";

                            $sql2 = "DELETE FROM `smassmit_spectra`
                                    WHERE 1
                                ";

                            // Execute the query
                            if ($result = $this->db->query($sql) && $result2 = $this->db->query($sql2)) {
                                //update smassmit db to sql
                                require_once ROOT . '/mvc/functions/makesmassmitdb.php';
                            } else {
                                // Check again
                                View::$alert = 'Operation could not be completed.<br>Please try again';
                            }
                        }
                        if ($values[1] == "regenerate") {

                            // Update last update time
                            touch(ROOT . "/mvc/library/smass_uptime.txt");

                            //dl smassmit spectra
                            require_once ROOT . '/mvc/functions/smassmitget.php';

                            //make the smassmit database usable
                            require_once ROOT . '/mvc/functions/smassmitcheck.php';

                            //update smassmit db to sql
                            require_once ROOT . '/mvc/functions/makesmassmitdb.php';
                        }
                        if ($values[1] == "download") {
                            //dl smassmit spectra
                            require_once ROOT . '/mvc/functions/smassmitget.php';
                        }
                        if ($values[1] == "check") {
                            //make the smassmit database usable
                            require_once ROOT . '/mvc/functions/smassmitcheck.php';
                        }
                    }

                    break;

                case "adduser":

                    // Post provided
                    if (isset($_POST['username']) && isset($_POST['password'])) {
                        // Validate username and password
                        $password = trim($_POST['password']);
                        $username = trim($_POST['username']);
                        $ypassword = trim($_POST['pass']);

                        if (empty($_POST['username']) || empty($_POST['password'] || empty($_POST['pass']))) {
                            View::$alert = 'You must enter username, password for the new user andyour password';
                        } else {
                            // Check if username available
                            $sql = "
                            SELECT USERNAME FROM `user` WHERE 
                            `USERNAME` = '" . $this->db->escape($username) . "'
                        ";
                            $result = $this->db->query($sql);

                            // Run the query
                            if (false == $row = mysqli_fetch_assoc($result)) {

                                if ($_SESSION['data']['PASSWORD'] == md5($ypassword)) {
                                    // Prepare the SQL
                                    $sql = "INSERT INTO `user`
                                    SET USERNAME = '" . $this->db->escape($username) . "', 
                                        PASSWORD = '" . md5($password) . "'
                                    ";

                                    // Execute the query
                                    if ($result = $this->db->query($sql)) {
                                        // Redirect to login
                                        View::$info = 'User ' . $username . ' added succesfully';
                                    } else {
                                        // Check again
                                        View::$alert = 'SQL could not be executed <br>Check the form and try again';
                                    }
                                } else {
                                    View::$alert = 'Password did not match';
                                }
                            } else {
                                // Check again
                                View::$alert = 'Username already in use';
                            }
                        }
                    } else {
                        View::$info = 'Enter username and password for new user';
                    }

                    break;

                case "pass":

                    // Post provided
                    if (isset($_POST['oldp']) && isset($_POST['newp'])) {
                        // Validate username and password
                        $oldp = trim($_POST['oldp']);
                        $newp = trim($_POST['newp']);
                        $newp2 = trim($_POST['newp2']);

                        if (empty($_POST['oldp']) || empty($_POST['newp']) || empty($_POST['newp2'])) {
                            View::$alert = 'You must complete all fields';
                        } else {
                            // Prepare the query
                            $sql = "
                                SELECT * FROM `user` WHERE 
                                `USERNAME` = '" . $_SESSION['auth'] . "'
                             ";
                            $result = $this->db->query($sql);
                            // Run the query
                            if (null !== $row = mysqli_fetch_assoc($result)) {
                                // Check if password matches
                                if (md5($oldp) == $row['PASSWORD']) {
                                    if ($oldp == $newp) {
                                        View::$alert = 'New password must differ from old password';
                                    } else {

                                        if ($newp == $newp2) {
                                            // Prepare the query
                                            $sql = "
                                            UPDATE  `user` 
                                            SET     `PASSWORD` = '" . md5($newp) . "'
                                            WHERE   `USERNAME` = '" . $_SESSION['auth'] . "'
                                        ";

                                            // Execute the query
                                            if ($result = $this->db->query($sql)) {
                                                // Update succesful
                                                View::$info = 'Password for user ' . $_SESSION['auth'] . ' updated succesfully';
                                            } else {
                                                // Check again
                                                View::$alert = 'SQL could not be executed <br>Check the form and try again';
                                            }
                                        } else {
                                            View::$alert = 'Reentered password did not match';
                                        }
                                    }
                                } else {
                                    View::$alert = 'Password did not match<br>Please try again';
                                }
                            } else {
                                View::$alert = 'Query could not be executed<br>Please try again';
                            }
                        }
                    } else {
                        View::$info = 'Enter old and new password';
                    }

                    break;

                case "make":

                    if (isset($values[1]) && !empty($values[1])) {
                        if ($values[1] == "make") {
                            // Make database
                            require_once ROOT . '/mvc/functions/gentables.php';
                        }
                    }

                    break;

                case "removeuser":

                    $sql = " 
            SELECT      USERNAME 
            FROM        user
    ";

                    // Prepare the data
                    $fields = array();

                    // Generate table headers
                    $headers = array(
                        'USERNAME' => 'Username',
                    );
                    // Get the result, max 50
                    if (false !== $result = $this->db->query($sql)) {
                        while ($row = mysqli_fetch_assoc($result)) {
                            $fields[] = $row;
                        }
                    }

                    if (isset($_POST['actionId']) && !empty($_POST['actionId'])) {
                        $sql = " 
                                DELETE FROM    user
                                WHERE   USERNAME = '" . $_POST['actionId'] . "'
                        ";

                        if (false !== $result = $this->db->query($sql)) {
                            View::$success = 'User removed succesfully';
                        } else {
                            View::$alert = 'User could not be found';
                        }
                    }


                    break;

                default:
                    break;
            }
        }

        //Output the html
        require_once ROOT . '/mvc/view/tpl/admin.tpl.php';
    }

    public function login() {
        // Set the page title
        View::$title = 'Login';

        // Post provided
        if (isset($_POST) && !empty($_POST)) {
            if (!empty($_POST['username']) && !empty($_POST['password'])) {
                // Validate username and password
                $username = $_POST['username'];
                $password = $_POST['password'];

                // Prepare the query
                $sql = "
                    SELECT * FROM `user` WHERE 
                    `USERNAME` = '" . $this->db->escape($username) . "'
                    AND 
                    `PASSWORD` = '" . md5($password) . "'
                ";
                // Get the result
                $result = $this->db->query($sql);

                // Run the query
                if (null !== $row = mysqli_fetch_assoc($result)) {
                    // Good login
                    $_SESSION['auth'] = $username;
                    $_SESSION['data'] = $row;

                    // Redirect to home
                    header('location: /m4ast');
                    exit();
                }

                View::$alert = 'Bad username or password';
            } else {
                View::$alert = 'You must enter both username and password';
            }
        } else {
            View::$info = 'Login required to access database manipulation';
        }

        // Output the HTML
        require_once ROOT . '/mvc/view/tpl/login.tpl.php';
    }

    public function logout() {
        // Clear session data
        unset($_SESSION['auth']);
        unset($_SESSION['data']);

        // Redirect to home
        header('location: /m4ast');
    }

}
/*EOF*/