<?php
// Define the root
define('ROOT', dirname(__FILE__));  //this will take the path to the index, all the way to /m4ast

// Define path to octave output files
define('OctaveImg', 'http://spectre.imcce.fr/m4ast');

// Define path to octave
define('Octave', '/usr/local/bin/octave ');

// Get the request URI without m4ast/index.php
$requestUri = trim(preg_replace('%^\/m4ast(\/index\.php)?%i', '', $_SERVER['PHP_SELF']), '/');

// Prepare the controller
$pieces = array_filter(explode('/', $requestUri));

// Get the controller
if (null === $controller = array_shift($pieces)) {
    $controller = 'Index';
}

$controller = ucfirst(strtolower($controller));

// Get the method
if (null === $method = array_shift($pieces)) {
    $method = 'home';
}

// Prepare the class
$className = 'Controller' . $controller;

// Class path
$classPath = ROOT . '/mvc/controller/' . strtolower($controller) . '.php';

// Class exists
if (!is_file($classPath)) {
    $classPath = '/mvc/controller/Index.php';
    $className = 'ControllerIndex';
}

// Require controller
require_once $classPath;

// Common values
class View {
    public static $title;
    public static $body;
    public static $alert;
    public static $success;
    public static $info;
}

// Get the object
$classObject = new $className;

// No autoloader
require_once ROOT . '/mvc/library/db.php';

// New db
$classObject->db = new LibraryDb();

// No autoloader
require_once ROOT . '/mvc/functions/NightPlanning.php';

// New NightPlanning
$classObject->np = new NightPlanning();

// No autoloader
require_once ROOT . '/mvc/functions/Upload.php';

// New Upload
$classObject->upload = new Upload();

// No autoloader
require_once ROOT . '/mvc/functions/NameEquiv.php';

// New NameEquiv
$classObject->nameeq = new NameEquiv();

// Start the session
session_start();

// Get the method
if (!method_exists($classObject, $method)) {
    $method = 'home';
}

// Output buffer
ob_start();

// Call the method
call_user_func_array(array($classObject, $method), $pieces);

// Get the text
View::$body = ob_get_clean();

if (!isset(View::$title)) {
    View::$title = ucfirst($method);
}

// Load the template
require_once ROOT . '/mvc/view/tpl/index.tpl.php';